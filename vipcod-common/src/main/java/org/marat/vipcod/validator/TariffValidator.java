package org.marat.vipcod.validator;

import org.apache.commons.lang3.StringUtils;
import org.marat.vipcod.model.entities.Tariff;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Администратор on 19.06.14.
 */
public class TariffValidator extends AbstractValidator<Tariff>{
    public List<String> findErrors(Tariff tariff){
        ArrayList<String> errors = new ArrayList<String>();

        if (StringUtils.isBlank(tariff.getName())) errors.add("Укажите название модели");
        if (tariff.getPrice() <= 0) errors.add("Укажите цену тарифа");
        if (tariff.getCountPortion() <= 0) errors.add("Укажите кол-во посещений для тарифа");
        if (tariff.getDaysPortion() <= 0) errors.add("Укажите кол-во дней для тарифа");

        return errors;
    }
}
