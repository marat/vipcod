package org.marat.vipcod.validator;

/**
 * Created by Администратор on 19.06.14.
 */
public class ValidatorException extends RuntimeException {
    public ValidatorException(String msg) {
        super(msg);
    }
}
