package org.marat.vipcod.validator;

import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.action.Abonement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marat on 08.06.2015.
 */
public class AbonementValidator  extends AbstractValidator<Abonement>  {

    @Override
    public List<String> findErrors(Abonement abonement) throws ValidatorException {
        ArrayList<String> errors = new ArrayList<String>();

        if (abonement == null) errors.add("action abonement is null");
        else {
            if (abonement.getEndTime() == null) errors.add("Укажите время действия абонемента");
            if (abonement.getMaxCount() == null || abonement.getMaxCount() <= 0)
                errors.add("Укажите количество посещений для абонемента");
        }

        if (abonement.getCard() == null){
            errors.add("abonement without card!");
        }

        ValidatorFactory.validate(abonement.getCard());

        return errors;
    }
}
