package org.marat.vipcod.validator;

import org.apache.commons.lang3.StringUtils;
import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.entities.action.Abonement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Администратор on 18.06.14.
 */
public class CardValidator extends AbstractValidator<Card> {

    public List<String> findErrors(Card card) {
        ArrayList<String> errors = new ArrayList<String>();

        Person person = card.getPerson();
        if (person == null) errors.add("person is null");
        else {
            if (StringUtils.isBlank(person.getName())) errors.add("Укажите имя клиента");
            if (StringUtils.isBlank(person.getSecondName())) errors.add("Укажите фамилию клиента");
            if (person.getBirth() == null) errors.add("Укажите дату рождения клиента");
        }

        /*Abonement limit = card.getSelectedAbonement();
        ValidatorFactory.validate(limit);*/

        return errors;
    }
}
