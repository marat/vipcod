package org.marat.vipcod.validator;

import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.Tariff;
import org.marat.vipcod.model.entities.action.Abonement;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Администратор on 19.06.14.
 */
public class ValidatorFactory {
    private static Map<Class, AbstractValidator> validators = new HashMap<Class, AbstractValidator>();

    static {
        validators.put(Card.class, new CardValidator());
        validators.put(Abonement.class, new AbonementValidator());
        validators.put(Tariff.class, new TariffValidator());
    }


    public static void beanValidate(Object obj) throws ValidatorException {
        javax.validation.ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Object>> errs = validator.validate(obj);
        if (!errs.isEmpty()) {
            throw new ValidatorException(convertViolationsToString(errs.toArray()));
        }
    }

    private static String convertViolationsToString(Object[] strArr) {
        StringBuilder sb = new StringBuilder();
        for (Object str : strArr)
            sb.append(
                    str instanceof ConstraintViolationImpl ? ((ConstraintViolationImpl) str).getMessage() : String.valueOf(str)
            );
        return sb.toString();
    }


    public static void validate(Object obj) throws ValidatorException {
//        beanValidate(obj);

        boolean validated = false;
        for (Class clz : validators.keySet()) {
            if (clz.isInstance(obj)) {
                validators.get(clz).validate(obj);
                validated = true;
            }
        }
        if (!validated) throw new IllegalArgumentException("Validator not defined for " + obj);
    }
}
