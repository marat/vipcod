package org.marat.vipcod.validator;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Администратор on 19.06.14.
 */
public abstract class AbstractValidator<T> {
    public abstract List<String> findErrors(T t) throws ValidatorException;

    public void validate(T t) throws ValidatorException {
        List<String> errors = findErrors(t);
        if (errors != null && !errors.isEmpty()) throw new ValidatorException(Arrays.toString(errors.toArray()));
    }
}
