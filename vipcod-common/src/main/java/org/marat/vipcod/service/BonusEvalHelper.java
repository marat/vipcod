package org.marat.vipcod.service;

import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.action.Abonement;

/**
 * Created by Marat on 13.07.2015.
 */
public class BonusEvalHelper {

    public static void changeBonusesOnPayment(Abonement abonement, Double paySum, Double payBonus) {
        Card card = abonement.getCard();
        card.setBonus(card.getBonus() + 0.1 * paySum);
        if (card.getBonus() < payBonus) throw new RuntimeException("?? ??????? ??????? ??? ????????");
        card.setBonus(card.getBonus() - payBonus);
    }
}
