package org.marat.vipcod.service;

import org.marat.vipcod.model.entities.Tariff;
import org.marat.vipcod.model.entities.ModelUtils;
import org.marat.vipcod.model.entities.action.Abonement;

import java.util.Date;

/**
 * Created by marat on 18.02.2015.
 */
public class UpdateAbonementHelper {

    /**
     * устанавливает ограничения карты в соответствие с моделью. состояние карты обновляет в @activeCount
     * @param abonement - изменяемый абонемент
     * @param activeCount - новое состояние карты, либо null если не требуется обновлять
     * @return - обновленная карта по модели и новому состоянию
     */
    public static Abonement fillCardByTariff(Abonement abonement, Long activeCount) {
        if (activeCount != null) {
            abonement.setCount(activeCount);
        }

        Tariff tariff = abonement.getTariff();
        if (tariff != null) {
            abonement.setMaxCount(tariff.getCountPortion());
            Date fromTime = new Date();
            abonement.setStartTime(ModelUtils.beginOfDay(fromTime));
            if (tariff.getDaysPortion() != null) {
                abonement.setEndTime(ModelUtils.addDays(fromTime, tariff.getDaysPortion().intValue()));
            }
        }

        return abonement;
    }
}
