package org.marat.vipcod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.RollbackException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

/**
 * Created by Marat on 16.06.2015.
 */
public class MessageUtils {

    public static String getExceptionMessage(Throwable e) {
        if (e == null) return null;
        if (e instanceof RollbackException) e = e.getCause();

        Throwable x = e;
        int i = 10;

        while (x != null && (i-- > 0)) {
            if (x instanceof ConstraintViolationException) {
                ConstraintViolationException cve = (ConstraintViolationException) x;
                ConstraintViolation<?> violation = cve.getConstraintViolations().iterator().next();
                //return violation.getMessage() + " : " + violation.getInvalidValue();
                return String.format("%s : %s", violation.getMessage(), violation.getPropertyPath());
            }

            x = x.getCause();
        }

        String message = e.getMessage();
        return message == null ? e.toString() : message;
    }

    static final Logger logger = LoggerFactory.getLogger(MessageUtils.class);


    public static void printExceptionTrace(Exception ex) {
        for (StackTraceElement st : ex.getStackTrace()) {
            if (st.toString().contains("marat")) {
                System.err.println("######## " + st);
            }
        }
    }
}
