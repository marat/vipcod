vipcodApp.controller('PaymentsController', function ($scope, $rootScope, $http, $log, $location, alertService) {

    moment.locale('ru');

    $rootScope.monthList = Utils.monthList.slice(10); //todo: получать из dim_monthyear
    $rootScope.nextMonth = Utils.nextMonth;

    onEvent($rootScope, 'event_showPayments', function (event, item) {
        $scope.showPage();
    });

    $scope.countPerPage = 20;
    $scope.currentPage = 1;
    $scope.maxSize = 5;
    $scope.currentFilter = '';

    $scope.person_id = $location.search().person_id;
    $scope.coach_id = $location.search().coach_id;
    $scope.service_id = $location.search().service_id;
    $scope.period = $location.search().period;

    $scope.$watch('currentPage', function () {
        $scope.showPage()
    });

    $scope.edit = function (item) {
        $rootScope.$emit('event_editPayment', item);
    };

    $scope.showPage = function () {
        $http.post(Settings.REST_POINT + '/payment/list',
            {
                "personUuid": $scope.person_id,
                "coachUuid": $scope.coach_id,
                "serviceId": $scope.service_id,
                "period": $scope.period == undefined ? null : $scope.period,
                "pageNum": $scope.currentPage - 1,
                "pageSize": $scope.countPerPage,
                "filter": $scope.currentFilter
            }
        ).then(function (resp) {
            $scope.items = resp.data.items;
            $scope.totalItems = resp.data.total;
            if ($scope.person_id) {
                $scope.person = resp.data.person;
            }

        }).catch(function (resp) {
            if (Utils.verifyUser(resp.status, $location)) return;
            Utils.showErrorMessage(resp);
        });
    };

    $scope.export = function () {
        location = Settings.REST_POINT + "/coach/payments/" + $scope.coach_id + "/" + $scope.service_id + ".xlsx?"
            + ($scope.period ? ("&period=" + $scope.period) : "")
            + ($scope.currentFilter ? ("&filter=" + $scope.currentFilter) : "");
    };

    $scope.filterQuery = function (periodValue) {
        //location = window.location + "&period=" + period;
        $location.search({
            coach_id: $scope.coach_id,
            service_id: $scope.service_id,
            person_id: $scope.person_id,
            period: $scope.period ? null : periodValue,
            filter: $scope.currentFilter
        }).replace();
    };

});

vipcodApp.controller("ModalPaymentController", function ($scope, $rootScope, $uibModal, $http, $log, $location, alertService) {

    onEvent($rootScope, 'event_newPayment', function (event, person) {
        $scope.person_id = person.uuid;
        $scope.openNew();
    });

    onEvent($rootScope, 'event_editPayment', function (event, item) {
        $scope.item = angular.copy(item);
        $scope.item.originSum = $scope.item.sum;
        $scope.open();
    });

    $scope.openNew = function () {
        $scope.item = initNewPersonPayment($scope.person_id);
        $scope.open();
    };

    function initNewPersonPayment(person_id) {
        var initPerson = {
            person: {uuid: person_id},
            backSum: 0,
            originSum: null,
            cash: true
        };

        $http.get(Settings.REST_POINT + '/payment/last/' + $scope.person_id
        ).then(function (resp) {
            initPerson.service = resp.data.service;
            initPerson.tariff = resp.data.tariff;
            initPerson.coach = resp.data.coach;
            //initPerson.cash = data.cash; - для нового платежа делаем стабильно НАЛ
        });

        return initPerson;
    }

    $scope.open = function () {
        var modalWin = $uibModal.open({
            templateUrl: 'views/modal/modalPaymentContent.html?bust=' + Math.random().toString(36).slice(2),
            backdrop: true,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log, $http, $filter, item) {

                var vm = this;

                vm.selectedSessionsValue = {}; // состояние чекбоксов сессий (календаря)
                vm.selectedDatesValue = {}; // состояние чекбоксов по датам (календаря)

                $scope.selectedSessionsValue = vm.selectedSessionsValue;
                $scope.selectedDatesValue = vm.selectedDatesValue;
                $scope.selectedScheduleItems = []; // выбранные пункты расписания для резервации (учитываются только поля: data, session.id)

                // действия при изменении состояния выбора расписания: даты
                var onChangeSelectedDates = function (newValue, oldValue) {
                    if (newValue == oldValue) return;

                    // актуализация selectedScheduleItems
                    $scope.selectedScheduleItems = [];
                    Object.keys(vm.selectedDatesValue).forEach(function (dt) {
                        if (vm.selectedDatesValue[dt]) {
                            var coord = dt.split('+');
                            $scope.selectedScheduleItems.push($scope.availableSchedulesMap[coord[0]][coord[1]][coord[2]]);
                        }
                    });

                    $scope.item.visitsCount = $scope.selectedScheduleItems.length;
                    $log.log("$scope.item.visitsCount: " + $scope.item.visitsCount);

                    $scope.refreshFormSum();
                };

                // действия при изменении состояния выбора расписания: сессии
                var onSelectedScheduleHandler = function (newValue, oldValue) {
                    if (!newValue && !oldValue) return;
                    if (newValue == oldValue) return;

                    newValue = newValue ? JSON.parse(newValue) : {}; // поскольку для проверки изменений использовался JSON.stringify
                    oldValue = oldValue ? JSON.parse(oldValue) : {}; // поскольку для проверки изменений использовался JSON.stringify

                    $log.log('newValue: ', newValue);
                    $log.log('oldValue: ', oldValue);

                    var selectedTimes = [];
                    var beforeTimes = [];
                    Object.keys(newValue).forEach(function (key) {
                        if (newValue[key]) selectedTimes.push(key);
                    });
                    Object.keys(oldValue).forEach(function (key) {
                        if (oldValue[key]) beforeTimes.push(key);
                    });

                    $log.log('selectedTimes: ', selectedTimes);
                    $log.log('beforeTimes: ', beforeTimes);

                    var diffAdd = selectedTimes.subtract(beforeTimes); // добавленные эелементы
                    var diffDel = beforeTimes.subtract(selectedTimes); // удаленные элементы

                    $log.log('diffAdd: ', diffAdd);
                    $log.log('diffDel: ', diffDel);


                    //  availableSchedulesMap[dayOfWeek][begin][date]

                    diffAdd.forEach(function (str1) {
                        var coord = str1.split('+');
                        var datamap = $scope.availableSchedulesMap[coord[0]][coord[1]];
                        Object.keys(datamap).forEach(function (dt) {
                            vm.selectedDatesValue[datamap[dt].session.dayOfWeek + '+' + datamap[dt].session.begin + '+' + dt] = true;
                        });
                    });

                    diffDel.forEach(function (str1) {
                        var coord = str1.split('+');
                        var datamap = $scope.availableSchedulesMap[coord[0]][coord[1]];
                        Object.keys($scope.availableSchedulesMap[coord[0]][coord[1]]).forEach(function (dt) {
                            delete vm.selectedDatesValue[datamap[dt].session.dayOfWeek + '+' + datamap[dt].session.begin + '+' + dt];
                        });
                    });

                    onChangeSelectedDates();
                };

                $scope.$watch(function (scope) {
                    return JSON.stringify(scope.selectedSessionsValue);
                }, onSelectedScheduleHandler);

                $scope.$watch(function (scope) {
                    return JSON.stringify(scope.selectedDatesValue);
                }, onChangeSelectedDates);


                /*$scope.$watch(function (scope) {
                 return JSON.stringify(scope.selectedDatesValue);
                 }, function (newV, oldV) {
                 onSelectedScheduleHandler(newV, oldV);
                 });*/

                if (!item.reservations) item.reservations = [];

                item.reservations.forEach(function (resn) {
                    $scope.selectedScheduleItems.push(resn.schedule);
                    vm.selectedSessionsValue[resn.schedule.session.dayOfWeek + '+' + resn.schedule.session.begin] = true;
                    vm.selectedDatesValue[resn.schedule.session.dayOfWeek + '+' + resn.schedule.session.begin + '+' + resn.schedule.date] = true;
                });

                $scope.availableSchedules = [];

                $scope.services = [];
                $scope.tariffs = [];
                $scope.coaches = [];
                $scope.coachesForSelect = [];
                $scope.persons = [];

                var now = new Date();
                if (!item.payTime) item.payTime = new Date(Date.UTC(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes()));
                $scope.item = item;

                /**
                 * СОХРАНЕНИЕ ПЛАТЕЖА
                 */
                $scope.submit = function () {
                    // убираем зацикленность
                    $scope.item.service.tariffs = null;

                    // отправляем данные на сервер
                    $http.post(Settings.REST_POINT + '/payment/save',
                        {
                            payment: $scope.item,
                            selectedScheduleItems: $scope.selectedScheduleItems
                        }
                    ).then(function (resp) {
                        if (resp.data.error) {
                            Utils.showErrorMessage(resp.data.error, alertService);
                        } else {
                            $scope.item = resp.data.result;
                            $rootScope.$emit('event_showPayments', $scope.item);
                        }
                    }).catch(function (resp) {
                        if (Utils.verifyUser(resp.status, $location, $uibModalInstance)) return;
                        Utils.showErrorMessage(resp.data, alertService);
                    });

                    $uibModalInstance.dismiss('cancel');
                };

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.compensationButtonOnClick = function () {
                    var modalInstance = $uibModal.open({
                        templateUrl: 'views/modal/modalPaymentCompensationContent.html?bust=' + Math.random().toString(36).slice(2),
                        backdrop: true,
                        windowClass: 'modal',
                        controller: 'PaymentCompensationController',
                        resolve: {
                            item: function () {
                                return $scope.item;
                            }
                        }
                    });

                    modalInstance.result.then(function (selectedItem) {
                        $scope.refreshForm(false);
                    }, function () {
                        $scope.refreshForm(false);
                        //$log.info('Modal dismissed at: ' + new Date());
                    });
                };

                $scope.updateCoachesForService = function () {
                    $scope.coachesForSelect = [];
                    var theCoach = null;


                    // выборка тренеров по услуге
                    $scope.coaches.forEach(function (coach) {
                        if (coach.services) {
                            coach.services.forEach(function (service) {
                                if (service && $scope.item.service && service.id == $scope.item.service.id) {
                                    $scope.coachesForSelect.push(coach);
                                    if ($scope.item.coach && $scope.item.coach.uuid == coach.uuid) {
                                        // не обнуляем тренера, если выбрана услуга, которую он предоставляет
                                        theCoach = $scope.item.coach;
                                    }
                                }
                            });
                        }
                    });

                    $scope.item.coach = theCoach;
                };

                $scope.onSelectService = function () {
                    $scope.item.tariff = null;
                    //$scope.item.coach = null;
                    $scope.updateCoachesForService();
                    $scope.refreshForm(true);
                };

                $scope.onSelectTariff = function () {
                    $scope.refreshForm(false);
                };

                $scope.onSelectCoach = function () {
                    $scope.refreshForm(true);
                };

                $scope.onSelectPeriod = function () {
                    $scope.refreshForm(true);
                };

                $scope.updateCalendarData = function (callback_fn) {

                    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    //$log.log('vibor konkretnih dat eshe ne realizovan - poka rabotaem s periodami');
                    //callback_fn();
                    //return;
                    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    $log.log('#### update data: ' + $scope.item.period + '  ::  ' + Utils.getMonthById($scope.item.period));

                    $http.get(Settings.REST_POINT + '/schedule/groups/'
                        + ($scope.item.service ? $scope.item.service.id : '0') + "/"
                        + ($scope.item.coach ? $scope.item.coach.uuid : '0') + "/"
                        + ($scope.item.tariff ? $scope.item.tariff.uuid : '0') + "/"
                        + (Utils.getMonthById($scope.item.period) ? Utils.getMonthById($scope.item.period).mmyyyy : '0')
                    ).then(function (resp) {
                        if (!resp.data) {
                            callback_fn();
                            return;
                        }

                        $scope.availableSchedules = [];
                        $scope.availableSchedulesMap = {
                            1: {},
                            2: {},
                            3: {},
                            4: {},
                            5: {},
                            6: {},
                            7: {}
                        };

                        resp.data.forEach(function (record) {
                            $scope.availableSchedules.push(record);

                            if (!$scope.availableSchedulesMap[record.session.dayOfWeek][record.session.begin]) $scope.availableSchedulesMap[record.session.dayOfWeek][record.session.begin] = {};
                            $scope.availableSchedulesMap[record.session.dayOfWeek][record.session.begin][record.date] = record;
                        });


                        callback_fn();

                    }).catch(function (resp) {
                        if (Utils.verifyUser(resp.status, $location)) return;
                        Utils.showErrorMessage(resp);
                    });
                };

                $scope.refreshFormSum = function () {
                    ////// пересчет суммы
                    $scope.item.sum = ($scope.item.tariff ? $scope.item.tariff.priceOne : 0) * $scope.item.visitsCount - $scope.item.backSum;
                    //if ($scope.item.sum < 0) $scope.item.sum = '' || $scope.item.originSum;
                    $scope.restyleSum();
                };

                $scope.refreshForm = function (truncSum) {

                    ////// перерисовка календаря
                    // запрос учебных групп по выбранным параметрам
                    $scope.updateCalendarData(function () {
                        $log.log("######### refreshForm ##########");
                        if ($scope.selectedScheduleItems && truncSum && $scope.availableSchedules && $scope.availableSchedules.length > 0) {
                            $scope.selectedScheduleItems = [];
                            for (var member in vm.selectedSessionsValue) delete vm.selectedSessionsValue[member];
                            for (var member in vm.selectedDatesValue) delete vm.selectedDatesValue[member];
                            $scope.item.visitsCount = 0;
                        }

                        ////// пересчет суммы
                        $scope.refreshFormSum();
                    });
                };

                $scope.restyleSum = function () {
                    if ($scope.item.originSum && $scope.item.sum != $scope.item.originSum) {
                        $scope.sumStyle = {'font-style': 'italic', 'color': 'red'};
                    } else {
                        $scope.sumStyle = {};
                    }
                };

                $scope.onChangeSum = function () {
                    $scope.restyleSum();
                };

                $scope.isRefund = function (item) {
                    var result = item.sum != null && item.sum < 0;
                    //if (result) $scope.item.period = 0;
                    return result;
                };

                $scope.getTitleForTimeSchedule = function (data) {
                    var title = "";
                    var coachName = "";
                    Object.keys(data).forEach(function (item) {
                        if (title) title += ',  ';
                        title += moment(parseInt(item)).format('DD.MM.YYYY');
                        if (data[item].coach && coachName.indexOf(data[item].coach.name) == -1) {
                            coachName += (" " + data[item].coach.name);
                        }
                    });
                    return {coaches: coachName, dates: title};
                };

                // заполнение параметра "клиент"
                $http.get(Settings.REST_POINT + '/person/get/' + item.person.uuid
                ).then(function (resp) {
                    $scope.persons = [resp.data];
                    $scope.item.person = resp.data;
                });

                /* получение списка доступных услуг */
                var updateServices = function (callbck) {
                    $http.post(Settings.REST_POINT + '/service/list',
                        {
                            "pageNum": 0,
                            "pageSize": 99999,
                            "filter": null
                        }
                    ).
                    then(function (resp) {
                        $scope.tariffs = [];
                        var tariffsFirst = [];
                        $scope.services = resp.data.items;
                        $scope.services.forEach(function (service) {
                            if (item.service && service.id == item.service.id) {
                                item.service = service; // обогащаем service у item
                            }
                            service.tariffs.forEach(function (tariff, index) {
                                if (tariff.deleted && (!item.tariff || tariff.uuid != item.tariff.uuid)) {
                                    service.tariffs.splice(index, 1);
                                    return;
                                }

                                tariff.name = /*service.name + ' → ' + */tariff.name;
                                //tariff.nameForFilter = service.name + ' → ' + tariff.name;
                                tariff.serviceName = service.name;
                                if (item.service && service.id == item.service.id) {
                                    tariffsFirst.push(tariff);
                                } else {
                                    $scope.tariffs.push(tariff);
                                }
                            });
                        });
                        // в начало ставим тарифы той услуги, которая была в прошлый раз
                        $scope.tariffs = tariffsFirst.concat($scope.tariffs);

                        if (item.tariff) {
                            $scope.tariffs.forEach(function (element) {
                                if (item.tariff.uuid == element.uuid) {
                                    $scope.item.tariff = element;
                                }
                            })
                        }

                        callbck();
                    });
                };

                /* получение списка тренеров */
                var updateCoaches = function (callbck) {
                    $http.post(Settings.REST_POINT + '/coach/list',
                        {
                            "pageNum": 0,
                            "pageSize": 999,
                            "filter": null
                        }
                    ).
                    then(function (resp) {
                        $scope.coaches = resp.data.items;
                        $scope.coaches.unshift({name: "-", uuid: '0'});
                        if (item.coach) {
                            $scope.coaches.forEach(function (element, index, array) {
                                if (item.coach.uuid == element.uuid) {
                                    $scope.item.coach = element;
                                }
                            })
                        }
                        $scope.coachesForSelect = $scope.coaches;

                        callbck();
                    });
                };

                updateServices(
                    function () {
                        updateCoaches(
                            function () {
                                $scope.updateCoachesForService();
                                $scope.refreshForm(false); // false для инициализации чекбоксов при редактировании
                            }
                        )
                    }
                );
            },
            resolve: {
                item: function () {
                    return $scope.item;
                }
            }
        });

    };
});

vipcodApp.controller("PaymentCompensationController", function ($scope, $rootScope, $uibModal, $http, $log, $location, $uibModalInstance, alertService, item) {

    $scope.p60 = function () {
        item.backSum *= 0.6;
    };

    $scope.item = item;

    $http.get(Settings.REST_POINT + '/payment/last/' + item.person.uuid + "/" + 0
    ).then(function (resp) {
        $scope.lastPayments = resp.data;

        for (var idx = 0; idx < $scope.lastPayments.length;) {
            var pay = $scope.lastPayments[idx];
            pay.periodName = Utils.getMonthById(pay.period).name;
            pay.periodName = pay.periodName.charAt(0).toUpperCase() + pay.periodName.substring(1);
            if (pay.uuid == item.uuid) {
                $scope.lastPayments.splice(idx, 1);
            } else {
                ++idx;
            }

            $scope.lastPayments.forEach(function (pay) {
                pay.reservations.forEach(function (resv) {
                    item.compensatedReservations.forEach(function (compResv) {
                        if (resv.id == compResv.id) {
                            resv.compensated = 'YES';
                        }
                    });
                });
            });

        }
    });

    $scope.close = function () {
        $uibModalInstance.close(item.backSum);
    };

    $scope.toggle = function () {
        item.backSum = 0;
        item.compensatedReservations = [];

        $log.log("----");
        $scope.lastPayments.forEach(function (pay) {
            var reservations = pay.reservations;
            if (reservations) {
                reservations.forEach(function (resrv) {
                    $log.log(resrv);
                    if (resrv.compensated && (!resrv.paymentCompensator || resrv.paymentCompensator.uuid == item.uuid)) {
                        item.backSum += resrv.payment.tariff.priceOne;
                        if (item.compensatedReservations.indexOf(resrv) == -1) {
                            item.compensatedReservations.push(resrv);
                        }
                    }
                });
            }
        });
    };
});
