vipcodApp.controller('CoachesController', function ($scope, $rootScope, $http, $log, $location) {

    onEvent($rootScope, 'event_showCoaches', function (event, item) {
        $scope.showPage();
    });

    $scope.countPerPage = 20;
    $scope.currentPage = 1;
    $scope.maxSize = 5;
    $scope.currentFilter = '';

    $scope.$watch('currentPage', function () {
        $scope.showPage()
    });

    $scope.edit = function (item) {
        $rootScope.$emit('event_editCoach', item);
    };


    $scope.showPage = function () {
        $http.post(Settings.REST_POINT + '/coach/list',
            {
                "pageNum": $scope.currentPage - 1,
                "pageSize": $scope.countPerPage,
                "filter": $scope.currentFilter
            }
        ).then(function (resp) {
            $scope.items = resp.data.items;
            $scope.totalItems = resp.data.total;
        }).catch(function (resp) {
            if (Utils.verifyUser(resp.status, $location)) return;
            Utils.showErrorMessage(resp);
        });
    };
});


vipcodApp.controller("ModalCoachController", function ($scope, $rootScope, $uibModal, $log, $location) {

    onEvent($rootScope, 'event_editCoach', function (event, item) {
        $scope.item = angular.copy(item);
        $scope.open();
    });

    $scope.openNew = function () {
        $scope.item = {};
        $scope.open();
    };

    $scope.open = function () {
        var modalWin = $uibModal.open({
            templateUrl: 'views/modal/modalCoachContent.html?bust=' + Math.random().toString(36).slice(2),
            backdrop: true,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log, $http, item) {
                $scope.multipleServices = {};
                $scope.multipleServices.selectedServices = [];

                $scope.item = item;
                $scope.submit = function () {
                    item.services = $scope.multipleServices.selectedServices;

                    $http.post(Settings.REST_POINT + '/coach/save', item).
                    then(function (resp) {
                        if (resp.data.error) {
                            alert(resp.data.error);
                        } else {
                            $scope.item = resp.data.result;
                            $rootScope.$emit('event_showCoaches', $scope.item);
                        }
                    }).
                    catch(function (resp) {
                        if (Utils.verifyUser(resp.status, $location, $uibModalInstance)) return;
                        Utils.showErrorMessage(resp);
                    });


                    $uibModalInstance.dismiss('cancel');
                };

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                /* получение списка доступных тренеру услуг */
                $http.post(Settings.REST_POINT + '/service/list',
                    {
                        "pageNum": 0,
                        "pageSize": 999,
                        "filter": null
                    }
                ).
                then(function (resp) {

                    $scope.service = {};
                    $scope.services = resp.data.items;
                    $scope.multipleServices = {};
                    $scope.multipleServices.selectedServices = [];

                    $scope.services.forEach(function (element, index, array) {
                        var selected = false;
                        if (item.services) {
                            item.services.forEach(function (element1, index1, array1) {
                                if (element1.id == element.id) {
                                    selected = true;
                                }
                            });
                        }

                        if (selected) {
                            $scope.multipleServices.selectedServices.push(element);
                        }
                    });

                    /*$scope.$watch('multiplePosUsers.selectedPeople', function(){
                     var users = $scope.multiplePosUsers.selectedPeople;
                     if (users && users.length > 0) {
                     alert('changed: ' + users);
                     }
                     });*/
                });
            },
            resolve: {
                item: function () {
                    return $scope.item;
                }
            }
        });

    };
});


















