vipcodApp.controller('ClientsController', function ($scope, $rootScope, $http, $log, $location) {

    onEvent($rootScope, 'event_showPersons', function (event, item) {
        $scope.showPage();
    });

    $scope.countPerPage = 20;
    $scope.currentPage = 1;
    $scope.maxSize = 5;
    $scope.currentFilter = $rootScope.currentClientFilter ? $rootScope.currentClientFilter : '';

    $scope.$watch('currentPage', function () {
        $scope.showPage()
    });

    $scope.edit = function (item) {
        $rootScope.$emit('event_editPerson', item);
    };

    $scope.pay = function (item) {
        $location.path('/payments').search({person_id: item.uuid});
    };

    $scope.addPay = function (item) {
        //$location.path('/payments').search({person_id: item.uuid});
        $rootScope.$emit('event_newPayment', item);
    };

    $scope.showPage = function () {
        $http.post(Settings.REST_POINT + '/person/list', {
                "pageNum": $scope.currentPage - 1,
                "pageSize": $scope.countPerPage,
                "filter": $scope.currentFilter
            }
        ).then(function (resp) {
            $rootScope.currentClientFilter = $scope.currentFilter;
            $scope.items = resp.data.items;
            $scope.totalItems = resp.data.total;
        }).catch(function (resp) {
            if (Utils.verifyUser(resp.status, $location)) return;
            Utils.showErrorMessage(resp);
        });
    };
});

vipcodApp.controller("ModalPersonController", function ($scope, $rootScope, $uibModal, $log, $location) {

    onEvent($rootScope, 'event_editPerson', function (event, item) {
        $scope.item = angular.copy(item);
        $scope.open();
    });

    $scope.openNew = function () {
        $scope.item = {};
        $scope.open();
    };

    $scope.open = function () {
        $uibModal.open({
            templateUrl: 'views/modal/modalPersonContent.html?bust=' + Math.random().toString(36).slice(2),
            backdrop: true,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log, $http, item) {

                $scope.item = item;
                $scope.submit = function () {
                    $http.post(Settings.REST_POINT + '/person/save', item)
                        .then(function (resp) {
                            if (resp.data.error) {
                                Utils.showErrorMessage(resp.data.error);
                            } else {
                                $scope.item = resp.data.result;
                                $rootScope.$emit('event_showPersons', $scope.item);
                            }
                            $uibModalInstance.dismiss('cancel');

                        }).catch(function (resp) {
                        if (Utils.verifyUser(status, $location, $uibModalInstance)) {
                            return;
                        }
                        Utils.showErrorMessage(resp);
                    });
                };
                // кнопка "отмена"
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                // кнопка "удалить клиента"
                $scope.delete = function () {
                    //todo: запросить подтверждение и описание причины
                    $http.post(Settings.REST_POINT + '/person/delete', item
                    ).then(function (resp) {
                        if (resp.data.error) {
                            Utils.showErrorMessage(resp.data.error);
                        } else {
                            Utils.showInfoMessage("Запись удалена");
                            $scope.item = resp.data.result;
                            $rootScope.$emit('event_showPersons', $scope.item);
                        }
                        $uibModalInstance.dismiss('cancel');

                    }).catch(function (resp) {
                        if (Utils.verifyUser(resp.status, $location, $uibModalInstance)) return;
                        Utils.showErrorMessage(resp);
                    });
                };
            },
            resolve: {
                item: function () {
                    return $scope.item;
                }
            }
        })
        ;

    }
    ;
})
;
