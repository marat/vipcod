vipcodApp.controller("ModalGetReasonController", function ($scope, $rootScope, $uibModal, $log, $location, alertService) {

    onEvent($rootScope, 'event_getReasonModal', function (event, item, onSubmitCallBack) {
        $scope.item = angular.copy(item);
        $scope.open(onSubmitCallBack);
    });

    $scope.open = function (onSubmitCallBack) {

        var modalWin = $uibModal.open({
            templateUrl: 'views/modal/modalGetReasonContent.html?bust=' + Math.random().toString(36).slice(2),
            backdrop: false,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log, $http, $filter, item) {
                $scope.msg = null;

                $scope.submit = function () {
                    // todo: проверка сообщения
                    onSubmitCallBack($scope.msg, item);
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            },
            resolve: {
                item: function () {
                    return $scope.item;
                }
            }
        });

    }
    ;
})
;






