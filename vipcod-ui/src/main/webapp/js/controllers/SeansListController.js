vipcodApp.controller('SeansListController', function ($scope, $rootScope, $http, $log, $location, alertService) {
    moment.locale('ru');

    onEvent($rootScope, 'event_showSeansList', function (event, item) {
        $scope.showPage();
    });

    $scope.filter = {
        selectionWeekDays: [],
        selectedSessionBegin: null,
        period: 0
    };
    $rootScope.monthList = Utils.monthList.slice(10); //todo: получать из dim_monthyear

    $scope.coach_id = $location.search().coach_id;

    $scope.$watch('currentPage', function () {
        $scope.showPage()
    });

    $scope.onSelectSession = function () {
        $scope.filter.selectedSchedule = undefined;
        $scope.items = [];
        $scope.showPage();
    };
    $scope.onSelectSchedule = function () {
        $scope.showPage();
    };

    //$scope.weekdays = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
    $scope.weekdays = Utils.dayList;

    // Toggle selection for a given WeekDay by name
    $scope.toggleSelectionWeekDays = function toggleSelection(weekday) {
        var idx = $scope.filter.selectionWeekDays.indexOf(weekday);
        if (idx > -1) {
            $scope.filter.selectionWeekDays.splice(idx, 1);
        } else {
            $scope.filter.selectionWeekDays.push(weekday);
        }
        $scope.filter.selectedSessionBegin = '';
        $scope.onSelectSessionBegin();
        $scope.showPage();
    };


    $scope.onSelectPeriod = function () {
        $scope.showPage();
    };

    $scope.onSelectSessionBegin = function () {
        $scope.showPage();
    };

    $scope.showPage = function () {

        if ($scope.coach_id) {
            $http.get(Settings.REST_POINT + '/coach/get/' + $scope.coach_id
            ).then(function (resp) {
                $scope.coach = resp.data;
            });


            if ($scope.filter.period) {
                $http.get(Settings.REST_POINT + '/schedule/coachSessionsBegin/' + $scope.coach_id + '/' + $scope.filter.period + '/' + $scope.filter.selectionWeekDays
                ).then(function (resp) {
                    $log.info(resp.data);
                    $scope.sessionsBegin = resp.data;
                });
            }
        }

        if ($scope.filter.selectedSessionBegin) {
            $http.get(Settings.REST_POINT + '/schedule/personsForSchedule/' + $scope.coach_id + '/' + $scope.filter.selectedSessionBegin + '/' + $scope.filter.selectionWeekDays + '/' + $scope.filter.period
            ).then(function (resp) {
                $scope.items = resp.data.items;
                $scope.totalItems = resp.data.total;
                if ($scope.person_id) {
                    $scope.person = resp.data.person;
                }

            }).catch(function (resp) {
                if (Utils.verifyUser(resp.status, $location)) return;
                Utils.showErrorMessage(resp);
            });
        } else {
            $scope.items = null;
            $scope.totalItems = null;
        }
    };

    $scope.export = function () {
        location = Settings.REST_POINT + "/schedule/personsForSchedule/" + $scope.coach_id + '/' + $scope.filter.selectedSessionBegin + '/' + $scope.filter.selectionWeekDays + '/' + $scope.filter.period + ".xlsx"
    };
});
