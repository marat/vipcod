vipcodApp.controller('AbonsController', function ($scope, $rootScope, $http, $log, $location) {

    onEvent($rootScope, 'event_showAbons', function (event, item) {
        $scope.showPage();
    });


    $scope.countPerPage = 20;
    $scope.currentPage = 1;
    $scope.maxSize = 5;
    $scope.currentFilter = '';

    $scope.$watch('currentPage', function () {
        $scope.showPage()
    });

    $scope.edit = function (item) {
        $rootScope.$emit('event_editAbon', item);
    };


    $scope.showPage = function () {
        $http.post(Settings.REST_POINT + '/tariff/list',
            {
                "pageNum": $scope.currentPage - 1,
                "pageSize": $scope.countPerPage,
                "filter": $scope.currentFilter
            }
        ).then(function (resp) {
            $scope.items = resp.data.items;
            $scope.totalItems = resp.data.total;
        }).catch(function (resp) {
            if (Utils.verifyUser(resp.status, $location)) return;
            Utils.showErrorMessage(resp);
        });
    };
});


vipcodApp.controller("ModalAbonController", function ($scope, $rootScope, $uibModal, $location) {

    onEvent($rootScope, 'event_editAbon', function (event, item) {
        $scope.item = angular.copy(item);
        $scope.open();
    });


    $scope.openNew = function () {
        $scope.item = {};
        $scope.open();
    };

    $scope.open = function () {
        var modalWin = $uibModal.open({
            templateUrl: 'views/modal/modalAbonContent.html?bust=' + Math.random().toString(36).slice(2),
            backdrop: true,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log, $http, item) {
                $scope.item = item;
                $scope.submit = function () {

                    $http.post(Settings.REST_POINT + '/tariff/save', item).
                    then(function (resp) {
                        if (resp.data.error) {
                            alert(resp.data.error);
                        } else {
                            $scope.item = resp.data.result;
                            $rootScope.$emit('event_showAbons', $scope.item);
                        }
                    }).
                    catch(function (resp) {
                        if (Utils.verifyUser(resp.status, $location, $uibModalInstance)) return;
                        Utils.showErrorMessage(resp);
                    });


                    $uibModalInstance.dismiss('cancel');
                };
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            },
            resolve: {
                item: function () {
                    return $scope.item;
                }
            }
        });

    };
});
