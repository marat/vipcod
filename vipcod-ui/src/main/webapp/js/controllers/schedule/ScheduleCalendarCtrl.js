/**
 *
 */
vipcodApp.controller('ScheduleCalendarCtrl', function ScheduleCalendarCtrl($scope, $compile, $location, uiCalendarConfig) {

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    /* event source that calls a function on every view switch */
    $scope.eventSource = {
        url: Settings.REST_POINT + '/schedule/get',
        type: 'GET',
        className: 'EventResourcePane',
        data: function () {
            return {
                view: $scope.currentView
            }
        },
        contentType: 'application/json',
        error: function (data) {
            Utils.showErrorMessage(data.statusText);
            if (Utils.verifyUser(data.status, $location)) return;
        },
        eventDataTransform: $scope.eventDataTransform
    };


    $scope.alertOnEventClick = function (date, jsEvent, view) {
        alert(date.title + ' was clicked. Type: ' + date.type);
        uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', date);
    };
    $scope.alertOnDrop = function (event, delta, revertFunc, jsEvent, ui, view) {
        alert('Event Droped to make dayDelta ' + delta);
    };
    $scope.alertOnResize = function (event, delta, revertFunc, jsEvent, ui, view) {
        alert('Event Resized to make dayDelta ' + delta);
    };

    /* Change View */
    $scope.changeView = function (view, calendar) {
        $scope.currentView = view;
        uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
    };
    /* Change View */
    $scope.renderCalender = function (calendar) {
        if (uiCalendarConfig.calendars[calendar]) {
            uiCalendarConfig.calendars[calendar].fullCalendar('render');
        }
    };
    /* Render Tooltip */
    $scope.eventRender = function (event, element, view) {
        element.attr({
            'tooltip': event.title,
            'tooltip-append-to-body': true
        });
        $compile(element)($scope);
    };


    $scope.eventDataTransform = function (eventData) {
        var start = eventData.start?moment(eventData.start):moment(eventData.date);
        var end = eventData.end?moment(eventData.end):moment(eventData.date);

        var resp = {
            id: eventData.id,
            title: eventData.title?eventData.title:(eventData.coach?eventData.coach.name:''),
            subtitle: eventData.subtitle?eventData.subtitle:(eventData.service?eventData.service.name:''),
            allDay: eventData.start?false:true,
            start: start,
            end: end
        };
        return resp;
    };

    /* config object */
    $scope.uiConfig = {
        calendar: {
            defaultView: 'agendaWeek',
            height: 700,
            editable: false,
            header: {
                left: 'title',
                center: '',
                right: 'today prev,next'
            },
            allDaySlot: false,
            slotEventOverlap: false, // не даем плашкам событий накрывать друг друга
            eventClick: $scope.alertOnEventClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender,

            minTime: "06:00:00",
            maxTime: "22:00:00",
            slotDuration: '00:30:00',
            lang: 'ru',
            axisFormat: 'H:mm',
            views: {
                agendaDay: {
                    slotDuration: '00:30:00'
                },
                agendaWeek: {
                    slotDuration: '00:15:00'
                },
                month: {
                    columnFormat: 'ddd'
                }
            },
            columnFormat: 'ddd [(]D MMM[)]',
            lazyFetching: false
        }
    };

    $scope.currentView = $scope.uiConfig.calendar.defaultView;

    /* event sources array*/
    $scope.eventSources = [$scope.eventSource];
});

/* EOF */