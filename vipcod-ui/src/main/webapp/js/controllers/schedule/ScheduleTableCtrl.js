/**
 *
 */
scheduleTableCtrl = vipcodApp.controller('ScheduleTableCtrl', function ScheduleTableCtrl($scope, $http, $location, $log, alertService) {

    $scope.sortType = 'date'; // set the default sort type
    $scope.sortReverse = false;  // set the default sort order

    $scope.schedule = {};
    $scope.sessionsNames = [];
    $scope.forDelete = [];

    $scope.filterObject = {};


    $scope.selectedDates = [];
    $scope.highlightDays = [];
    $scope.allSelectedSign = false;

    this.activeDate = new Date();
    this.selectedDates = $scope.selectedDates;
    this.type = 'individual';


    //selectedDates
    $scope.$watch('selectedDates', function () {
        $scope.refresh();
    }, true);

    $scope.refresh = function () {
        $log.log("refresh");

        /* получение расписания */
        $http.post(Settings.REST_POINT + '/schedule/table',
            dateTruncTimezoneToUtc($scope.selectedDates)
        ).then(function (resp) {
            $scope.items = resp.data;
        });
    };
    $scope.refresh();

    /* получаем дни, для которых расписание уже было утверждено */
    var refreshHighlightDays = function () {
        $scope.highlightDays.length = 0;
        $http.get(Settings.REST_POINT + '/plan/reservedDates'
        ).then(function (resp) {
            resp.data.forEach(function (date) {
                $scope.highlightDays.push({
                    date: date,
                    css: 'reserved',
                    selectable: true,
                    title: '...'
                });
            });
        });
    };
    refreshHighlightDays();


    /* получение списка доступных сеансов */
    $http.get(Settings.REST_POINT + '/references/sessions',
        {params: {
            "pageNum": 0,
            "pageSize": 99999,
            "filter": null
        }}
    ).
    then(function (resp) {
        $scope.sessions = resp.data.items;
        $scope.sessionsNames = [];
        $scope.selectedSessionName = null;

        $scope.sessions.forEach(function (sess) {
            if ($scope.schedule.session && sess.id == $scope.schedule.session.id) {
                $scope.schedule.session = sess; // обогащаем
            }

            if (!Utils.contains($scope.sessionsNames, sess.name)) {
                $scope.sessionsNames.push(sess.name);
            }
        });
        $scope.sessionsNames.sort();

    });

    /* получение списка доступных услуг */
    $http.post(Settings.REST_POINT + '/service/list',
        {
            "pageNum": 0,
            "pageSize": 99999,
            "filter": null
        }
    ).
    then(function (resp) {
        $scope.services = resp.data.items;
        $scope.services.forEach(function (service) {
            if ($scope.schedule.service && service.id == $scope.schedule.service.id) {
                $scope.schedule.service = service; // обогащаем
            }
        });
    });

    /* получение списка тренеров */
    $http.post(Settings.REST_POINT + '/coach/list',
        {
            "pageNum": 0,
            "pageSize": 999,
            "filter": null
        }
    ).then(function (resp) {
        $scope.coaches = resp.data.items;
        $scope.coaches.unshift({name: "-", uuid: ''});
        if ($scope.schedule.coach) {
            $scope.coaches.forEach(function (element, index, array) {
                if ($scope.schedule.coach.uuid == element.uuid) {
                    $scope.schedule.coach = element;
                }
            })
        }
    });

    $scope.select = function (item) {
        $scope.selectedSessionName = item.session.name;
        $scope.schedule.service = item.service;
        $scope.schedule.coach = item.coach;
        $scope.schedule.limit = item.limit;
        //$scope.refresh();
    };


    $scope.onSelectService = function () {
        //alert('onSelectService');
    };
    $scope.onSelectSession = function () {
        //alert('onSelectSession');
    };
    $scope.onSelectCoach = function () {
        //alert('onSelectCoach');
    };
    $scope.onSelectDate = function () {
        //alert('onSelectDate');
        $scope.sessionsNames = [];
        $scope.sessions.forEach(function (sess) {
            var dayOfWeek = Utils.dayOfWeek($scope.schedule.date);
            if (sess.dayOfWeek == dayOfWeek) {
                $scope.sessionsNames.push(sess);
            }
        });

        $scope.schedule.session = null;
    };

    /**
     * вычисление значения общего для всех записей чекбокса
     */
    $scope.updateAllSelectedSign = function () {
        var items = $scope.items;
        var sel = items.filter(function (o) {
            return o.selected
        }).length;
        if (sel == items.length) $scope.allSelectedSign = true;
        else if (sel == 0) $scope.allSelectedSign = false;
        else $scope.allSelectedSign = null;

        //alert('updateAllSelectedSign: ' + sel + "  " + items.length);
    };

    /**
     * клик по общему чекбоксу
     * @param items - текущие значения в таблице
     * @param allSelectedItems - выборка всех отмеченных элементов
     */
    $scope.toggle = function (items, allSelectedItems) {
        if (!items) return;

        var forDeleteCount = allSelectedItems.length;

        if (Object.prototype.toString.call(items) === '[object Array]') {
            var noSelectedElemenets = items.filter(function (o) {
                return allSelectedItems.indexOf(o) < 0
            });

            // если есть неотмеченные - отмечаем все, иначе снимаем отметку со всех
            var t = (noSelectedElemenets.length > 0);
            items.forEach(function (o) {
                o.selected = t
            });
            if (t) {
                forDeleteCount += noSelectedElemenets.length;
            } else {
                forDeleteCount -= (items.length - noSelectedElemenets.length);
            }
        } else {
            items.selected = !items.selected;
            forDeleteCount += (items.selected ? 1 : -1);
        }
    };

    $scope.exists = function (item, list) {
        if (!item) return false;
        if (Object.prototype.toString.call(item) === '[object Array]') {
            for (var i in item) {
                if (list.indexOf(i) < 0) return true;
            }
            return false;
        }
        return list.indexOf(item) > -1;
    };

    /**
     * кнопка "удалить"
     */
    $scope.deleteItems = function (itemsForDelete) {

        $http.post(Settings.REST_POINT + '/schedule/delete', itemsForDelete)
            .then(function (resp) {
                if (resp.data.error) {
                    Utils.showErrorMessage(resp.data.error, alertService, 10000);
                    resp.data.result.forEach(function (err) {
                        Utils.showWarnMessage(
                            moment(err.date).format('ll') + ", " +
                            err.session.begin + ", " +
                            err.coach.name + ", " +
                            err.service.name,
                            alertService, 10000);
                    });
                } else {
                    $scope.refresh();
                }
                refreshHighlightDays();
            }).catch(function (resp) {
            if (Utils.verifyUser(resp.status, $location)) return;
            Utils.showErrorMessage(resp.data, alertService);
            refreshHighlightDays();
        });

    };

    function dateTruncTimezoneToUtc(dates) {
        var result = [];

        if (Array.isArray(dates)) {
            dates.forEach(function (d) {
                result.push(moment.utc(moment(d).format('YYYYMMDD'), "YYYYMMDD"));
            });
            return result;
        } else {
            return moment.utc(moment(dates).format('YYYYMMDD'), "YYYYMMDD");
        }


    }
});

/* EOF */