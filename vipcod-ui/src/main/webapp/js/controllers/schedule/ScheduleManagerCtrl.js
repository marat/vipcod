/**
 *
 */
vipcodApp.controller('ScheduleManagerCtrl', function ScheduleManagerCtrl($scope, $rootScope, $http, $log, $compile, $interval, $location, alertService) {

    // утвердить расписание
    $scope.accept = function (planSchedule) {
        $rootScope.$emit('event_acceptSchedule', planSchedule);
    };

    // сохранить расписание в файл
    $scope.saveToPc = function (planSchedule) {
        Utils.saveToPc(planSchedule, moment().format("YYYY.MM.DD_HH.mm.ss") + ".schedule");
    };

    // загрузить расписание из файла
    $scope.handleFileSelect = function (evt) {
        var reader = new FileReader();
        var fileData = evt.target.files[0]; // FileList object

        // Closure to capture the file information.
        reader.onload = (function (theFile) {
            return function (e) {
                loadReferenceDataForSchedule(angular.fromJson(e.target.result));
                //$scope.planSchedule = angular.fromJson(e.target.result);
                autosave();
            };
        })(fileData);

        reader.readAsText(fileData);
    };
    document.getElementById('fileLoader').addEventListener('change', $scope.handleFileSelect, false);

    // автосохранение текущего плана
    var autosave = function () {
        $log.log("autosave");

        $http.post(Settings.REST_POINT + '/plan/save',
            {
                "name": $scope.currentPlanName,
                "plan": $scope.planSchedule
            }
        ).then(function (resp) {

        }).catch(function (resp) {
            if (Utils.verifyUser(resp.status, $location)) return;
            Utils.showErrorMessage(resp.data, alertService);
        });
    };
    $scope.Timer = $interval(function () {
        autosave();
    }, 10000);

    $scope.$on("$destroy", function () {
        if (angular.isDefined($scope.Timer)) {
            $interval.cancel($scope.Timer);
            autosave();
            $log.log('$destroy');
        }
    });

    $scope.planSchedule = {};
    var planScheduleOld = null;

    // грузим сохраненное состояние плана
    $scope.currentPlanName = "default";
    $http.get(Settings.REST_POINT + '/plan/get/' + $scope.currentPlanName
    ).then(function (resp) {
        planScheduleOld = Object.keys(resp.data).length ? resp.data : null;
        loadReferenceDataForSchedule(planScheduleOld);

    }).catch(function (resp) {
        if (Utils.verifyUser(resp.status, $location)) return;
        Utils.showErrorMessage(resp.data, alertService);
    });

    var loadReferenceDataForSchedule = function (planScheduleOld) {
        // грузим услуги
        $http.get(Settings.REST_POINT + '/service/getAll'
        ).then(function (resp) {
            $scope.services = {};
            resp.data.forEach(function (serv) {
                $scope.services[serv.id] = serv;
                $scope.planSchedule[serv.id] = {};
            });
        });

        // грузим тренеров
        $http.get(Settings.REST_POINT + '/coach/getAll'
        ).then(function (resp) {
            $scope.coaches = {};
            $scope.serviceCoachMap = {};

            var fillDays = function (item, planScheduleOld, servId, coachUuid) {
                for (var i = 1; i <= 7; ++i) {
                    try {
                        item[i] = {items: planScheduleOld[servId][coachUuid][i].items};
                    } catch (err) {
                        //$log.error(err);
                        item[i] = {items: []};
                    }
                }
            };

            resp.data.forEach(function (coach) {
                $scope.coaches[coach.uuid] = coach;

                coach.services.forEach(
                    function (serv) {
                        if (!$scope.serviceCoachMap[serv.id]) $scope.serviceCoachMap[serv.id] = [];
                        $scope.serviceCoachMap[serv.id].push(coach);
                        if (!$scope.planSchedule[serv.id]) $scope.planSchedule[serv.id] = {};
                        $scope.planSchedule[serv.id][coach.uuid] = {};
                        fillDays($scope.planSchedule[serv.id][coach.uuid], planScheduleOld, serv.id, coach.uuid);
                    }
                );
            });

            // без тренера
            Object.keys($scope.planSchedule).forEach(
                function (serv) {
                    if (Object.keys($scope.planSchedule[serv]).length == 0) {
                        $scope.planSchedule[serv]['0'] = {};
                        $scope.serviceCoachMap[serv] = [{name: 'без тренера', shortName: 'без тренера', uuid: '0'}];
                        fillDays($scope.planSchedule[serv]['0'], planScheduleOld, serv, '0');
                    }
                }
            );

        });

        // грузим сеансы
        $http.get(Settings.REST_POINT + '/references/sessions'
        ).then(function (resp) {
            $scope.sessions = {};

            // data.items dayOfWeek/name

            resp.data.items.forEach(function (sess) {
                if (!$scope.sessions[sess.dayOfWeek]) $scope.sessions[sess.dayOfWeek] = [];
                $scope.sessions[sess.dayOfWeek].push(sess);
            });
        });
    };

    $scope.showScheduleEditPanel = function (el, servId, coachUid, dayWeek) {
        var selectorContent =
            "<ui-select id=\"scheduleSelector\" name=\"sess\" multiple ng-model=\"planSchedule['" + servId + "']['" + coachUid + "'][" + dayWeek + "].items\" " +
            "theme=\"bootstrap\" ng-disabled=\"disabled\" style='\"position: relative\"'>" +
            "<ui-select-match placeholder=\"- нет занятий -\">{{$item.begin}}</ui-select-match>" +
            "<ui-select-choices repeat=\"sess in sessions[" + dayWeek + "] | filter: {name: $select.search}\">" +
            "<div ng-bind-html=\"sess.name | highlight: $select.search\"></div>" +
            "</ui-select-choices>" +
            "</ui-select>";

        if ($scope.planSchedule['last']) {
            $scope.planSchedule['last']['hide'] = false;
            $("#scheduleSelector").remove();
        }

        $log.log(el, servId, coachUid, dayWeek);

        var selement = $compile(selectorContent)($scope);
        $(el.currentTarget.parentElement).append(selement);

        $scope.planSchedule['last'] = $scope.planSchedule[servId][coachUid][dayWeek];
        $scope.planSchedule['last']['hide'] = true;
    };
});


var vm = vipcodApp.controller("ModalScheduleAcceptController", function ($scope, $rootScope, $uibModal, $http, $log, $location, alertService) {

    onEvent($rootScope, 'event_acceptSchedule', function (event, planSchedule) {
        //$scope.planSchedule = angular.copy(planSchedule);
        $scope.open(planSchedule);
    });


    function addOrRemove(array, value) {
        var index = array.indexOf(value);
        if (index === -1) {
            array.push(value);
        } else {
            array.splice(index, 1);
        }
    }

    $scope.open = function (planSchedule) {
        var modalWin = $uibModal.open({
            templateUrl: 'views/modal/modalScheduleAcceptContent.html?bust=' + Math.random().toString(36).slice(2),
            backdrop: true,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log, $http, $q, $timeout) {
                $scope.errorList = [];

                $scope.selectedDates = [];
                $scope.selectedDiapazoneMode = true;
                $scope.highlightDays = [];
                this.selectedDates = $scope.selectedDates;


                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };


                Date.prototype.addDays = function (days) {
                    var dat = new Date(this.valueOf())
                    dat.setDate(dat.getDate() + days);
                    return dat;
                };
                Date.prototype.delDays = function (days) {
                    var dat = new Date(this.valueOf())
                    dat.setDate(dat.getDate() - days);
                    return dat;
                };

                function getDates(startDate, stopDate) {
                    var dateArray = [];
                    var currentDate = startDate;
                    while (currentDate <= stopDate) {
                        dateArray.push(new Date(currentDate))
                        currentDate = currentDate.addDays(1);
                    }
                    return dateArray;
                };


                var refreshHighlightDays = function () {
                    $scope.highlightDays.length = 0;
                    $http.get(Settings.REST_POINT + '/plan/reservedDates'
                    ).then(function (resp) {
                        resp.data.forEach(function (date) {
                            $scope.highlightDays.push({
                                date: date,
                                css: 'reserved',
                                selectable: true,
                                title: '...'
                            });
                        });
                    });
                };
                refreshHighlightDays();

                $scope.daySelectionClick = function (event, clieckedDay) {
                    if ($scope.selectedDiapazoneMode) {
                        if ($scope.selectedDates.length == 1) {
                            var date = clieckedDay.date;
                            // выбираем диапазон
                            var dateFrom = $scope.selectedDates[0];
                            if (date < dateFrom) {
                                var d = date;
                                date = dateFrom;
                                dateFrom = d;
                            }

                            $scope.selectedDates.length = 0;
                            Array.prototype.push.apply(
                                $scope.selectedDates,
                                getDates(dateFrom.toDate(), date.toDate().delDays(1)));
                        } else {
                            $scope.selectedDates.length = 0;
                            clieckedDay.mdp.selected = false;
                        }
                    } else {
                        // не диапазоны
                        //addOrRemove($scope.selectedDates, clieckedDay.date);
                    }
                };

                $scope.submit = function () {
                    $rootScope.progressbar.start();
                    $rootScope.progressbar.setHeight('5px');

                    $scope.sendSaveRequestXhr = function (date, serv_id, coach_uuid, sess) {

                        var deferred = $q.defer();
                        $http.post(Settings.REST_POINT + '/schedule/add',
                            {
                                dates: [date],
                                sessionBegin: sess.begin,
                                service: {id: serv_id},
                                coach: {uuid: coach_uuid},
                                limit: 999
                            }
                        ).then(function (resp) {
                            if (resp.data.error) {
                                $scope.errorList.push(moment(date).format('DD.MM.YYYY') + ": " + resp.data.error);
                            }
                            deferred.resolve('request successful');
                            $scope.highlightDays.push({
                                date: date,
                                css: 'reserved',
                                selectable: true,
                                title: '...'
                            });
                        }).catch(function (resp) {
                            if (Utils.verifyUser(resp.status, $location)) return;
                            $scope.errorList.push(moment(date).format('DD.MM.YYYY') + ": " + resp.data.error);
                            deferred.resolve('request successful');
                        });

                        return deferred.promise;
                    };


                    var sended = false;

                    $scope.allPromiseArray = [];

                    function saveDate(dateLocal) {
                        {
                            var promiseArray = [];

                            var date = new Date(dateLocal);
                            date.setMinutes(date.getMinutes() - date.getTimezoneOffset());

                            var dayOfWeek = Utils.dayOfWeek(date);

                            Object.keys(planSchedule).forEach(function (servId) {
                                Object.keys(planSchedule[servId]).forEach(function (coachUuid) {

                                    if (planSchedule[servId][coachUuid][dayOfWeek]) {

                                        if (!planSchedule[servId][coachUuid][dayOfWeek].items) {
                                            $log.error("items is undefinded: " + servId + " " + coachUuid + " " + dayOfWeek);
                                            planSchedule[servId][coachUuid][dayOfWeek].items = [];
                                        }

                                        planSchedule[servId][coachUuid][dayOfWeek].items.forEach(function (sess) {
                                            promiseArray.push(
                                                $scope.sendSaveRequestXhr(date, servId, coachUuid, sess)
                                            );
                                            sended = true;
                                        });
                                    }
                                })
                            });

                            $scope.allPromiseArray.push(
                                $q.all(promiseArray).then(function (arrayOfResults) {
                                    $log.log("arrayOfResults: ", arrayOfResults);
                                    $scope.selectedDates.splice($scope.selectedDates.indexOf(dateLocal), 1);
                                    goNext();
                                })
                            );
                        }
                    }

                    var maxBar = $scope.selectedDates.length;

                    var goNext = function () {
                        $rootScope.progressbar.set((maxBar - $scope.selectedDates.length) * 100.0 / maxBar);

                        if ($scope.selectedDates.length == 0) {
                            $q.all($scope.allPromiseArray).then(function () {
                                $scope.allPromiseArray.length = 0;
                                if (!sended) {
                                    Utils.showWarnMessage("Нет расписания для выбранных дней", alertService);
                                }
                                $rootScope.progressbar.complete();
                                Utils.showInfoMessage("Завершено", alertService);
                                refreshHighlightDays();
                            });
                        } else {
                            $timeout(function () {
                                saveDate($scope.selectedDates[0]);
                            }, 1);
                        }
                    };

                    goNext();
                }
            }
        });
    };
});
