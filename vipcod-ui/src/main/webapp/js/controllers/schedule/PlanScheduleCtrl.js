/**
 *
 */

var PlanFormConfig = {
    timeFormat: 'HH:mm',
    dayWeekFormat: 'ddd'
};

vipcodApp.controller('PlanScheduleCtrl', function PlanScheduleCtrl($scope, $rootScope, $compile, $location, $http, uiCalendarConfig, alertService, $log) {

    onEvent($rootScope, 'event_addEvent', function (event, item) {
        $log.log(item);
        $scope.selectedSessionSource.splice(0, 1);
        $scope.newEventsSource.push($scope.eventDataTransform(item));
    });

    // контекстные меню
    $scope.selected = 'None';
    $scope.items = [
        {name: 'John', otherProperty: 'Foo'},
        {name: 'Joe', otherProperty: 'Bar'}];
    $scope.menuOptions = [
        ['Select', function ($itemScope) {
            $scope.selected = $itemScope.item.name;
        }],
        null, // Dividier
        ['Remove', function ($itemScope) {
            $scope.items.splice($itemScope.$index, 1);
        }]
    ];


    $scope.selectedSession = undefined;
    $scope.selectedSessionDay = 1;
    $scope.selectedSessionSource = [];
    $scope.newEventsSource = [];

    $rootScope.coaches = [];
    $scope.sessions = {};

    $scope.sessionDays = [
        {key: 1, label: 'Пн'},
        {key: 2, label: 'Вт'},
        {key: 3, label: 'Ср'},
        {key: 4, label: 'Чт'},
        {key: 5, label: 'Пт'},
        {key: 6, label: 'Сб'},
        {key: 7, label: 'Вс'}
    ];

    // справочник сеансов
    $http.get(Settings.REST_POINT + '/references/sessions'
    ).then(function (resp) {
        resp.data.items.forEach(function (sess) {
            if (!$scope.sessions[sess.dayOfWeek]) $scope.sessions[sess.dayOfWeek] = [];
            $scope.sessions[sess.dayOfWeek].push(sess);
        });
    });

    // список тренеров
    $http.post(Settings.REST_POINT + '/coach/list',
        {
            pageNum: 0,
            pageSize: 9999
        }
    ).then(function (resp) {
        $rootScope.coaches = resp.data.items;
    });

    $scope.numToDayName = function (i) {
        if (i == 1) return 'ПН';
        if (i == 2) return 'ВТ';
        if (i == 3) return 'СР';
        if (i == 4) return 'ЧТ';
        if (i == 5) return 'ПТ';
        if (i == 6) return 'СБ';
        if (i == 7) return 'ВС';
    };

    $scope.onEventClick = function (item, jsEvent, view) {
        $log.log("onEventClick: ", item);
        selectSessionByMoment(item.start);
        $rootScope.$emit('event_editEvent', item, $scope.selectedSession);
    };

    /* Change View */
    $scope.changeView = function (view, calendar) {
        $log.log(view);
        $scope.currentView = view;
        if (!calendar) calendar = 'planCalendar';
        uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
    };


    /**
     * по моменту определяет день, набор сеансов этого дня и сенас для момента @moment
     * @param moment
     */
    var selectSessionByMoment = function (moment) {
        $log.log("selectSessionByMoment: ", moment);
        $scope.selectedSessionDay = 1 + parseInt(moment.format("e")); // выбранный день
        var clickedTime = moment.format(PlanFormConfig.timeFormat);

        if (!$scope.sessions) alert("System dev error: справочник сеансов не загружен!");

        $scope.sessionsTimes = $scope.sessions[$scope.selectedSessionDay]; // сеансы выбранного дня
        $scope.sessionsTimes.forEach(function (timeItem) {
            if (timeItem.begin <= clickedTime && timeItem.end > clickedTime) {
                $scope.selectedSession = timeItem; // выбранный сеанс
            }
        });
    };

    /**
     * Обработка клика по календарю
     * @param eventTime
     * @param element
     * @param view
     */
    $scope.onDayClick = function (eventTime, element, view) {
        //alert('dayClick: ' + eventTime.format(PlanFormConfig.timeFormat) + "  " + eventTime.format(PlanFormConfig.dayWeekFormat));
        selectSessionByMoment(eventTime);
        $scope.selectedSessionSource.splice(0, 1);
        $scope.selectedSessionSource.push({
            id: 66666,
            title: '',
            allDay: false,
            color: '#bbb',
            start: eventTime.format("YYYY-MM-DD") + 'T' + $scope.selectedSession.begin,
            end: eventTime.format("YYYY-MM-DD") + 'T' + $scope.selectedSession.end
        });

        uiCalendarConfig.calendars['planCalendar'].fullCalendar('rerenderEvents');
        uiCalendarConfig.calendars['planCalendar'].fullCalendar('gotoDate', eventTime);
        if ($scope.currentView == 'agendaWeek') {
            $scope.changeView('agendaDay');
        }
    };


    $scope.eventDataTransform = function (eventData) {
        var start = eventData.start ? moment(eventData.start) : moment(eventData.date);
        var end = eventData.end ? moment(eventData.end) : moment(eventData.date);

        return {
            id: eventData.id,
            title: (eventData.title ? eventData.title : (eventData.coach ? eventData.coach.name : '')),
            subtitle: (eventData.subtitle ? eventData.subtitle : (eventData.service ? eventData.service.name : '')),
            color: '#3a87ad',
            allDay: eventData.start ? false : true,
            start: start,
            end: end
        };
    };

    /* config object */
    $scope.uiConfig = {
        calendar: {
            defaultView: 'agendaWeek',
            height: 700,
            editable: false,
            header: {
                left: '',
                center: '',
                right: ''
            },
            allDaySlot: true, // будем отображать управляющие события
            allDayText: '',
            slotEventOverlap: false, // не даем плашкам событий накрывать друг друга
            eventClick: $scope.onEventClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender,
            dayClick: $scope.onDayClick,

            minTime: "06:00:00",
            maxTime: "22:00:00",
            slotDuration: '00:30:00',
            lang: 'ru',
            axisFormat: PlanFormConfig.timeFormat,
            views: {
                agendaDay: {
                    slotDuration: '00:30:00'
                },
                agendaWeek: {
                    slotDuration: '00:15:00'
                }
            },
            hideHeader: true,
            columnFormat: PlanFormConfig.dayWeekFormat,
            lazyFetching: false,
            defaultDate: moment('9990-01-01')
        }
    };

    $scope.currentView = $scope.uiConfig.calendar.defaultView;

    $scope.showPage = function () {
        uiCalendarConfig.calendars['planCalendar'].fullCalendar('rerenderEvents');
    };

    // сохраненные записи
    $scope.eventSource = {
        url: Settings.REST_POINT + '/plan/get',
        type: 'GET',
        color: 'blue',   // a non-ajax option
        textColor: 'white', // a non-ajax option
        className: 'EventResourcePane',
        data: function () {
            return {
                view: $scope.currentView
            }
        },
        contentType: 'application/json',
        error: function (data) {
            if (Utils.verifyUser(data.status, $location)) return;
            Utils.showErrorMessage(data.statusText);
        },
        eventDataTransform: $scope.eventDataTransform
    };
    $scope.eventSources = [$scope.eventSource, $scope.newEventsSource, $scope.selectedSessionSource];

});


vipcodApp.controller("ModalPlanScheduleController", function ($scope, $rootScope, $uibModal, $log, $location) {
    var selectedCoach = [{}, {}, {}];
    var selectedService = [];
    $scope.selectedCoach = selectedCoach;
    $scope.selectedService = selectedService;

    function getCoachUidArray(selectedCoaches) {
        var res = [];
        if (!selectedCoaches) return null;
        selectedCoaches.forEach(function (coach) {
            res.push(coach.uid);
        });
        return res;
    }

    function getIdArray(elements) {
        var res = [];
        if (!elements) return null;
        elements.forEach(function (el) {
            res.push(el.id);
        });
        return res;
    }

    onEvent($rootScope, 'event_editEvent', function (event, item, sessionSelected) {
        $scope.item = {event: angular.copy(item), session: sessionSelected};
        $scope.open();
    });

    $scope.openNew = function () {
        $scope.item = {};
        $scope.open();
    };

    $scope.open = function () {
        $uibModal.open({
            templateUrl: 'views/modal/modalPlanSchedule.html?bust=' + Math.random().toString(36).slice(2),
            backdrop: true,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log, $http, item) {

                $scope.item = item.event;
                $scope.item.sessionTitle = item.event.start.format('HH:mm') + " - " + item.event.end.format('HH:mm');
                $scope.submit = function () {

                    $http.post(Settings.REST_POINT + '/plan/save',
                        {
                            session: item.session.id,
                            coach: getCoachUidArray($scope.selectedCoach),
                            service: getIdArray($scope.selectedService)
                        }
                    ).then(function (resp) {
                        if (resp.data.error) {
                            Utils.showErrorMessage(resp.data.error);
                        } else {
                            // todo: в data.result созданное событие. Обновлять весь календарь не обязательно
                            //uiCalendarConfig.calendars['planCalendar'].fullCalendar('renderEvent', $scope.eventDataTransform(data.result));
                            $rootScope.$emit('event_addEvent', resp.data.result);
                        }
                        $uibModalInstance.dismiss('cancel');

                    }).catch(function (resp) {
                        if (Utils.verifyUser(resp.status, $location)) return;
                        Utils.showErrorMessage(resp);
                    });
                };
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
                $scope.add = function () {
                    selectedCoach.push({});
                };
            },
            resolve: {
                item: function () {
                    return $scope.item;
                }
            }
        });

    };
});


