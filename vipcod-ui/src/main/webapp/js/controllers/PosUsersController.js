vipcodApp.controller('PosUsersController', function ($scope, $rootScope, $http, $log, $location) {

    onEvent($rootScope, 'event_showPosUsers', function (event, item) {
        $scope.showPage();
    });

    $scope.countPerPage = 20;
    $scope.currentPage = 1;
    $scope.maxSize = 5;
    $scope.currentFilter = '';

    $scope.$watch('currentPage', function () {
        $scope.showPage()
    });

    $scope.edit = function (item) {
        $log.log("edit: " + item);
        $rootScope.$emit('event_editPosUser', item);
    };


    $scope.showPage = function () {
        $http.post(Settings.REST_POINT + '/posuser/list',
            {
                "pageNum": $scope.currentPage - 1,
                "pageSize": $scope.countPerPage,
                "filter": $scope.currentFilter
            }
        ).then(function (resp) {
            $log.log("posuser/list success: " + resp.data);
            $scope.items = resp.data.items;
            $scope.totalItems = resp.data.total;
        }).catch(function (resp) {
            if (Utils.verifyUser(resp.status, $location)) return;
            Utils.showErrorMessage(resp);
        });
    };
});


vipcodApp.controller("ModalPosUserController", function ($scope, $rootScope, $uibModal, $log, $location) {

    onEvent($rootScope, 'event_editPosUser', function (event, item) {
        $scope.item = angular.copy(item);
        $scope.open();
    });

    $scope.openNew = function () {
        $scope.item = {};
        $scope.open();
    };

    $scope.open = function () {
        var modalWin = $uibModal.open({
                templateUrl: 'views/modal/modalPosUserContent.html?bust=' + Math.random().toString(36).slice(2),
                backdrop: true,
                windowClass: 'modal',
                controller: function ($scope, $uibModalInstance, $log, $http, item) {
                    $scope.item = item;
                    $scope.submit = function () {
                        $log.log('Submiting user info.');

                        $http.post(Settings.REST_POINT + '/posuser/save', item)
                            .then(function (resp) {
                                if (resp.data.error) {
                                    alert(resp.data.error);
                                } else {
                                    $scope.item = resp.data.result;
                                    $rootScope.$emit('event_showPosUsers', $scope.item);
                                }
                            }).
                        catch(function (resp) {
                            if (Utils.verifyUser(resp.status, $location, $uibModalInstance)) return;
                            Utils.showErrorMessage(resp);
                        });

                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.cancel = function () {
                        $log.log('Canceling user info.');
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve: {
                    item: function () {
                        return $scope.item;
                    }
                }
            })
            ;

    }
    ;
})
;
