// сервис открытия окна редактирования услуги
vipcodApp.factory('editServiceWindow', function ($uibModal, $location, $rootScope, alertService) {
    return {

        // функция открытия окна редактирования услуги
        openServiceWindowForEdit: function ($scope, service) {
            $scope.item = service;

            var modalWin = $uibModal.open({
                templateUrl: 'views/modal/modalServiceContent.html?bust=' + Math.random().toString(36).slice(2),
                backdrop: true,
                windowClass: 'modal',
                controller: function ($scope, $uibModalInstance, $log, $http, item) {
                    $scope.item = item;
                    $scope.submit = function () {
                        $log.log('Submiting user info.');

                        $http.post(Settings.REST_POINT + '/service/save', item).
                        then(function (resp) {
                            if (resp.data.error) {
                                alert(resp.data.error);
                            } else {
                                //$scope.item = resp.data.result;
                                if ($rootScope.showPage) $rootScope.showPage();

                            }
                        }).
                        catch(function (resp) {
                            if (Utils.verifyUser(resp.status, $location)) return;
                            Utils.showErrorMessage(resp.data, alertService);
                        });

                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve: {
                    item: function () {
                        return $scope.item;
                    }
                }
            });
        }
    }
});

// сервис открытия окна редактирования тарифа
vipcodApp.factory('editTariffWindow', function ($uibModal, $location, $rootScope, alertService) {
    return {
        // функция открытия окна редактирования тарифа
        openTariffWindowForEdit: function ($scope, tariff) {
            $scope.item = tariff;

            var modalWin = $uibModal.open({
                templateUrl: 'views/modal/modalTariffContent.html?bust=' + Math.random().toString(36).slice(2),
                backdrop: true,
                windowClass: 'modal',
                controller: function ($scope, $uibModalInstance, $log, $http, item) {
                    $scope.item = item;
                    $scope.submit = function () {
                        $log.log('Submiting user info.');

                        $http({
                            method: 'POST',
                            url: Settings.REST_POINT + '/tariff/' + (tariff.service ? tariff.service.id : 0) + '/save', //TODO: у tariff нет service
                            data: item
                        }).
                        then(function (resp) {
                            if (resp.data.error) {
                                alert(resp.data.error);
                            } else {
                                //$scope.item = resp.data.result;
                                if ($rootScope.showPage) $rootScope.showPage();

                            }
                        }).
                        catch(function (resp) {
                            if (Utils.verifyUser(resp.status, $location)) return;
                            Utils.showErrorMessage(resp.data, alertService);
                        });

                        $uibModalInstance.dismiss('cancel');
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve: {
                    item: function () {
                        return $scope.item;
                    }
                }
            });
        }
    }
});


vipcodApp.controller('ServicesController', function ($scope, $rootScope, $http, $log, $location, editServiceWindow, editTariffWindow, alertService) {

    $scope.tree_data = [];

    $scope.onclick_tree_handler = function (branch) {
        console.log('you clicked on', branch)
    };

    $scope.onselect_tree_handler = function (branch) {
        console.log('you select', branch);
        if (branch.expanded) {
            $rootScope.selectedService = branch.id;
            prepareDataForTreeGrid($scope.tree_data);
        }
    };

    function prepareDataForTreeGrid(items) {
        console.log(items);

        items.forEach(function (serv) {
            serv.children = serv.tariffs;
            serv.expanded = ($rootScope.selectedService == serv.id);
        });

        items.createTariff = $scope.createTariff;
        items.editServiceOrTariff = $scope.editServiceOrTariff;
        items.removeServiceOrTariff = $scope.removeServiceOrTariff;
        return items;
    }

    $rootScope.showPage = function () {
        $http.post(Settings.REST_POINT + '/service/tree',
            {
                "pageNum": $scope.currentPage - 1,
                "pageSize": $scope.countPerPage,
                "filter": $scope.currentFilter
            }
        ).then(function (resp) {
            $log.log("service/list success: " + resp.data);
            $scope.tree_data = prepareDataForTreeGrid(resp.data.items);
        }).catch(function (resp) {
            if (Utils.verifyUser(resp.status, $location)) return;
            Utils.showErrorMessage(resp, alertService);
        });
    };

    $scope.createTariff = function (branch) {
        editTariffWindow.openTariffWindowForEdit($scope, {
            service: {id: branch['id']}
        });
        $rootScope.selectedService = branch['id'];
    };

    $scope.editServiceOrTariff = function (branch) {
        if (branch.price >= 0 || branch.priceOne >= 0) { // отличаем услугу от тарифа по цене: у услуги нет price
            editTariffWindow.openTariffWindowForEdit($scope, branch);
        } else {
            editServiceWindow.openServiceWindowForEdit($scope, branch);
        }
    };

    $scope.removeServiceOrTariff = function (branch) {
        $http.get(Settings.REST_POINT + '/' + (branch.price || branch.priceOne ? 'tariff' : 'service') + '/del',
            {params: {id: branch['id'], uuid: branch['uuid']}}
        ).then(function (resp) {
            alertService.add("success", "Запись удалена успешно", 3000);
            $rootScope.showPage();
        }).catch(function (resp) {
            if (Utils.verifyUser(resp.status, $location)) return;
            Utils.showErrorMessage(resp.data, alertService);
        });
    };

    $rootScope.showPage();
    $scope.plus_button = function (i) {
        alert(i);
    };
});


vipcodApp.controller("ModalServiceController", function ($scope, $rootScope, editServiceWindow) {

    $scope.openNewServiceWindow = function () {
        editServiceWindow.openServiceWindowForEdit($scope, {noNds: false});
    };

});
