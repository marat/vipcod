vipcodApp.controller('PosController', function ($scope, $rootScope, $http, $log, $location) {

    onEvent($rootScope, 'event_showPoses', function (event, item) {
        $scope.showPage();
    });

    $scope.countPerPage = 20;
    $scope.currentPage = 1;
    $scope.maxSize = 5;
    $scope.currentFilter = '';

    $scope.$watch('currentPage', function () {
        $scope.showPage()
    });

    $scope.edit = function (item) {
        $rootScope.$emit('event_editPos', item);
    };


    $scope.showPage = function () {
        $http.post(Settings.REST_POINT + '/pos/list',
            {
                "pageNum": $scope.currentPage - 1,
                "pageSize": $scope.countPerPage,
                "filter": $scope.currentFilter
            }
        ).then(function (resp) {
            $scope.items = resp.data.items;
            $scope.totalItems = resp.data.total;
        }).catch(function (resp) {
            if (Utils.verifyUser(resp.status, $location)) return;
            Utils.showErrorMessage(resp);
        });
    };
});


vipcodApp.controller("ModalPosController", function ($scope, $rootScope, $uibModal, $log, $location) {

    onEvent($rootScope, 'event_editPos', function (event, item) {
        $scope.item = angular.copy(item);
        $scope.open();
    });

    $scope.openNew = function () {
        $scope.item = {};
        $scope.open();
    };

    $scope.open = function () {
        var modalWin = $uibModal.open({
            templateUrl: 'views/modal/modalPosContent.html?bust=' + Math.random().toString(36).slice(2),
            backdrop: true,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log, $http, item) {
                $scope.multiplePosUsers = {};
                $scope.multiplePosUsers.selectedPeople = [];

                $scope.item = item;
                $scope.submit = function () {
                    item.users = $scope.multiplePosUsers.selectedPeople;

                    $http.post(Settings.REST_POINT + '/pos/save', item)
                        .then(function (resp) {
                            if (resp.data.error) {
                                alert(resp.data.error);
                            } else {
                                $scope.item = resp.data.result;
                                $rootScope.$emit('event_showPoses', $scope.item);
                            }
                        }).
                    catch(function (resp) {
                        if (Utils.verifyUser(resp.status, $location, $uibModalInstance)) return;
                        Utils.showErrorMessage(resp);
                    });


                    $uibModalInstance.dismiss('cancel');
                };

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                /* получение списка доступных терминалу пользователей */
                $http.post(Settings.REST_POINT + '/posuser/list',
                    {
                        "pageNum": 0,
                        "pageSize": 999,
                        "filter": null
                    }
                ).then(function (resp) {

                    $scope.person = {};
                    $scope.people = resp.data.items;
                    $scope.multiplePosUsers = {};
                    $scope.multiplePosUsers.selectedPeople = [];

                    $scope.people.forEach(function (element, index, array) {
                        var selected = false;
                        if (item.users) {
                            item.users.forEach(function (element1, index1, array1) {
                                if (element1.uuid == element.uuid) {
                                    selected = true;
                                }
                            });
                        }

                        if (selected) {
                            $scope.multiplePosUsers.selectedPeople.push(element);
                        }
                    });

                    /*$scope.$watch('multiplePosUsers.selectedPeople', function(){
                     var users = $scope.multiplePosUsers.selectedPeople;
                     if (users && users.length > 0) {
                     alert('changed: ' + users);
                     }
                     });*/
                });
            },

            resolve: {
                item: function () {
                    return $scope.item;
                }
            }
        });
    };
});
