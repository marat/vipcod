vipcodApp.controller('DetailDayReportController', function DetailDayReportController($scope, $location, $http) {
        $scope.report = function () {
            $http.get(  Settings.REST_POINT + '/report/daily',
                {params: {
                    date: moment($scope.selectedDate).format('YYYY-MM-DD'),
                    nds:  $scope.selectedNds
                }}
            ).then(function (resp) {
                $scope.items = resp.data.result;
                $scope.sumItems = 0.0;
                resp.data.result.forEach(function(item){
                    $scope.sumItems += item.sum;
                });

            }).catch(function (resp) {
                if (Utils.verifyUser(resp.status, $location)) return;
                Utils.showErrorMessage(resp);
            });
        };

        $scope.export = function(){
            location = Settings.REST_POINT + "/report/daily/" + moment($scope.selectedDate).format('YYYY-MM-DD') + ".xlsx?nds=" + $scope.selectedNds;
        };

        $scope.selectedDate = new Date();
        $scope.selectedNds = "";

        $scope.items = [];
        $scope.sumItems = 0;
        $scope.report();


    }
);
