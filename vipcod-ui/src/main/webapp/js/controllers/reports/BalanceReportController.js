vipcodApp.controller('BalanceReportController', function BalanceReportController($scope, $rootScope, $location, $http, $timeout, ngProgressFactory) {
        $scope.report = function () {
            $rootScope.progressbar.start();

            $http.get(  Settings.REST_POINT + '/report/balance',
                {params: {
                    dateFrom: moment($scope.selectedDateFrom).format('YYYY-MM-DD'),
                    dateTo: moment($scope.selectedDateTo).format('YYYY-MM-DD')
                }}
            ).then(function (resp) {
                $rootScope.progressbar.complete();
                $scope.items = resp.data.result;
            }).catch(function (resp) {
                $rootScope.progressbar.complete();
                if (Utils.verifyUser(resp.status, $location)) return;
                Utils.showErrorMessage(resp);
            });
        };

        $scope.export = function(){
            $rootScope.progressbar.start();
            location = Settings.REST_POINT + "/report/balance/" + moment($scope.selectedDateFrom).format('YYYY-MM-DD') + "/" + moment($scope.selectedDateTo).format('YYYY-MM-DD') + ".xlsx";
            $timeout(function(){$rootScope.progressbar.complete();}, 5000);
        };

        $scope.selectedDateFrom = Utils.beginOfMonthDate();
        $scope.selectedDateTo = Utils.endOfMonthDate();

        $scope.items = [];
        $scope.report();
    }
);
