/**
 * @ngdoc directive
 * @name ngSeed.directives:pwCheck
 * @description
 * Simply checks if two passwords match.
 */
vipcodApp.directive('pwCheck', function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            var firstPassword = '#' + attrs.pwCheck;
            $(elem).add(firstPassword).on('keyup', function () {
                scope.$apply(function () {
                    var v = elem.val() === $(firstPassword).val();
                    ctrl.$setValidity('pwcheck', v);
                });
            });
        }
    }
});


vipcodApp.controller('RegistrationController', function RegistrationController($scope, $rootScope, $http, $log, $location) {

        $scope.credentials = [];

        $scope.registration = function () {
            $scope.errorMessage = undefined;

            $http.post(Settings.REST_POINT + '/api/registration',
                {
                    "email": this.credentials.email,
                    "name": this.credentials.name,
                    "phone": this.credentials.phone,
                    "password": this.credentials.password
                }
            ).
            then(function (resp) {
                if (resp.data.error) {
                    $scope.errorMessage = resp.data.error;
                } else {
                    $location.path("/login");
                }
            }).
            catch(function (resp) {
                if (Utils.verifyUser(resp.status, $location)) return;
                Utils.showErrorMessage(resp);
            });
        };
    }
);
