vipcodApp.controller('HeadController', function LoginController($scope, $rootScope, $http, $log, $location, $interval, ngProgressFactory, alertService) {

    $rootScope.progressbar = ngProgressFactory.createInstance();

    $rootScope.monthList = Utils.monthList;
    $rootScope.nextMonth = Utils.nextMonth;

    var refresh = function () {
        $log.log("refreshing");

        Utils.requestUser($http,
            function (user) {
                $rootScope.user = user;
                $rootScope.role = {
                    "kassir": user.roles.indexOf('K') >= 0,
                    "manager": user.roles.indexOf('M') >= 0,
                    "sysadmin": user.roles.indexOf('S') >= 0
                }
            },
            function (status, error) {
                Utils.verifyUser(status, $location);
                if (status == 0 || status == 404) Utils.showErrorMessage("Сервер недоступен!", alertService);
            }
        );
    };


    var startSessionCheckerInterval = function () {
        $interval(function () {
            if ($location.path().indexOf("login") < 0) refresh();
        }, 30000);

    };

    $scope.$on('userLogined', function () {
        refresh()
    });
    refresh();
    startSessionCheckerInterval();
});
