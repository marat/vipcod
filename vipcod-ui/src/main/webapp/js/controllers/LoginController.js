vipcodApp.controller('LoginController', function LoginController($scope, $rootScope, $http, $log, $location) {
        $scope.credentials = [];

        $scope.showForm = function () {
            $http.get(Settings.REST_POINT + '/api/logout');
        };
        $scope.showForm();

        $scope.login = function () {
            $scope.errorMessage = undefined;

            $http.post(Settings.REST_POINT + '/api/login',
                {
                    "email": this.credentials.email,
                    "password": this.credentials.password
                }
            ).then(function (resp) {
                if (resp.data.error) {
                    $scope.errorMessage = resp.data.error;
                } else {
                    $location.path("/home");
                    $rootScope.$broadcast('userLogined');
                }
            }).
            catch(function (resp) {
                console.log(resp.status);
                if (resp.status == 401) $location.path("/login");
                $scope.errorMessage = resp.data.error;
            });
        };
    }
);
