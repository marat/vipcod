var Utils = {

    showErrorMessage: function (resp, alertService, delay) {
        var showMsg = alertService ? function (msg, redelay) {
            alertService.add("danger", msg, redelay ? redelay : (delay ? delay : 3000));
        } : alert;

        if (resp.data) resp = resp.data;

        if (!resp) {
            showMsg('Произошла ошибка. Обратитесь к системному администратору');
        } else if (resp.message) {
            showMsg(resp.message);
            if (resp.stack) {
                showMsg(resp.stack, 10000);
            }
        } else if (resp.error) {
            showMsg(resp.error);
        } else {
            showMsg(resp);
        }
    },

    showWarnMessage: function (msg, alertService, delay) {
        var showMsg = alertService ? function (msg) {
            alertService.add("warning", msg, delay ? delay : 6000);
        } : alert;
        showMsg(msg);
    },

    verifyUser: function (status, $location, $uibModalInstance) {
        if (status == 401) {
            if ($uibModalInstance) {
                $uibModalInstance.dismiss('cancel');
            }
            if ($location.url() != "/login") {
                //Utils.showErrorMessage("Действие не может быть выполнено - время сессии пользователя истекло.");
                $location.path("/login");
            }
            return true;
        }
    },

    showInfoMessage: function (data, alertService) {
        var showMsg = alertService ? function (msg) {
            alertService.add("info", msg, 3000);
        } : alert;

        showMsg(data);
    },

    requestUser: function ($http, callBackSuccess, callBackFailed) {
        $http.get(Settings.REST_POINT + '/sessions/info?_=' + Math.random())
            .then(function (arg) {
                if (arg.data.error) {
                    callBackFailed(arg.status, arg.data.error);
                } else {
                    callBackSuccess(arg.data.result.user);
                }

            }).catch(function (arg) {
            callBackFailed(arg.status, arg.data);
        });

    },

    monthList: [

        {mid: 10, name: "октябрь 2015", mmyyyy: "102015"},
        {mid: 11, name: "ноябрь 2015", mmyyyy: "112015"},
        {mid: 12, name: "декабрь 2015", mmyyyy: "122015"},
        {mid: 13, name: "январь 2016", mmyyyy: "012016"},
        {mid: 14, name: "февраль 2016", mmyyyy: "022016"},
        {mid: 15, name: "март 2016", mmyyyy: "032016"},
        {mid: 16, name: "апрель 2016", mmyyyy: "042016"},
        {mid: 17, name: "май 2016", mmyyyy: "052016"},
        {mid: 18, name: "июнь 2016", mmyyyy: "062016"},
        {mid: 19, name: "июль 2016", mmyyyy: "072016"},
        {mid: 20, name: "август 2016", mmyyyy: "082016"},
        {mid: 21, name: "сентябрь 2016", mmyyyy: "092016"},
        {mid: 22, name: "октябрь 2016", mmyyyy: "102016"},
        {mid: 23, name: "ноябрь 2016", mmyyyy: "112016"},
        {mid: 24, name: "декабрь 2016", mmyyyy: "122016"},
        {mid: 25, name: "январь 2017", mmyyyy: "012017"},
        {mid: 26, name: "февраль 2017", mmyyyy: "022017"},
        {mid: 27, name: "март 2017", mmyyyy: "032017"},
        {mid: 28, name: "апрель 2017", mmyyyy: "042017"},
        {mid: 29, name: "май 2017", mmyyyy: "052017"},
        {mid: 30, name: "июнь 2017", mmyyyy: "062017"},
        {mid: 31, name: "июль 2017", mmyyyy: "072017"},
        {mid: 32, name: "август 2017", mmyyyy: "082017"},
        {mid: 33, name: "сентябрь 2017", mmyyyy: "092017"},
        {mid: 34, name: "октябрь 2017", mmyyyy: "102017"},
        {mid: 35, name: "ноябрь 2017", mmyyyy: "112017"},
        {mid: 36, name: "декабрь 2017", mmyyyy: "122017"},
        {mid: 37, name: "январь 2018", mmyyyy: "012018"},
        {mid: 38, name: "февраль 2018", mmyyyy: "022018"},
        {mid: 39, name: "март 2018", mmyyyy: "032018"},
        {mid: 40, name: "апрель 2018", mmyyyy: "042018"},
        {mid: 41, name: "май 2018", mmyyyy: "052018"},
        {mid: 42, name: "июнь 2018", mmyyyy: "062018"},
        {mid: 43, name: "июль 2018", mmyyyy: "072018"},
        {mid: 44, name: "август 2018", mmyyyy: "082018"},
        {mid: 45, name: "сентябрь 2018", mmyyyy: "092018"},
        {mid: 46, name: "октябрь 2018", mmyyyy: "102018"},
        {mid: 47, name: "ноябрь 2018", mmyyyy: "112018"},
        {mid: 48, name: "декабрь 2018", mmyyyy: "122018"}
    ],

    dayList: [
        {mid: 1, name: "понедельник", shortName: "Пн"},
        {mid: 2, name: "вторник", shortName: "Вт"},
        {mid: 3, name: "среда", shortName: "Ср"},
        {mid: 4, name: "четверг", shortName: "Чт"},
        {mid: 5, name: "пятница", shortName: "Пт"},
        {mid: 6, name: "суббота", shortName: "Сб"},
        {mid: 7, name: "воскресенье", shortName: "Вс"}
    ],

    getMonthById: function (id) {
        var ts = Utils.monthList.filter(function (mitem) {
            return mitem.mid == id;
        });
        return ts && ts.length > 0 ? ts[0] : id;
    },

    getDayWeekById: function (id) {
        var ts = Utils.dayList.filter(function (mitem) {
            return mitem.mid == id;
        });
        return ts && ts.length > 0 ? ts[0] : null;
    },

    nextMonth: function (idx) {
        if (!idx) {
            var yr = new Date().getFullYear() - 2015;
            idx = yr * 12 + new Date().getMonth() + 1;
        }
        ++idx;
        //if (idx > 12) idx = 1;
        return idx;
    },

    endOfMonthDate: function (d) {
        if (!d) d = new Date();
        return new Date(d.getFullYear(), d.getMonth() + 1, 0);
    },

    beginOfMonthDate: function (d) {
        if (!d) d = new Date();
        return new Date(d.getFullYear(), d.getMonth(), 1);
    },

    dayOfWeek: function (d) {
        if (!d) return null;
        return ((d.getDay() + 6) % 7) + 1;
    },

    contains: function (list, x) {
        return list.indexOf(x) >= 0;
    },


    saveToPc: function (data, filename) {

        if (!data) {
            console.error('No data');
            return;
        }

        if (!filename) {
            filename = 'download.json';
        }

        if (typeof data === 'object') {
            data = JSON.stringify(data, undefined, 2);
        }

        var blob = new Blob([data], {type: 'text/json'});

        // FOR IE:

        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, filename);
        }
        else {
            var e = document.createEvent('MouseEvents'),
                a = document.createElement('a');

            a.download = filename;
            a.href = window.URL.createObjectURL(blob);
            a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
            e.initEvent('click', true, false, window,
                0, 0, 0, 0, 0, false, false, false, false, 0, null);
            a.dispatchEvent(e);
        }
    }
};