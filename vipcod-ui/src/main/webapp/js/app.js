var vipcodApp = angular.module(
    'vipcodApp', ['valdr', 'ngRoute', 'ui.bootstrap', 'ngSanitize', 'ui.select', 'pascalprecht.translate', 'treeGrid', 'ngDialog',
        'ui.bootstrap', 'ui.calendar', 'ui.bootstrap.contextMenu', 'ngProgress', 'multipleDatePicker' /*, 'gm.datepickerMultiSelect'*/ /*, 'ng-context-menu'*/], function ($httpProvider) {
    }
);

vipcodApp.config(function ($routeProvider) {

    $routeProvider.when('/login',
        {
            templateUrl: 'views/login.html',
            controller: 'LoginController'
        });

    $routeProvider.when('/registration',
        {
            templateUrl: 'views/registration.html',
            controller: 'RegistrationController'
        });

    /*$routeProvider.when('/home',
     {
     templateUrl: 'views/home.html',
     controller: 'HomeController'
     });*/

    $routeProvider.when('/clients',
        {
            templateUrl: 'views/clients.html',
            controller: 'ClientsController'
        });

    $routeProvider.when('/services',
        {
            templateUrl: 'views/services.html',
            controller: 'ServicesController'
        });

    $routeProvider.when('/schedule',
        {
            templateUrl: 'views/schedule.html',
            controller: 'ScheduleCalendarCtrl'
        });

    $routeProvider.when('/schedule_table',
        {
            templateUrl: 'views/schedule_table.html',
            controller: 'ScheduleTableCtrl'
        });

    $routeProvider.when('/schedule_plan',
        {
            templateUrl: 'views/schedule_plan.html',
            controller: 'SchedulePlanCtrl'
        });

    $routeProvider.when('/schedule_manager',
        {
            templateUrl: 'views/schedule_manager.html',
            controller: 'ScheduleManagerCtrl'
        });

    $routeProvider.when('/planSchedule',
        {
            templateUrl: 'views/planSchedule.html',
            controller: 'PlanScheduleCtrl'
        });

    $routeProvider.when('/coaches',
        {
            templateUrl: 'views/coaches.html',
            controller: 'CoachesController'
        });

    $routeProvider.when('/payments',
        {
            templateUrl: 'views/payments.html',
            controller: 'PaymentsController'
        });

    $routeProvider.when('/seansList',
        {
            templateUrl: 'views/seansList.html',
            controller: 'SeansListController'
        });

    $routeProvider.when('/pos',
        {
            templateUrl: 'views/pos.html',
            controller: 'PosController'
        });

    $routeProvider.when('/posusers',
        {
            templateUrl: 'views/posusers.html',
            controller: 'PosUsersController'
        });

    $routeProvider.when('/olapReports',
        {
            templateUrl: 'views/reports/olapReports.html',
            controller: 'OlapReportsController'
        });
    $routeProvider.when('/detailDayReport',
        {
            templateUrl: 'views/reports/detailDayReport.html',
            controller: 'DetailDayReportController'
        });
    $routeProvider.when('/balanceReport',
        {
            templateUrl: 'views/reports/balanceReport.html',
            controller: 'BalanceReportController'
        });


    $routeProvider.otherwise({redirectTo: '/clients'});
});

vipcodApp.config(function (valdrProvider, valdrMessageProvider) {
    valdrProvider.setConstraintUrl('constraints.json');
    //valdrMessageProvider.setTemplate('<div class="valdr-message">{{ violation.message }}</div>');
});

vipcodApp.run(function (uibPaginationConfig) {
    uibPaginationConfig.firstText = 'нач.';
    uibPaginationConfig.previousText = 'пред.';
    uibPaginationConfig.nextText = 'след.';
    uibPaginationConfig.lastText = 'кон.';
});


vipcodApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13 && (!scope.itemForm || !scope.itemForm.$invalid)) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter, {'event': event});
                });

                event.preventDefault();
            }
        });
    };
});

vipcodApp.directive('jsonDate', function ($filter) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {

            //format text going to user (model to view)
            ngModel.$formatters.push(function (value) {
                return new Date(value);
            });

            //format text from the user (view to model)
            ngModel.$parsers.push(function (value) {
                var date = new Date(value);
                if (!isNaN(date.getTime())) {
                    return value.getTime();
                }
            });
        }
    }
});

var onEvent = function (scope, name, func) {
    scope.$$listeners[name] = [];
    scope.$on(name, func);
};

vipcodApp.directive('input', function () {
    var count = 0;
    return {
        restrict: 'E',
        compile: function () {
            return {
                pre: function (scope, element, attrs) {
                    // if the input is created by ui-select and has ng-model
                    if (element.hasClass('ui-select-search') && attrs.ngModel) {
                        if (!attrs.name) { // if the input is not having any name
                            attrs.name = 'ui-select' + (++count); // assign name
                        }
                    }
                }
            }
        }
    }
});

vipcodApp.filter('month', function () {
    return function (input) {
        var result = "-";
        Utils.monthList.forEach(function (item) {
            if (item.mid == input) result = item.name;
        });
        return result;
    }
});

vipcodApp.filter('dayOfWeek', function () {
    return function (input) {
        var result = "-";
        Utils.dayList.forEach(function (item) {
            if (item.mid == input) result = item.name;
        });
        return result;
    }
});

vipcodApp.directive('selectOnClick', ['$window', function ($window) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on('click', function () {
                if (!$window.getSelection().toString()) {
                    // Required for mobile Safari
                    this.setSelectionRange(0, this.value.length)
                }
            });
        }
    };
}]);

vipcodApp.directive('capitalizeFirst', function ($parse) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            var capitalize = function (inputValue) {
                if (inputValue === undefined || inputValue === null || inputValue == '') return null;

                var capitalized = inputValue.charAt(0).toUpperCase() +
                    inputValue.substring(1);
                if (capitalized !== inputValue) {
                    modelCtrl.$setViewValue(capitalized);
                    modelCtrl.$render();
                }
                return capitalized;
            };
            modelCtrl.$parsers.push(capitalize);
            capitalize($parse(attrs.ngModel)(scope)); // capitalize initial value
        }
    };
});

vipcodApp.filter('range', function () {
    return function (input, min, max) {
        min = parseInt(min); //Make string input int
        max = parseInt(max);
        for (var i = min; i < max; i++)
            input.push(i);
        return input;
    };
});

vipcodApp.filter('simpleSort', function () {
    return function (input) {
        return input.sort();
    }
});

vipcodApp.directive('indeterminate', [function() {
    return {
        require: '?ngModel',
        link: function(scope, el, attrs, ctrl) {
            ctrl.$formatters = [];
            ctrl.$parsers = [];
            ctrl.$render = function() {
                var d = ctrl.$viewValue;
                el.data('checked', d);
                switch(d){
                    case true:
                        el.prop('indeterminate', false);
                        el.prop('checked', true);
                        break;
                    case false:
                        el.prop('indeterminate', false);
                        el.prop('checked', false);
                        break;
                    default:
                        el.prop('indeterminate', true);
                }
            };
            /*
            скрыто, чтобы управлять значением ng-model из ng-click
            el.bind('click', function() {
                var d;
                switch(el.data('checked')){
                    case false:
                        d = true;
                        break;
                    case true:
                        d = null;
                        break;
                    default:
                        d = false;
                }
                ctrl.$setViewValue(d);
                scope.$apply(ctrl.$render);
            });*/
        }
    };
}]);


Array.prototype.subtract = function(a) {
    return this.filter(function(i) {return a.indexOf(i) < 0;});
};
