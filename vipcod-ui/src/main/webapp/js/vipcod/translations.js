vipcodApp.config(function ($translateProvider) {
    $translateProvider.translations('ru', {
        '{javax.validation.constraints.Size.message}': 'длина должна быть от {{min}} до {{max}}',
        '{javax.validation.constraints.Size.minOnly.message}': 'введите не менее {{min}} символов',
        '{javax.validation.constraints.NotNull.message}': 'обязательное поле',
        '{javax.validation.constraints.Past.message}': 'дата должна быть в прошлом',

        '{javax.validation.constraints.AssertFalse.message}': 'должно быть "нет"',
        '{javax.validation.constraints.AssertTrue.message}': 'должно быть "да"',
        '{javax.validation.constraints.DecimalMax.message}': 'должно быть не больше {{value}}',
        '{javax.validation.constraints.DecimalMin.message}': 'должно быть не меньше {{value}}',
        '{javax.validation.constraints.Digits.message}': 'число выходит за границы (ожидается <{{integer}} знаков>.<{{fraction}} знаков>)',
        '{javax.validation.constraints.Future.message}': 'должно быть в будущем',
        '{javax.validation.constraints.Max.message}': 'должно быть не больше {{value}}',
        '{javax.validation.constraints.Min.message}': 'должно быть не меньше {{value}}',
        '{javax.validation.constraints.Null.message}': 'должно оставаться незаполненным',
        '{javax.validation.constraints.Pattern.message}': 'должно соответствовать "{{regexp}}"',
        '{org.hibernate.validator.constraints.Email.message}': 'укажите email',


        'phone': 'телефон'

    });
    $translateProvider.preferredLanguage('ru');
});
