<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html style="height: 100%" ng-app="vipcodApp">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <title>МАУ Комплексная спортивная школа</title>


    <!-- Javascript -->
    <script src="js/bower_components/jquery/dist/jquery.min.js" type="text/javascript"></script>

    <script src="js/bower_components/angular/angular.js" type="text/javascript"></script>
    <script src="js/bower_components/angular-route/angular-route.js" type="text/javascript"></script>
    <script src="js/bower_components/angular-sanitize/angular-sanitize.js"></script>
    <script src="js/bower_components/angular-translate/angular-translate.js" type="text/javascript"></script>
    <script src="js/bower_components/angular-i18n/angular-locale_ru-ru.js"></script>

    <script src="js/bower_components/ng-dialog/js/ngDialog.min.js"></script>
    <link rel="stylesheet" type="text/css" href="js/bower_components/ng-dialog/css/ngDialog.min.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="js/bower_components/ng-dialog/css/ngDialog-theme-default.min.css" media="all"/>


    <!-- bootstrap-ui-->
    <link rel="stylesheet" type="text/css" href="js/bower_components/bootstrap/dist/css/bootstrap.css" media="all"/>
    <script src="js/bower_components/bootstrap/dist/js/bootstrap.js" type="text/javascript"></script>
    <script src="js/bower_components/angular-bootstrap/ui-bootstrap-tpls.js" type="text/javascript"></script>


    <link rel="stylesheet" type="text/css" href="js/bower_components/angular-ui-select/dist/select.css" media="all"/>
    <script src="js/bower_components/angular-ui-select/dist/select.js"></script>


    <script src="js/bower_components/valdr/valdr.js" type="text/javascript"></script>
    <script src="js/bower_components/valdr/valdr-message.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="js/bower_components/angular-bootstrap-grid-tree/src/treeGrid.css"
          media="all"/>
    <script src="js/bower_components/angular-bootstrap-grid-tree/src/tree-grid-directive.js"></script>

    <!--<script src="js/bower_components/ng-context-menu/dist/ng-context-menu.js"></script>-->
    <script src="js/bower_components/angular-bootstrap-contextmenu/contextMenu.js"></script>

    <link rel="stylesheet" href="js/bower_components/fullcalendar/dist/fullcalendar.css">
    <script src="js/bower_components/moment/moment.js"></script>
    <script src="js/bower_components/fullcalendar/dist/fullcalendar.js"></script>
    <%--<script src="js/bower_components/fullcalendar/dist/lang-all.js"></script>--%>
    <script src="js/bower_components/fullcalendar/dist/locale-all.js"></script>
    <script src="js/lib/calendar.js"></script>

    <script src="js/bower_components/moment/locale/ru.js"></script>

    <script src="js/bower_components/ngprogress/build/ngprogress.min.js"></script>
    <link rel="stylesheet" href="js/bower_components/ngprogress/ngProgress.css">

    <script type="text/javascript" src="js/bower_custom/angular-multiple-date-picker/dist/multipleDatePicker.marat.js"></script>
    <link rel="stylesheet" type="text/css" href="js/bower_custom/angular-multiple-date-picker/dist/multiple-date-picker.css"/>

    <%--<script src="js/bower_components/gm.datepickerMultiSelect/dist/gm.datepickerMultiSelect.min.js"></script>--%>

    <script src="js/settings.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/app.js"></script>
    <script src="js/vipcod/translations.js" type="text/javascript"></script>


    <style>
        .alert {
            padding: 10px;
            margin-bottom: 5px;
        }

        .sw4_notify {
            position: fixed;
            top: 0;
            width: 100%;
            z-index: 999999;
            border-radius: 0 !important;
        }

        .sw4_notify > alert {
            /*width: 100%;*/
            display: block;
            border-radius: 0 !important;
            margin-bottom: 1px;
        }
    </style>


</head>

<body style="height: 100%">

<div class="sw4_notify">
    <alert ng-repeat="alert in alerts" type="alert.type" class="alert alert-{{alert.type}}">{{ alert.msg }}
        <button type="button" class="close" ng-click="closeAlert($index)"><span aria-hidden="true">&times;</span>
        </button>
    </alert>
</div>

<div ng-controller="HeadController" style="position: absolute; font-size: xx-small; float: right; padding-left: 90%" class="btn-xs"><span ng-show="user">{{user.name}} ({{user.company.name}})</span></div>

<div class="grid">
    <div class="col_12 navbar-collapse">
        <nav class="navbar navbar-static-top">

            <ul class="nav nav-tabs">
                <li><a href="#!login" id="menuLogin"><i class="glyphicon glyphicon-off"></i></a></li>
                <%--<li><a href="#!home" id="menuHome"><i class="glyphicon glyphicon-home"></i></a></li>--%>
                <li><a href="#!clients" id="menuClients" ng-show="role.kassir || role.manager"><i class="glyphicon glyphicon-user"></i> Клиенты</a></li>
                <li><a href="#!services" id="menuServices" ng-show="role.manager"><i class="glyphicon glyphicon-shopping-cart"></i> Услуги</a></li>
                <li><a href="#!coaches" id="menuCoaches" ng-show="role.kassir || role.manager"><i class="glyphicon glyphicon-education"></i> Тренеры</a></li>
                <%--<li><a href="#!seansList" id="menuSeansList" ng-show="role.kassir || role.manager"><i class="glyphicon glyphicon-education"></i> Сеансы</a></li>--%>
                <li><a href="#!detailDayReport"><i class="glyphicon glyphicon-time"></i> Дневной отчет</a></li>
                <li><a href="#!balanceReport"><i class="glyphicon glyphicon-piggy-bank"></i> Баланс</a></li>
                <li><a href="#!schedule_table" ng-show="role.manager"><i class="glyphicon glyphicon-th-list"></i> Расписание</a></li>
                <li><a href="#!schedule_manager" ng-show="role.manager"><i class="glyphicon glyphicon-th"></i> Планирование</a></li>
                <%--<li><a href="#!schedule"><i class="glyphicon glyphicon-signal"></i> Календарь</a></li>--%>
                <li><a href="#!olapReports" target="_blank" ng-show="role.manager"><i class="glyphicon glyphicon-signal"></i> OLAP</a></li>
            </ul>
        </nav>
    </div>

    <div style="padding: 0px 15px 0px 15px">
        <ng-view></ng-view>
    </div>

</div>

<script src="js/controllers/helpers/select.ui.helper.js"></script>
<script src="js/controllers/helpers/alertService.js"></script>
<script src="js/controllers/helpers/getReasonModal.js"></script>

<script src="js/controllers/LoginController.js"></script>
<script src="js/controllers/HeadController.js"></script>
<script src="js/controllers/RegistrationController.js"></script>
<script src="js/controllers/ClientsController.js"></script>
<script src="js/controllers/HomeController.js"></script>
<script src="js/controllers/SettingsController.js"></script>
<script src="js/controllers/PosController.js"></script>
<script src="js/controllers/PosUsersController.js"></script>
<script src="js/controllers/reports/OlapReportsController.js"></script>
<script src="js/controllers/reports/DetailDayReportController.js"></script>
<script src="js/controllers/reports/BalanceReportController.js"></script>
<script src="js/controllers/services/ServicesController.js"></script>
<script src="js/controllers/CoachesController.js"></script>
<script src="js/controllers/PaymentsController.js"></script>
<script src="js/controllers/SeansListController.js"></script>
<script src="js/controllers/schedule/ScheduleTableCtrl.js"></script>
<script src="js/controllers/schedule/SchedulePlanCtrl.js"></script>
<script src="js/controllers/schedule/ScheduleManagerCtrl.js"></script>
<script src="js/controllers/schedule/ScheduleCalendarCtrl.js"></script>
<script src="js/controllers/schedule/PlanScheduleCtrl.js"></script>


<link rel="stylesheet" type="text/css" href="css/style.css" media="all"/>
</body>
</html>
