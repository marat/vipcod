package org.marat.vipcod.model.entities.server;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.marat.vipcod.model.entities.AbstractEntityWithId;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by marat on 16.07.2015.
 */
@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Table(name = "COMPANY_ENTITY")
public class Company extends AbstractEntityWithId {
    private String name;

    private String description;

    /**
     * ?????? ?????????? ?????????
     */
    @ManyToMany
    @XmlTransient
    @JsonIgnore
    private List<Pos> pos = new ArrayList<>();

    /**
     * ?????? ????????? ?????????? ???
     */
    @ManyToMany
    @XmlTransient
    @JsonIgnore
    private List<ClientBase> clientBases = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Pos> getPos() {
        return pos;
    }

    public void setPos(List<Pos> pos) {
        this.pos = pos;
    }

    public List<ClientBase> getClientBases() {
        return clientBases;
    }

    public void setClientBases(List<ClientBase> clientBases) {
        this.clientBases = clientBases;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
