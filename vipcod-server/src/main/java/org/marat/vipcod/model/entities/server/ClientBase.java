package org.marat.vipcod.model.entities.server;

import org.marat.vipcod.model.entities.AbstractEntityWithId;
import org.marat.vipcod.model.entities.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * Клиентская база
 *
 *   pos1  -\             /- person1
 *   pos2  --]==- CB1 -==[-  person2
 *   pos3  -/            \_  person3
 *                       /
 *   user 1----* pos4 *-------1 CB2 *------*  company
 *
 */
@Entity
@Cacheable(false)
@Table(name = "CLIENT_BASE")
public class ClientBase extends AbstractEntityWithId {
    private static final Logger LOG = LoggerFactory.getLogger(ClientBase.class);

    @NotNull
    String name;

    @NotNull
    protected long ownerId;

    @ManyToMany(fetch = FetchType.LAZY)
    Set<Person> persons = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY)
    List<Pos> poses = new ArrayList<>();

    public Set<Person> getPersons() {
        return persons;
    }

    public void setPersons(Set<Person> persons) {
        this.persons = persons;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Pos> getPoses() {
        return poses;
    }

    public void setPoses(List<Pos> poses) {
        this.poses = poses;
    }
}
