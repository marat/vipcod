package org.marat.vipcod.model.entities.server;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.marat.vipcod.model.entities.AbstractEntityWithUuid;
import org.marat.vipcod.model.entities.pos.PosUser;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * POS-терминал
 */
@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Table(name = "POS_ENTITY")
public class Pos extends AbstractEntityWithUuid {

    @NotNull
    @XmlElement(nillable = true)
    String name;

    @NotNull
    @Size(min = 3, message = "{javax.validation.constraints.Size.minOnly.message}")
    @XmlElement(nillable = true)
    String description;

    @ManyToOne
    private Company owner;

    @ManyToMany
    private List<PosUser> users = new ArrayList<>();

    @XmlTransient
    @JsonIgnore
    @ManyToOne
    private ClientBase clientBase;

    @Transient
    private Long clientBaseId;

    public Pos() {
    }

    public Pos(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Company getOwner() {
        return owner;
    }

    public void setOwner(Company owner) {
        this.owner = owner;
    }

    public List<PosUser> getUsers() {
        return users;
    }

    public void setUsers(List<PosUser> users) {
        this.users = users;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ClientBase getClientBase() {
        return clientBase;
    }

    public void setClientBase(ClientBase clientBase) {
        this.clientBase = clientBase;
    }

    /*public Long getClientBaseId() {
        return clientBase == null ? null : clientBase.getId();
    }

    public void setClientBaseId(Long clientBaseUid) {
        throw new RuntimeException("Unexpected setter calling: " + clientBaseUid);
    }*/
}
