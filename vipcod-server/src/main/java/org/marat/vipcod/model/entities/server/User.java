package org.marat.vipcod.model.entities.server;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Администратор on 28.10.2014.
 */
@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Table(name = "USER_ENTITY")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @XmlElement(nillable = true)
    protected Long id;

    @XmlElement(nillable = true)
    private String email;

    @XmlElement(nillable = true)
    private String phone;

    @XmlElement(nillable = true)
    private String name;

    @XmlElement(nillable = true)
    private String password;

    @ManyToOne(optional = false)
    private Company company;


    /*
     * роли пользователя в системе:
     *      A - администратор бек-офиса,
     *      S - системный администратор,
     *      K - кассир
     */
    @Column(name = "ROLES")
    private String roles = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
