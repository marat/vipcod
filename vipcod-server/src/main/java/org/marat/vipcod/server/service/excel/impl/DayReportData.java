package org.marat.vipcod.server.service.excel.impl;

import org.marat.vipcod.model.entities.Payment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class DayReportData implements Serializable {
    private List<Payment> payments;
    private List<Payment> paymentCorrects;
    private List<ServAggrItem> agregateNds = new ArrayList<>();
    private List<ServAggrItem> agregateNoNds = new ArrayList<>();

    public DayReportData() {
    }

    public DayReportData(List payments, List paymentCorrects) {
        setPayments(payments);
        setPaymentCorrects(paymentCorrects);
    }

    public List getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;

        payments.forEach(pay -> {
            List<ServAggrItem> servList = pay.getNds() ? agregateNds : agregateNoNds;

            List<ServAggrItem> collect = servList.stream().filter(i -> i.getServName().equals(pay.getService().getName())).collect(Collectors.toList());
            ServAggrItem pair = null;
            if (!collect.isEmpty()){
                pair = collect.get(0);
                servList.remove(pair);
            }

            if (pair == null) pair = new ServAggrItem(pay.getService().getName(), 0L, 0L, 0L, 0.0, 0.0, 0.0);
            pair.setCount(pair.getCount() + pay.getVisitsCountDvSp());
            pair.setSum(pair.getSum() + pay.getSum());
            if (!pay.getCash()) {
                pair.seteSum(pair.geteSum() + pay.getSum());
                pair.setEcount(pair.getEcount() + pay.getVisitsCountDvSp());
            } else {
                pair.setcSum(pair.getcSum() + pay.getSum());
                pair.setCcount(pair.getCcount() + pay.getVisitsCountDvSp());
            }
            servList.add(pair);
        });
    }

    public List<Payment> getPaymentCorrects() {
        return paymentCorrects;
    }

    public void setPaymentCorrects(List<Payment> paymentCorrects) {
        this.paymentCorrects = paymentCorrects;
    }

    public List<ServAggrItem> getAgregateNds() {
        return agregateNds;
    }

    public void setAgregateNds(List<ServAggrItem> agregateNds) {
        this.agregateNds = agregateNds;
    }

    public List<ServAggrItem> getAgregateNoNds() {
        return agregateNoNds;
    }

    public void setAgregateNoNds(List<ServAggrItem> agregateNoNds) {
        this.agregateNoNds = agregateNoNds;
    }

    public Double getSum() {
        double sum = 0.0;
        for (Object item : payments) {
            Payment payment = (Payment) item;
            sum += payment.getSum();
        }
        for (Object item : paymentCorrects) {
            Payment payment = (Payment) item;
            sum += payment.getSum();
        }
        return sum;
    }
}


