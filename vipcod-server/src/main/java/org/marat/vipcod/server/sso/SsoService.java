package org.marat.vipcod.server.sso;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Администратор on 21.10.2014.
 */
public class SsoService {
    public static SsoService instance = new SsoService();

    private Map<String, String> ssoSessionUids = new ConcurrentHashMap<>(10);

    public String getPosBySessionId(String sessionId) {
        String s = ssoSessionUids.get(sessionId);
        if (s == null) throw new SsoException();
        return s;
    }

    public String createSsoSession(String pos){
        String sessionId = UUID.randomUUID().toString();
        ssoSessionUids.put(sessionId, pos);
        return sessionId;
    }
}
