package org.marat.vipcod.server.service;

import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.action.Abonement;
import org.marat.vipcod.model.entities.action.ActionHistory;
import org.marat.vipcod.model.entities.server.Pos;
import org.marat.vipcod.model.integration.synch.SynchUploadException;
import org.marat.vipcod.server.dao.AbonementRepository;
import org.marat.vipcod.server.dao.CardRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by marat on 16.02.2015.
 */
@Component
public class ActionActiveCalculatorService {

    private static final Logger LOG = LoggerFactory.getLogger(ActionActiveCalculatorService.class);

    @Autowired
    AbonementRepository abonementRepository;

    @Autowired
    CardRepository cardRepository;

    public Abonement calculate(Pos pos, ActionHistory newActionHistoryStep) {
        Abonement cardState = abonementRepository.findByPosAndEan(pos.getUuid(), newActionHistoryStep.getEan());

        LOG.info("###########: {} : {}", pos.getUuid(), cardState);

        String actionType = newActionHistoryStep.getNewStatus();
        if (actionType.equals("CHECK")) {
            // еще одно посещение

            //todo: добавить политики отметки посещений и для каждого терминала хранить отдельное состояние - чтобы
            // одну карту разные терминалы могли обслуживать со своими правиами (например, в одном из салонов игнорируются
            // посещения в другие заведения из совместной с ними программы. Карты общие - политика снятия посещений разная

            cardState.setCount(cardState.getCount() + 1);
        } else if (actionType.equals("NEW")) {
            // карта преобретена
            if (cardState != null && cardState.getCount() != 0L) {
                LOG.error("нарушена логика сохранения состояния карты и истории");
                throw new SynchUploadException("нарушена логика сохранения состояния карты и истории");
            }
            Card persistedCard = cardRepository.findByEan(newActionHistoryStep.getEan());
            if (cardState == null) {
                cardState = new Abonement(persistedCard, persistedCard.getManager());
            }
            ////cardState.setPos(pos.getUuid());
            cardState.setRecordTime(new Date());
            cardState.setCount(0L);
            //persistedCard.setSelectedAbonement(cardState);
        } else if (actionType.equals("EDIT")) {
            //todo: уточнить политику. по идее просто так редактировать из терминала нельзя (подделка состояния карт)
            //но в случае монопольного терминала - такая возможность нужна
            cardState.setCount(newActionHistoryStep.getCount());
        } else {
            LOG.error("Неизвестный тип действия с картой: " + actionType);
            throw new SynchUploadException("Неизвестный тип действия с картой: " + actionType);
        }

        return cardState;
    }
}
