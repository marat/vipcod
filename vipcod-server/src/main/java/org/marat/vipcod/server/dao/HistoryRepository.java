package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.server.Pos;
import org.springframework.data.repository.query.Param;
import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.entities.action.ActionHistory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryRepository extends CrudRepository<ActionHistory, String> {

    @Query("select h from ActionHistory h, Card card " +
            "where card.ean = h.ean and card.person in (:persons) and (h.created > :prevVersion or h.updated > :prevVersion) and (h.created < :untilVersion or h.updated < :untilVersion)")
    List<ActionHistory> findByVersions(@Param("persons") List<Person> persons, @Param("prevVersion") Long prevVersion, @Param("untilVersion") Long untilVersion);

    @Query("select hist from ActionHistory hist, ClientBase cb " +
            "inner join cb.persons person " +
            "where hist.card.person.uuid = person.uuid and :pos member of cb.poses and (hist.created > :prevVersion or hist.updated > :prevVersion) and (hist.created < :untilVersion or hist.updated < :untilVersion)")
    List<ActionHistory> findByVersions(@Param("pos") Pos pos, @Param("prevVersion") Long prevVersion, @Param("untilVersion") Long untilVersion);

    ActionHistory findByUuid(String uuid);

    @Query("select coalesce(max(h.updated), 0) from ActionHistory h")
    long currentVersion();
}
