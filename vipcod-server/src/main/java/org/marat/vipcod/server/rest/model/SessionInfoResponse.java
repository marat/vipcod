package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.integration.api.StatusResponse;

/**
 * Created by marat on 19.03.2015.
 */

public class SessionInfoResponse extends StatusResponse<SessionInfo> {

    public SessionInfoResponse() {
    }

    public SessionInfoResponse(String error) {
        super(error);
    }

    public SessionInfoResponse(SessionInfo result) {
        super(result);
    }

    public SessionInfoResponse(SessionInfo result, String error) {
        super(result, error);
    }

}

