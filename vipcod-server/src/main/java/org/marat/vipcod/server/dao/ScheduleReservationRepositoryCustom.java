package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.schedule.ScheduleItem;
import org.springframework.stereotype.Repository;


@Repository
public interface ScheduleReservationRepositoryCustom {
    Long getCountForSchedule(ScheduleItem schedule);
}
