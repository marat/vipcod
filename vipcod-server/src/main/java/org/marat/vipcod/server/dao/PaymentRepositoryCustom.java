package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.Payment;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * Created by Marat on 16.03.2016.
 */
public interface PaymentRepositoryCustom {
    Long findTotalByParams(Long ownerId, String personUuid, Long serviceId, String coachUuid, Long period, String filter);
    List<Payment> findByParams(Long ownerId, String personUuid, Long serviceId, String coachUuid, Long period, String filter, PageRequest pageRequest);
}
