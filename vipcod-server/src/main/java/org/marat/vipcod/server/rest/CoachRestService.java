package org.marat.vipcod.server.rest;

import org.marat.vipcod.model.entities.Coach;
import org.marat.vipcod.model.entities.server.User;
import org.marat.vipcod.model.integration.api.PagedRequest;
import org.marat.vipcod.server.dao.CoachRepository;
import org.marat.vipcod.server.dao.PaymentRepository;
import org.marat.vipcod.server.dao.UserRepository;
import org.marat.vipcod.server.rest.model.CoachPagedResponse;
import org.marat.vipcod.server.rest.model.CoachStatusResponse;
import org.marat.vipcod.server.service.excel.impl.PaymentsExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Objects;

/**
 * Created by Администратор on 28.10.2014.
 */
@Component
@RequestMapping("/coach")
public class CoachRestService extends AbstractService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CoachRepository coachRepository;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    PaymentsExportService paymentsExportService;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public
    @ResponseBody
    List<Coach> getAllCoachs(HttpSession session) {
        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        return coachRepository.findByOwnerId(ownerId, new PageRequest(0, 9999));
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public
    @ResponseBody
    CoachPagedResponse getCoachs(
            @RequestBody PagedRequest request,
            HttpSession session
    ) {

        // todo: использовать id сессии
        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        List<Coach> coachList = null;
        long totalByOwnerId = 0;
        if (StringUtils.isEmpty(request.getFilter())) {
            coachList = coachRepository.findByOwnerId(ownerId, new PageRequest(request.getPageNum(), request.getPageSize()));
            totalByOwnerId = coachRepository.findTotalByOwnerId(ownerId);
        } else {
            String filter = "%" + request.getFilter() + "%";
            coachList = coachRepository.findByOwnerId(ownerId, filter, new PageRequest(request.getPageNum(), request.getPageSize()));
            totalByOwnerId = coachRepository.findTotalByOwnerId(ownerId, filter);
        }

        return new CoachPagedResponse(
                coachList,
                totalByOwnerId
        );
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public
    @ResponseBody
    CoachStatusResponse saveCoach(@RequestBody Coach coach, HttpSession session) {
        // todo: присвоить параметры, которые не сериализуемы. Либо скопировать значения
        // сериализуемых параметров в оригинал из пришедшего объекта

        // todo: проверить хозяина coach

        Coach originCoach = null;

        if (!org.apache.commons.lang3.StringUtils.isEmpty(coach.getUuid())) {
            originCoach = coachRepository.findOne(coach.getUuid());
        }

        if (originCoach == null) {
            originCoach = new Coach(coach.getUuid());
            User user = getUser(session);
            originCoach.setOwnerId(userRepository.findOne(user.getId()).getCompany().getId());
        }
        coach.setOwnerId(originCoach.getOwnerId());

        coach = coachRepository.save(coach);
        return new CoachStatusResponse(coach);
    }

    @RequestMapping(value = "/payments/{coachId}/{serviceId}.xlsx", method = RequestMethod.GET)
    void downloadAsXlsx(HttpServletResponse response,
                        HttpSession session,
                        @PathVariable("coachId") String coachId,
                        @PathVariable("serviceId") Long serviceId,
                        @RequestParam(value = "period", required = false) Long period,
                        @RequestParam(value = "filter", required = false) String filter
    ) throws IOException, ParseException {
        User user = getUser(session);

        filter = null; // todo: пока не разобрался с URIEncoding

        response.setContentType("application/octet-stream");

        List result;

        result = paymentRepository.findByParams(user.getCompany().getId(),
                null,
                serviceId,
                coachId,
                period,
                filter,
                null
        );

        paymentsExportService.report(result, response.getOutputStream());
    }

    @RequestMapping(value = "/get/{coachId}", method = RequestMethod.GET)
    @ResponseBody
    Coach getCoach(HttpSession session,
                   @PathVariable("coachId") String coachId
    ) throws IOException, ParseException {
        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        Coach coach = coachRepository.findOne(coachId);
        if (Objects.equals(coach.getOwnerId(), ownerId)) return coach;
        LOG.warn("Request not owned coach! {} != {}", ownerId, coach.getOwnerId());
        return null;
    }
}
