package org.marat.vipcod.server.rest;

import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.Payment;
import org.marat.vipcod.model.entities.server.Pos;
import org.marat.vipcod.model.integration.synch.SyncUploadResult;
import org.marat.vipcod.model.integration.synch.SynchUploadException;
import org.marat.vipcod.server.dao.*;
import org.marat.vipcod.server.rest.model.PersonPagedResponse;
import org.marat.vipcod.server.rest.model.PersonStatusResponse;
import org.marat.vipcod.server.sso.SsoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.entities.server.User;
import org.marat.vipcod.model.integration.api.PagedRequest;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

//import javax.ws.rs.QueryParam;

/**
 * Created by Администратор on 28.10.2014.
 */
@Component
@Transactional
@RequestMapping("/person")
public class PersonRestService extends AbstractService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    ClientBaseRepository clientBaseRepository;

    @Autowired
    CardRepository cardRepository;

    @Autowired
    PosRepository posRepository;

    @Autowired
    PaymentRepository paymentRepository;


    @RequestMapping(value = "/get/{uid}", method = RequestMethod.GET)
    public
    @ResponseBody
    Person getPersons(
            @PathVariable("uid") String uid,
            HttpSession session
    ) {
        return personRepository.findOne(uid);
    }


    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public
    @ResponseBody
    PersonPagedResponse getPersons(
            @RequestBody PagedRequest request,
            HttpSession session
    ) {
        User user = getUser(session);

        Long ownerId = user.getCompany().getId();

        String filter0 = request.getFilter();
        String filter1 = null;
        String filter2 = null;

        if (filter0.contains(" ")){
            // если пробел в фильтре - ищем и по фамилии и по имени
            String[] splited = filter0.split(" ", 2);
            filter1 = splited[0].trim() + "%";
            filter2 = splited[1].trim() + "%";
            return new PersonPagedResponse(
                    personRepository.findByOwnerId2(ownerId, filter1, filter2, new PageRequest(request.getPageNum(), request.getPageSize())),
                    personRepository.findTotalByOwnerId2(ownerId, filter1, filter2)
            );
        }


        String filter = StringUtils.isEmpty(filter0) ? "%" : "%" + filter0 + "%";

        return new PersonPagedResponse(
                personRepository.findByOwnerId(ownerId, filter, filter, new PageRequest(request.getPageNum(), request.getPageSize())),
                personRepository.findTotalByOwnerId(ownerId, filter, filter)
        );
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public
    @ResponseBody
    PersonStatusResponse deletePerson(@RequestBody Person person, HttpSession session, BindingResult bindingResult) {

        Person storedPerson = personRepository.findOne(person.getUuid());
        if (storedPerson == null) return new PersonStatusResponse("Клиент не найден в базе");

        storedPerson.setDeleted(true);
        storedPerson = personRepository.save(storedPerson);
        return new PersonStatusResponse(storedPerson);
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public
    @ResponseBody
    PersonStatusResponse savePersons(@Valid @RequestBody Person person, HttpSession session, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new PersonStatusResponse(bindingResult.toString());
        }
        User user = getUser(session);


        // todo: присвоить параметры, которые не сериализуемы. Либо скопировать значения
        // сериализуемых параметров в оригинал из пришедшего объекта

        // todo: проверить хозяина person

        Person originPerson = null;

        if (person.getUuid() != null) {
            originPerson = personRepository.findOne(person.getUuid());
        }
        if (originPerson == null) {
            originPerson = new Person(person.getUuid());
            originPerson.setOwnerId(user.getCompany().getId());
        }
        person.setOwnerId(originPerson.getOwnerId());

        String ean = person.getCurrentCardEan();
        Card existsCard = cardRepository.findByEan(ean);
        person.setCurrentCardEan(null);
        person = personRepository.save(person);

        clientBaseRepository.savePersonInBaseTx(person, user.getCompany().getId());

        if (existsCard == null) {
            // автоприсвоение номера абонемента для дворца спорта
            if (StringUtils.isEmpty(ean)) {
                ean = String.format("%06d", cardRepository.getNextEanForNewCard());
            }
            existsCard = cardRepository.save(new Card(ean, person));
        }

        Person cardPerson = existsCard.getPerson();
        if (!Objects.equals(cardPerson.getUuid(), person.getUuid())) {
            logger.error("Попытка сохранить персону чужую карту: {} != {}", cardPerson.getUuid(), person.getUuid());
            throw new RuntimeException("Абонемент с номером " + ean + " ранее был выдан другому клиенту: " + cardPerson.getFullName());
        }

        person.setCurrentCardEan(ean);
        person = personRepository.save(person);

        // обновление признака ндс у платежей отредактированного клиента
        List<Payment> paymentList = paymentRepository.findByOwnerIdAndPersonId(user.getCompany().getId(), person.getUuid(), new PageRequest(0, 9999));
        paymentList.forEach(Payment::updateNds);
        paymentRepository.save(paymentList);

        return new PersonStatusResponse(person);
    }

    @RequestMapping(value = "/upload/{suid}", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    SyncUploadResult uploadPersons(
            @PathVariable("suid") String uid,
            @Valid @RequestBody List<Person> person,
            HttpSession session, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new SynchUploadException(bindingResult.toString());
        }

        String posId = SsoService.instance.getPosBySessionId(uid);
        Pos pos = posRepository.findByPosId(posId);

        if (pos == null) throw new SynchUploadException("sync session not found by id = " + uid);

        for (Person pers : person) {
            if (pers.getUuid() != null) {
                Person entity = personRepository.findOne(pers.getUuid());
                if (entity == null) throw new SynchUploadException("person not found by uuid = " + pers.getUuid());
                if (entity.getOwnerId() != pos.getOwner().getId())
                    throw new SynchUploadException("person not found for owner by persn.uuid = " + pers.getUuid());
            }
        }
        personRepository.save(person);

        return new SyncUploadResult();
    }


}
