package org.marat.vipcod.server.service.excel.impl;

import org.marat.vipcod.model.entities.Coach;
import org.marat.vipcod.model.entities.Payment;

import java.io.Serializable;
import java.util.List;

public class ScheduleReservesExportData implements Serializable {

    private final String sessionBegin;
    private final Coach coach;
    private List<Payment> items;

    public ScheduleReservesExportData(List<Payment> paymentList, String sessionBegin, Coach coach) {
        this.items = paymentList;
        this.sessionBegin = sessionBegin;
        this.coach = coach;
    }

    public List<Payment> getItems() {
        return items;
    }

    public void setItems(List<Payment> items) {
        this.items = items;
    }

    public String getSessionTextInfo() {
        return sessionBegin;
    }
}
