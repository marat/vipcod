package org.marat.vipcod.server.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.marat.vipcod.model.entities.Payment;
import org.marat.vipcod.model.entities.schedule.ScheduleItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 */
public class PaymentSaveRequest implements Serializable {

    @JsonProperty(value = "payment")
    private Payment payment;

    @JsonProperty(value = "selectedScheduleItems")
    private List<ScheduleItem> selectedTimes = new ArrayList<>();

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public List<ScheduleItem> getSelectedTimes() {
        return selectedTimes;
    }

    public void setSelectedTimes(List<ScheduleItem> selectedTimes) {
        this.selectedTimes = selectedTimes;
    }

    @Override
    public String toString() {
        return "PaymentSaveRequest{" +
                "payment=" + payment +
                ", selectedTimes=" + selectedTimes +
                '}';
    }
}
