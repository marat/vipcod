package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.Coach;
import org.marat.vipcod.model.entities.Session;

/**
 * Created by marat on 23.06.2015.
 */
public class PlanItemRequest {
    Long session;
    String coach;
    Long service;

    public Long getSession() {
        return session;
    }

    public void setSession(Long session) {
        this.session = session;
    }

    public String getCoach() {
        return coach;
    }

    public void setCoach(String coach) {
        this.coach = coach;
    }

    public Long getService() {
        return service;
    }

    public void setService(Long service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "PlanItemRequest{" +
                "session='" + session + '\'' +
                ", coach='" + coach + '\'' +
                ", service='" + service + '\'' +
                '}';
    }
}
