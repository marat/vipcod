package org.marat.vipcod.server.dao.impl;

import org.marat.vipcod.model.entities.schedule.ScheduleItem;
import org.marat.vipcod.model.entities.schedule.ScheduleReservation;
import org.marat.vipcod.server.dao.ScheduleReservationRepositoryCustom;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ScheduleReservationRepositoryImpl implements ScheduleReservationRepositoryCustom {

    @PersistenceContext
    public EntityManager entityManage;

    @Override
    public Long getCountForSchedule(ScheduleItem schedule) {
        return (Long) entityManage.createQuery("select count(r) from ScheduleReservation r where r.schedule.id = :id")
                .setParameter("id", schedule.getId()).getSingleResult();
    }
}
