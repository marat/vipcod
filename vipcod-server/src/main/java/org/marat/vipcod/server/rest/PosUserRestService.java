package org.marat.vipcod.server.rest;

import org.marat.vipcod.model.entities.pos.PosUser;
import org.marat.vipcod.model.entities.server.User;
import org.marat.vipcod.model.integration.api.PagedRequest;
import org.marat.vipcod.server.NotAuthException;
import org.marat.vipcod.server.dao.PosRepository;
import org.marat.vipcod.server.dao.PosUserRepository;
import org.marat.vipcod.server.dao.UserRepository;
import org.marat.vipcod.server.rest.model.PosUserStatusResponse;
import org.marat.vipcod.server.rest.model.PosUsersPagedResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

//import javax.ws.rs.QueryParam;

/**
 * Created by Администратор on 28.10.2014.
 */
@Component
@RequestMapping("/posuser")
public class PosUserRestService extends AbstractService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PosUserRepository posUserRepository;

    @Autowired
    PosRepository posRepository;

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public
    @ResponseBody
    PosUsersPagedResponse getPosUsers(
            @RequestBody PagedRequest request,
            HttpSession session
    ) {

        // todo: использовать id сессии
        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        String filter = StringUtils.isEmpty(request.getFilter()) ? "%" : "%" + request.getFilter() + "%";

        /*if (StringUtils.isEmpty(filter)){
            return new PosUsersPagedResponse(
                    posUserRepository.findByOwnerId(userId, new PageRequest(request.getPageNum(), request.getPageSize())),
                    posUserRepository.findTotalByOwnerId(userId)
            );

        } else */{
            return new PosUsersPagedResponse(
                    posUserRepository.findByOwnerId(ownerId, filter, new PageRequest(request.getPageNum(), request.getPageSize())),
                    posUserRepository.findTotalByOwnerId(ownerId, filter)
            );

        }
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public PosUserStatusResponse savePosUser(@RequestBody PosUser posUser, HttpSession session) {
        // todo: присвоить параметры, которые не сериализуемы. Либо скопировать значения
        // сериализуемых параметров в оригинал из пришедшего объекта

        // todo: проверить хозяина posUser

        PosUser originPosUser = StringUtils.isEmpty(posUser.getUuid()) ? null : posUserRepository.findOne(posUser.getUuid());
        if (originPosUser == null) {
            originPosUser = new PosUser();
            User user = getUser(session);
            originPosUser.setOwnerId(user.getCompany().getId());
        }
        posUser.setOwnerId(originPosUser.getOwnerId());
        posUser = posUserRepository.save(posUser);
        return new PosUserStatusResponse(posUser);
    }

}
