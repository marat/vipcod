package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.server.User;

import java.io.Serializable;

/**
* Created by marat on 19.03.2015.
*/
public class SessionInfo implements Serializable {
    User user;

    public SessionInfo() {
    }

    public SessionInfo(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
