package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.schedule.SchedulePlan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchedulePlanRepository extends CrudRepository<SchedulePlan, Long> {
    SchedulePlan findByName(String name);

    @Query("select aa.name from SchedulePlan aa")
    List<String> getAllNames();
}
