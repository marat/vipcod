package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.Coach;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by marat on 18.06.2015.
 */
@Repository
public interface CoachRepository extends CrudRepository<Coach, String> {
    @Query("select p from Coach p where p.ownerId=:ownerId order by p.name")
    List<Coach> findByOwnerId(@Param("ownerId") long ownerId, Pageable p);

    @Query("select p from Coach p where p.ownerId=:ownerId and (upper(p.name) like upper(:filter)) order by p.name")
    List<Coach> findByOwnerId(@Param("ownerId") long ownerId, @Param("filter") String filer, Pageable p);

    @Query("select count(p) from Coach p where p.ownerId=:ownerId and (upper(p.name) like upper(:filter) )")
    long findTotalByOwnerId(@Param("ownerId") Long ownerId, @Param("filter") String filer);

    @Query("select count(p) from Coach p where p.ownerId=:ownerId")
    long findTotalByOwnerId(@Param("ownerId") Long ownerId);

    @Query("select p from Coach p order by p.name")
    Iterable<Coach> findAllNotDeleted();
}
