package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.schedule.SchedulePlan;
import org.marat.vipcod.model.integration.api.StatusResponse;

public class SchedulePlanResponse extends StatusResponse<SchedulePlan> {

    public SchedulePlanResponse() {
    }

    public SchedulePlanResponse(String error) {
        super(error);
    }

    public SchedulePlanResponse(SchedulePlan result) {
        super(result);
    }

    public SchedulePlanResponse(SchedulePlan result, String error) {
        super(result, error);
    }
}
