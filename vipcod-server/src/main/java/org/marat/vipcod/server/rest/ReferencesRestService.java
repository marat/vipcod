package org.marat.vipcod.server.rest;

import org.marat.vipcod.model.entities.server.User;
import org.marat.vipcod.server.NotAuthException;
import org.marat.vipcod.server.dao.SessionRepository;
import org.marat.vipcod.server.dao.UserRepository;
import org.marat.vipcod.server.rest.model.ReferencesResponse;
import org.marat.vipcod.server.rest.model.SessionInfo;
import org.marat.vipcod.server.rest.model.SessionInfoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * Сервис для работы с данными сеанса пользователя
 */
@Component
@RequestMapping("/references")
public class ReferencesRestService extends AbstractService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    SessionRepository sessionRepository;

    @RequestMapping(value = "/sessions", method = RequestMethod.GET)
    public
    @ResponseBody
    ReferencesResponse getInfo(HttpSession session) {
        User user = getUser(session);
        return new ReferencesResponse(sessionRepository.findForEnterprise(user.getCompany().getId()));

    }
}
