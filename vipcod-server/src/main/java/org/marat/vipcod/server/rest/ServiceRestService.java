package org.marat.vipcod.server.rest;

import org.marat.vipcod.model.entities.Tariff;
import org.marat.vipcod.server.NotAuthException;
import org.marat.vipcod.server.dao.ServiceRepository;
import org.marat.vipcod.server.dao.UserRepository;
import org.marat.vipcod.server.rest.model.ServicePagedResponse;
import org.marat.vipcod.server.rest.model.ServiceStatusResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.marat.vipcod.model.entities.Service;
import org.marat.vipcod.model.entities.server.User;
import org.marat.vipcod.model.integration.api.PagedRequest;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

//import javax.ws.rs.QueryParam;

/**
 * Created by Администратор on 28.10.2014.
 */
@Component
@RequestMapping("/service")
public class ServiceRestService extends AbstractService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ServiceRepository serviceRepository;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public
    @ResponseBody
    List<Service> getAll(HttpSession session) {

        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        return serviceRepository.findByOwnerId(ownerId, "%", new PageRequest(0, 9999));
    }


    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public
    @ResponseBody
    ServicePagedResponse getItems(
            @RequestBody PagedRequest request,
            HttpSession session
    ) {

        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        String filter = StringUtils.isEmpty(request.getFilter()) ? "%" : "%" + request.getFilter() + "%";

        return new ServicePagedResponse(
                serviceRepository.findByOwnerId(ownerId, filter, new PageRequest(request.getPageNum(), request.getPageSize())),
                serviceRepository.findTotalByOwnerId(ownerId, filter)
        );
    }

    @RequestMapping(value = "/tree", method = RequestMethod.POST)
    public
    @ResponseBody
    ServicePagedResponse getTree(
            @RequestBody PagedRequest request,
            HttpSession session
    ) {
        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        String filter = StringUtils.isEmpty(request.getFilter()) ? "%" : "%" + request.getFilter() + "%";

        List<Service> services = serviceRepository.findByOwnerId(ownerId, filter, new PageRequest(0, 9999));

        // удаленные (помеченные deleted) тарифы не отображаем
        for (Service service : services) {
            List<Tariff> tariffList = new ArrayList<>();
            for (Tariff tariff : service.getTariffs()) {
                if (!tariff.isDeleted()) tariffList.add(tariff);
            }
            service.setTariffs(tariffList);
        }

        return new ServicePagedResponse(
                services,
                serviceRepository.findTotalByOwnerId(ownerId, filter)
        );
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public
    @ResponseBody
    ServiceStatusResponse saveItem(@RequestBody Service item, HttpSession session) {
        // todo: присвоить параметры, которые не сериализуемы. Либо скопировать значения
        // сериализуемых параметров в оригинал из пришедшего объекта

        // todo: проверить хозяина Service

        Service originItem = null;

        if (item.getId() != null) {
            originItem = serviceRepository.findOne(item.getId());
        }
        if (originItem == null) {
            originItem = new Service(item.getId() == null ? 0 : item.getId());
            User user = getUser(session);
            originItem.setOwnerId(user.getCompany().getId());
        }
        item.setOwnerId(originItem.getOwnerId());

        item = serviceRepository.save(item);
        return new ServiceStatusResponse(item);
    }

    @RequestMapping(value = "/del", method = RequestMethod.GET)
    public void delItem(@RequestParam Long id, HttpSession session) {
        // todo: присвоить параметры, которые не сериализуемы. Либо скопировать значения
        // сериализуемых параметров в оригинал из пришедшего объекта
        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        Service serviceForDel = null;

        if (id != null) {
            serviceForDel = serviceRepository.findOne(id);
        }
        if (serviceForDel == null) {
            throw new RuntimeException("Сервис с идентификатором " + id + " не найден");
        }

        if (serviceForDel.getOwnerId() != ownerId) {
            throw new RuntimeException("Вы не являетесь создателем данного сервиса");
        }

        for (Tariff tariff : serviceForDel.getTariffs()) {
            if (!tariff.isDeleted()) throw new RuntimeException("Сервис с действующими тарифами не может быть удален");
        }

        serviceForDel.setDeleted(true);
        serviceRepository.save(serviceForDel);
    }
}
