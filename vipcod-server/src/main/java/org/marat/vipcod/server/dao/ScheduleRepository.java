package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.Session;
import org.marat.vipcod.model.entities.schedule.ScheduleItem;
import org.marat.vipcod.model.entities.schedule.ScheduleReservation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface ScheduleRepository extends CrudRepository<ScheduleItem, Long> {

    /**
     * Возвращает события календаря, отсортированные таким образом, чтобы можно было их
     * сагрегировать по неделям и месяцу за один обход.
     * Порядок сортировки: день, сеанс, тренер, услуга
     *
     * @param ownerId
     * @param start
     * @param end
     * @return
     */
    @Query("select p from ScheduleItem p where p.enterpriseId=:ownerId and p.date >= :start and p.date < :finish " +
            "order by p.date, p.coach.uuid, p.service.id, p.session.begin")
    List<ScheduleItem> findOrderedForMonth(@Param("ownerId") Long ownerId, @Param("start") Date start, @Param("finish") Date end);

    @Query("select p from ScheduleItem p where p.enterpriseId=:ownerId and p.date >= :start and p.date < :finish " +
            " and p.service.id = :serviceId and p.coach.uuid = :coachUuid " +
            "order by p.date, p.coach.name, p.service.id, p.session.begin")
    List<ScheduleItem> findOrderedForMonth(@Param("ownerId") Long ownerId, @Param("serviceId") Long serviceId, @Param("coachUuid") String coachUuid, @Param("start") Date start, @Param("finish") Date end);

    @Query("select p from ScheduleItem p where p.enterpriseId=:ownerId and p.date >= :start and p.date < :finish " +
            " and p.service.id = :serviceId " +
            "order by p.date, p.session.begin")
    List<ScheduleItem> findOrderedForMonth(@Param("ownerId") Long ownerId, @Param("serviceId") Long serviceId, @Param("start") Date start, @Param("finish") Date end);

    @Query("select p from ScheduleItem p where p.enterpriseId=:ownerId and p.date >= :start and p.date < :finish " +
            "order by p.date, p.session.begin, p.coach.name, p.service.id")
    List<ScheduleItem> findOrderedForWeek(@Param("ownerId") Long ownerId, @Param("start") Date start, @Param("finish") Date end);

    @Query("select p from ScheduleItem p where p.enterpriseId=:ownerId and p.date = :date and p.session.id = :sessionId and p.service.id = :serviceId and p.coach.uuid = :coachUuid")
    List<ScheduleItem> existsEqual(@Param("ownerId") Long ownerId, @Param("date") Date date,
                                   @Param("sessionId") Long sessionId,
                                   @Param("serviceId") Long serviceId,
                                   @Param("coachUuid") String coachUuid);

    @Query("select p from ScheduleItem p where p.enterpriseId=:ownerId and p.date in :dates")
    List<ScheduleItem> findOrderedForDays(@Param("ownerId") Long ownerId, @Param("dates") List<Date> dates);

    @Query("select distinct p.date from ScheduleItem p where p.enterpriseId=:ownerId order by p.date")
    List<Date> getReservedDates(@Param("ownerId") Long ownerId);

    @Query("select sr.schedule.session from ScheduleReservation sr where sr.payment.ownerId=:ownerId and sr.payment.person.deleted = false and " +
            "sr.schedule.coach.uuid=:coachUuid " +
            "and sr.payment.period = :period " +
            "and sr.schedule.session.dayOfWeek in :wdays " +
            "order by sr.schedule.date, sr.schedule.session.begin")
    TreeSet<Session> sessionsForCoach(@Param("ownerId") Long ownerId, @Param("coachUuid") String coachUuid,
                                      @Param("period") Long period, @Param("wdays") Integer[] wdays);

/*
    @Query("select sr.schedule from ScheduleReservation sr where sr.payment.ownerId=:ownerId and sr.payment.person.deleted = false " +
            "and sr.schedule.session.begin=:sessionBegin and sr.schedule.coach.uuid=:coachUuid and sr.payment.period = :period " +
            "order by sr.schedule.date, sr.schedule.session.begin")
    TreeSet<ScheduleItem> findScheduleReservedByCoach(@Param("ownerId") Long ownerId, @Param("sessionBegin") String sessionBegin, @Param("coachUuid") String coachUuid, @Param("period") Long period);
*/

    @Query("select sr from ScheduleReservation sr where sr.payment.ownerId=:ownerId and sr.payment.person.deleted = false " +
            "and sr.schedule.coach.uuid=:coachUuid " +
            "and sr.schedule.session.begin=:sessionBegin " +
            "and sr.schedule.session.dayOfWeek in :wdays " +
            "and sr.payment.period=:period")
    List<ScheduleReservation> findReservationsForCoachAndSchedule
            (@Param("ownerId") Long ownerId, @Param("coachUuid") String coachUuid, @Param("sessionBegin") String sessionBegin,
             @Param("wdays") Integer[] wdays,
             @Param("period") Long period);
}
