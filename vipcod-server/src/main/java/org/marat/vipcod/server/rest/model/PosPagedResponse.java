package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.server.Pos;
import org.marat.vipcod.model.integration.api.PagedResponse;

import java.util.List;

/**
 * Created by Администратор on 06.11.2014.
 */
public class PosPagedResponse extends PagedResponse<Pos> {
    public PosPagedResponse() {
    }

    public PosPagedResponse(List<Pos> items, long total) {
        this.setItems(items);
        this.setTotal(total);
    }
}
