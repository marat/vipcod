package org.marat.vipcod.server.rest;

import org.marat.vipcod.server.rest.controller.RegPosModel;
import org.marat.vipcod.model.entities.server.ClientBase;
import org.marat.vipcod.server.dao.ClientBaseRepository;
import org.marat.vipcod.server.dao.PosRepository;
import org.marat.vipcod.server.dao.PosUserRepository;
import org.marat.vipcod.server.dao.UserRepository;
import org.marat.vipcod.server.rest.model.PosPagedResponse;
import org.marat.vipcod.server.rest.model.PosStatusResponse;
import org.marat.vipcod.server.sso.SsoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.marat.vipcod.model.entities.pos.PosUser;
import org.marat.vipcod.model.entities.server.Pos;
import org.marat.vipcod.model.entities.server.User;
import org.marat.vipcod.model.integration.api.PagedRequest;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.util.UUID;

/**
 * Created by Администратор on 28.10.2014.
 */
@Component
@RequestMapping("/pos")
@Transactional
public class PosRestService extends AbstractService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PosUserRepository posUserRepository;

    @Autowired
    PosRepository posRepository;

    @Autowired
    ClientBaseRepository clientBaseRepository;

    @RequestMapping(value = "/init", method = RequestMethod.GET, produces = "text/plain")
    public
    @ResponseBody
    String initUsers(@RequestParam("pos") String pos) {
        if (pos == null) throw new IllegalArgumentException("pos expected");

        String sessionId = SsoService.instance.createSsoSession(pos);
        //userRestService.initTestUsers(pos);
        return sessionId;
    }

    @RequestMapping(value = "/reg", method = RequestMethod.POST, produces = "text/plain")
    public
    @ResponseBody
    String regPos(@RequestBody RegPosModel model) {
        User user = userRepository.findByEmailAndPassword(model.getUserName(), model.getPassword());
        if (user == null) throw new RuntimeException("User not found by password: " + model.getUserName() + "/" + model.getPassword());
        Long ownerId = user.getCompany().getId();

        Pos pos = new Pos(UUID.randomUUID().toString());
        pos.setOwner(user.getCompany());
        pos.setDescription(model.getDescription());
        user.getCompany().getPos().add(pos);
        pos = posRepository.save(pos);

        /*
        todo: на данном этапе развития системы будет существовать только одна клиентская база на все предприятие (на все его терминалы)
        // создание собственной клиентской базы терминала
        ClientBase posClientBase = new ClientBase();
        posClientBase.setName("Собственная клиентская база терминала " + pos.getUuid());
        posClientBase.setOwnerId(ownerId);
        */
        // привязка в общую клиентскую базу компании-владельца терминала
        ClientBase posClientBase = clientBaseRepository.addTerminalInClientBaseTx(pos, ownerId);
        pos.setClientBase(posClientBase);

        user = userRepository.save(user);

        PosUser posUser123 ;//= posUserRepository.findOne("123", "init user");

        posUser123 = new PosUser("123", "init user");
        posUser123.setOwnerId(ownerId);
        pos.getUsers().add(posUser123);
        posUser123 = posUserRepository.save(posUser123);

        // валидируем сохраненные данные. На этом этапе коммита еще не было - возможно, он упадет.
        // для вменяемого сообщения о ошибке делаем эту валидацию принудительно
        org.marat.vipcod.validator.ValidatorFactory.beanValidate(pos);

        return pos.getUuid();
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public
    @ResponseBody
    PosPagedResponse getPoses(
            @RequestBody PagedRequest request,
            HttpSession session
    ) {

        // todo: использовать id сессии
        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        String filter = StringUtils.isEmpty(request.getFilter()) ? "%" : "%" + request.getFilter() + "%";

        PosPagedResponse res = new PosPagedResponse(
                posRepository.findByOwnerId(ownerId, filter, new PageRequest(request.getPageNum(), request.getPageSize())),
                posRepository.findTotalByOwnerId(ownerId, filter)
        );
        return res;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public
    @ResponseBody
    PosStatusResponse savePos(@RequestBody Pos pos, HttpSession session) {
        // todo: присвоить параметры, которые не сериализуемы. Либо скопировать значения
        // сериализуемых параметров в оригинал из пришедшего объекта
        if (StringUtils.isEmpty(pos.getUuid())) {
            pos.setUuid(UUID.randomUUID().toString());
            //throw new RuntimeException("Идентификатор POS-терминала не указан");
        }
        // todo: проверить хозяина pos-терминала
        Pos originPos = posRepository.findByPosId(pos.getUuid());
        if (originPos == null) {
            originPos = new Pos(pos.getUuid());
            User user = getUser(session);
            originPos.setOwner(user.getCompany());
        }
        pos.setOwner(originPos.getOwner());
        pos.setUpdated(null);
        pos = posRepository.save(pos);
        return new PosStatusResponse(pos);
    }

    @RequestMapping(value = "/delUser", method = RequestMethod.GET)
    public
    @ResponseBody
    PosStatusResponse delUser(@RequestParam("posId") String posId, @RequestParam("userId") String userId, HttpSession session) {
        // сериализуемых параметров в оригинал из пришедшего объекта
        if (StringUtils.isEmpty(posId) || StringUtils.isEmpty(userId)) {
            throw new RuntimeException("Некорректные аргументы");
        }
        // todo: проверить хозяина pos-терминала
        Pos pos = posRepository.findByPosId(posId);
        if (pos == null) {
            throw new RuntimeException("терминал не найден");
        }

        PosUser user = posUserRepository.findOne(userId);
        if (user == null) {
            throw new RuntimeException("сотрудник не найден");
        }

        pos.getUsers().remove(user);
        posUserRepository.save(user);
        pos = posRepository.save(pos);

        return new PosStatusResponse(pos);
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.GET)
    public
    @ResponseBody
    PosStatusResponse addUser(@RequestParam("posId") String posId, @RequestParam("userId") String userId, HttpSession session) {
        // сериализуемых параметров в оригинал из пришедшего объекта
        if (StringUtils.isEmpty(posId) || StringUtils.isEmpty(userId)) {
            throw new RuntimeException("Некорректные аргументы");
        }
        // todo: проверить хозяина pos-терминала
        Pos pos = posRepository.findByPosId(posId);
        if (pos == null) {
            throw new RuntimeException("терминал не найден");
        }

        PosUser user = posUserRepository.findOne(userId);
        if (user == null) {
            throw new RuntimeException("сотрудник не найден");
        }

        pos.getUsers().add(user); // todo: сделать у юзверя несколько посов
        posUserRepository.save(user);
        pos = posRepository.save(pos);

        return new PosStatusResponse(pos);
    }
}
