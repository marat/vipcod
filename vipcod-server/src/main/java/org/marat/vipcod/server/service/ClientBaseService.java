package org.marat.vipcod.server.service;

import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.entities.server.ClientBase;
import org.marat.vipcod.model.entities.server.Pos;
import org.marat.vipcod.server.dao.ClientBaseRepository;
import org.marat.vipcod.server.dao.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Marat on 17.02.2015.
 */
@Component
public class ClientBaseService {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    ClientBaseRepository clientBaseRepository;


    public Person savePersonInBase(Person person, Pos pos) {
        ClientBase base = clientBaseRepository.savePersonInBaseTx(person, pos);

        return person;
    }
}
