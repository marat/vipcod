package org.marat.vipcod.server.rest;

import org.marat.vipcod.DateUtils;
import org.marat.vipcod.model.entities.Payment;
import org.marat.vipcod.model.entities.server.User;
import org.marat.vipcod.model.integration.api.impl.PaymentPagedRequest;
import org.marat.vipcod.server.dao.PaymentRepository;
import org.marat.vipcod.server.dao.PersonRepository;
import org.marat.vipcod.server.dao.PosRepository;
import org.marat.vipcod.server.dao.ScheduleReservationRepository;
import org.marat.vipcod.server.rest.model.PaymentPagedResponse;
import org.marat.vipcod.server.service.excel.impl.DayReportData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

//import javax.ws.rs.QueryParam;

/**
 * Created by Администратор on 28.10.2014.
 */
@Component
@RequestMapping("/seansList")
public class SeansListRestService extends AbstractService {

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    ScheduleReservationRepository scheduleReservationRepository;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    PosRepository posRepository;


    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public
    @ResponseBody
    PaymentPagedResponse getPayments(
            @RequestBody PaymentPagedRequest request,
            HttpSession session
    ) {
        logger.info("getPayments: " + request);

        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        if (request.getCoachUuid() == null)
            throw new RuntimeException("coachUid expected.");

        PaymentPagedResponse response;

        response = new PaymentPagedResponse(
                paymentRepository.findByParams(
                        ownerId,
                        request.getPersonUuid(),
                        request.getServiceId(),
                        request.getCoachUuid(),
                        request.getPeriod(),
                        request.getFilter(),
                        new PageRequest(request.getPageNum(), request.getPageSize())),

                paymentRepository.findTotalByParams(
                        ownerId,
                        request.getPersonUuid(),
                        request.getServiceId(),
                        request.getCoachUuid(),
                        request.getPeriod(),
                        request.getFilter()
                )
        );

        if (!response.getItems().isEmpty()) {
            response.setPerson(response.getItems().get(0).getPerson());
        } else {
            response.setPerson(request.getPersonUuid() == null ? null : personRepository.findOne(request.getPersonUuid()));
        }

        return response;
    }
}
