package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.Payment;
import org.marat.vipcod.model.integration.api.StatusResponse;

/**
 * Created by marat on 26.06.2015.
 */
public class PaymentStatusResponse extends StatusResponse<Payment> {
    public PaymentStatusResponse(Payment payment) {
        setResult(payment);
    }

    public PaymentStatusResponse(Payment result, String error) {
        super(result, error);
    }

    public PaymentStatusResponse() {
    }

    public PaymentStatusResponse(String error) {
        super(error);
    }
}
