package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.entities.server.Pos;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface PersonRepository extends CrudRepository<Person, String> {
    Person findByName(String name);

    @Query("select p from Person p where p.ownerId=:ownerId and (p.deleted is null or p.deleted <> true) and (upper(p.name) like upper(:filter) or upper(p.secondName) like upper(:filter) or upper(p.middleName) like upper(:filter) or upper(p.phone) like upper(:filter) or p.currentCardEan = :strongFilter) order by p.secondName, p.name")
    List<Person> findByOwnerId(@Param("ownerId") Long ownerId, @Param("filter") String filter, @Param("strongFilter") String strongFilter, Pageable p);

    @Query("select count(p) from Person p where p.ownerId=:ownerId and (p.deleted is null or p.deleted <> true) and (upper(p.name) like upper(:filter) or upper(p.secondName) like upper(:filter) or upper(p.middleName) like upper(:filter) or upper(p.phone) like upper(:filter) or p.currentCardEan = :strongFilter)")
    long findTotalByOwnerId(@Param("ownerId") Long ownerId, @Param("filter") String filter, @Param("strongFilter") String strongFilter);

    @Query("select p from Person p where p.ownerId=:ownerId and (p.deleted is null or p.deleted <> true) and (upper(p.name) like upper(:filter2) and upper(p.secondName) like upper(:filter1)) order by p.secondName, p.name")
    List<Person> findByOwnerId2(@Param("ownerId") Long ownerId, @Param("filter1") String filter1, @Param("filter2") String filter2, Pageable p);

    @Query("select count(p) from Person p where p.ownerId=:ownerId and (p.deleted is null or p.deleted <> true) and (upper(p.name) like upper(:filter2) or upper(p.secondName) like upper(:filter1))")
    long findTotalByOwnerId2(@Param("ownerId") Long ownerId, @Param("filter1") String filter1, @Param("filter2") String filter2);

    @Query("select pp from ClientBase cb inner join cb.persons pp where :pos member of cb.poses and (pp.created > :prevVersion or pp.updated > :prevVersion) and (pp.created < :untilVersion or pp.updated < :untilVersion)")
    List<Person> findByVersions(@Param("pos") Pos pos, @Param("prevVersion") Long prevVersion, @Param("untilVersion") Long untilVersion);

    @Query("select coalesce(max(p.updated), 0) from Person p")
    Long getCurrentVersion();

    Collection<Person> findAllByCurrentCardEan(String currentCardEan);
}
