package org.marat.vipcod.server.rest.reports;

import org.marat.vipcod.DateUtils;
import org.marat.vipcod.model.entities.Payment;
import org.marat.vipcod.model.entities.server.User;
import org.marat.vipcod.model.integration.api.StatusResponse;
import org.marat.vipcod.server.dao.PaymentRepository;
import org.marat.vipcod.server.dao.UserRepository;
import org.marat.vipcod.server.rest.AbstractService;
import org.marat.vipcod.server.rest.model.BalanceItem;
import org.marat.vipcod.server.service.excel.impl.DayPaymentsExportService;
import org.marat.vipcod.server.service.excel.impl.DayReportData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 */
@Component
@RequestMapping("/report")
public class DailyReportWebService extends AbstractService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    DayPaymentsExportService dayPaymentsExportService;

    @Autowired
    private NamedParameterJdbcTemplate jt;


    /**
     * содержимое таблицы для страницы "Дневной отчет"
     */
    @RequestMapping(value = "/daily", method = RequestMethod.GET)
    public
    @ResponseBody
    StatusResponse<ArrayList<Payment>> getPayments(
            @RequestParam("date") String dateStr,
            @RequestParam("nds") Boolean nds,
            HttpSession session
    ) throws Exception {

        // todo: использовать id сессии
        User user = getUser(session);

        //Date date = DateUtils.yyyy_MM_dd_HH_mm_ss_SSSZ.parse(dateStr);
        Date date = DateUtils.yyyy_MM_dd.parse(dateStr);

        List<Payment> result;

        if (nds != null) {
            result = paymentRepository.findAllByDateAndNds(
                    user.getCompany().getId(),
                    nds,
                    DateUtils.getStart(date),
                    DateUtils.getEnd(date));
        } else {
            result = paymentRepository.findAllByDate(
                    user.getCompany().getId(),
                    DateUtils.getStart(date),
                    DateUtils.getEnd(date));
        }
        // для дворца спорта не отображаем возвраты (отрицательные платежи)
        return new StatusResponse<>((ArrayList<Payment>) result.stream().filter(p -> p.getSum() > 0).collect(Collectors.toList()));
    }


    @RequestMapping(value = "/balance", method = RequestMethod.GET)
    public
    @ResponseBody
    StatusResponse<ArrayList<BalanceItem>> getBalances(
            @RequestParam("dateFrom") String dateStrFrom,
            @RequestParam("dateTo") String dateStrTo,
            HttpSession session
    ) throws ParseException {

        User user = getUser(session);

        Date dateFrom = DateUtils.yyyy_MM_dd.parse(dateStrFrom);
        Date dateTo = DateUtils.yyyy_MM_dd.parse(dateStrTo);

        List<Payment> pays = paymentRepository.findAllByDateForBalance(
                user.getCompany().getId(),
                DateUtils.getStart(dateFrom),
                DateUtils.getEnd(dateTo));

        return new StatusResponse(calculateBalance(pays));
    }

    private ArrayList<BalanceItem> calculateBalance(List<Payment> pays) {
        if (pays == null || pays.isEmpty()) return new ArrayList<>();

        ArrayList<BalanceItem> balanceItems = new ArrayList<>();
        BalanceItem balance = new BalanceItem(pays.get(0).getPerson(), pays.get(0).getService());
        balanceItems.add(balance);

        Date payBackDay = null;
        for (Payment p : pays) {
            assert p.getPerson() != null;

            if (!Objects.equals(p.getPerson(), balance.getPerson()) || !Objects.equals(p.getService(), balance.getService()) ) {
                // новый блок платежей. начинаем копить новый
                balance = new BalanceItem(p.getPerson(), p.getService());
                balanceItems.add(balance);
            }

            if (p.getBackSum() != 0) {
                if (payBackDay == null) payBackDay = DateUtils.getStart(p.getPayTime());
            }
            balance.setSumPeriod(balance.getSumPeriod() + p.getSum());
        }

        return balanceItems;
    }

    @RequestMapping(value = "/balance/{dateFrom}/{dateTo}.xlsx", method = RequestMethod.GET)
    void downloadAsXlsx(HttpServletResponse response,
                        @PathVariable("dateFrom") String dateStrFrom,
                        @PathVariable("dateTo") String dateStrTo,
                        HttpSession session) throws IOException, ParseException {
        User user = getUser(session);

        response.setContentType("application/octet-stream");

        Date dateFrom = DateUtils.yyyy_MM_dd.parse(dateStrFrom);
        Date dateTo = DateUtils.yyyy_MM_dd.parse(dateStrTo);

        StatusResponse<ArrayList<BalanceItem>> balancesResponse = getBalances(dateStrFrom, dateStrTo, session);
        if (balancesResponse.getError() != null) throw new IOException(balancesResponse.getError());
        dayPaymentsExportService.reportBalance(balancesResponse.getResult(), dateFrom, dateTo, response.getOutputStream());
    }


    @RequestMapping(value = "/daily/{date}.xlsx", method = RequestMethod.GET)
    void downloadAsXlsx(HttpServletResponse response,
                        HttpSession session,
                        @PathVariable("date") String dateStr, @RequestParam Boolean nds) throws IOException, ParseException {
        User user = getUser(session);

        response.setContentType("application/octet-stream");

        Date date = DateUtils.yyyy_MM_dd.parse(dateStr);

        List<Payment> result = null;

        if (StringUtils.isEmpty(nds)) {
            result = paymentRepository.findAllByDate(
                    user.getCompany().getId(),
                    DateUtils.getStart(date),
                    DateUtils.getEnd(date));
        } else {
            result = paymentRepository.findAllByDateAndNds(
                    user.getCompany().getId(),
                    nds,
                    DateUtils.getStart(date),
                    DateUtils.getEnd(date));
        }

        List<Payment> paymentCorrects = calculateCorrects(result);

        // для дворца спорта не отображаем возвраты (отрицательные платежи)
        result = result.stream().filter(p -> p.getSum() > 0).collect(Collectors.toList());

        dayPaymentsExportService.report(new DayReportData(result, paymentCorrects), date, response.getOutputStream(), nds);
    }


    private List<Payment> calculateCorrects(List<Payment> payments) {
        List<Payment> corrects = new ArrayList<>();

        for (Payment payment : payments) {
            if (payment.getBackSum() != 0) {
                Payment correct = new Payment(payment);

                correct.setBackSum(0);
                Double price = (payment.getSum() + payment.getBackSum()) / payment.getVisitsCount();
                correct.setSum(payment.getSum() - payment.getVisitsCountDvSp() * price);
                correct.setBackSum(0);
                correct.setVisitsCount(null);

                payment.setSum(price * payment.getVisitsCountDvSp());

                if (correct.getSum() > 0) corrects.add(correct);
            }
        }

        return corrects;
    }
}
