package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.schedule.ScheduleItem;
import org.marat.vipcod.model.integration.api.StatusResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marat on 06.04.2016.
 */
public class ScheduleStatusResponse extends StatusResponse<ArrayList<ScheduleItem>> {
    public ScheduleStatusResponse() {
    }

    public ScheduleStatusResponse(String error) {
        super(error);
    }

    public ScheduleStatusResponse(ArrayList<ScheduleItem> result) {
        setResult(result);
    }

    public ScheduleStatusResponse(ArrayList<ScheduleItem> result, String error) {
        super(result, error);
    }
}
