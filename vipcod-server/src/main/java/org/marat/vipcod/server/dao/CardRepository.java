package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.server.Pos;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.marat.vipcod.model.entities.Card;

import java.util.List;

@Repository
public interface CardRepository extends CrudRepository<Card, String> {
    Card findByEan(String ean);

    @Query("select p from Card p where p.ean=:ean")
    Card findFullByEan(@Param("ean") String ean);

    @Query("select p from Card p where p.ownerId=:ownerId")
    List<Card> findByOwnerId(@Param("ownerId") long ownerId, Pageable p);

    @Query("select p from Card p where p.ownerId=:ownerId and (p.ean like :filter or upper(p.person.name) like upper(:filter) )")
    List<Card> findByOwnerId(@Param("ownerId") long ownerId, @Param("filter") String filer, Pageable p);

    @Query("select count(p) from Card p where p.ownerId=:ownerId and (p.ean like :filter or upper(p.person.name) like upper(:filter) )")
    long findTotalByOwnerId(@Param("ownerId") Long ownerId, @Param("filter") String filer);

    @Query("select count(p) from Card p where p.ownerId=:ownerId")
    long findTotalByOwnerId(@Param("ownerId") Long ownerId);

    //@Query("select aa from Card aa where aa.person in (select cb.persons from ClientBase cb where :pos member of cb.poses) and (aa.created > :prevVersion or aa.updated > :prevVersion) and (aa.created < :untilVersion or aa.updated < :untilVersion)")
    //todo: проверить корректность запроса с условием and (card.created > :prevVersion or card.updated > :prevVersion) and (card.created < :untilVersion or card.updated < :untilVersion). Не понятно почему person.xxx - может копипаст, но вроде работает
    @Query("select card from Card card, ClientBase cb " +
            "inner join cb.persons person " +
            "where card.person.uuid = person.uuid and :pos member of cb.poses and (card.created > :prevVersion or card.updated > :prevVersion) and (card.created < :untilVersion or card.updated < :untilVersion)")
    List<Card> findByVersions(@Param("pos") Pos pos, @Param("prevVersion") Long prevVersion, @Param("untilVersion") Long untilVersion);

    @Query("select coalesce(max(p.updated), 0) from Card p")
    long currentVersion();

    @Query(value = "select 1+coalesce(max(p.ean\\:\\:numeric), 0) from Cards p", nativeQuery = true)
    Long getNextEanForNewCard();
}
