package org.marat.vipcod.server.rest.controller;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created by Администратор on 29.10.2014.
 */
public class LoginModel {
    @NotNull
    @NotBlank(message = "поле не может быть пустым")
    @Email
    String email;

    @NotNull
    @NotBlank(message = "поле не может быть пустым")
    String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
