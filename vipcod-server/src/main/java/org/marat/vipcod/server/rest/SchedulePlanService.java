package org.marat.vipcod.server.rest;

import org.marat.vipcod.model.entities.Coach;
import org.marat.vipcod.model.entities.Service;
import org.marat.vipcod.model.entities.Session;
import org.marat.vipcod.model.entities.schedule.ScheduleItem;
import org.marat.vipcod.model.entities.schedule.ScheduleItemGroup;
import org.marat.vipcod.model.entities.schedule.SchedulePlan;
import org.marat.vipcod.model.entities.server.User;
import org.marat.vipcod.model.integration.api.StatusResponse;
import org.marat.vipcod.server.dao.*;
import org.marat.vipcod.server.rest.model.PlanItemRequest;
import org.marat.vipcod.server.rest.model.SchedulePlanResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by marat on 22.06.2015.
 */
@Component
@RequestMapping("/plan")
public class SchedulePlanService extends AbstractService {

    private static final SimpleDateFormat FULL_CALENDAR_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    CoachRepository coachRepository;

    @Autowired
    ServiceRepository serviceRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
    SchedulePlanRepository schedulePlanRepository;

    @Autowired
    SessionRepository sessionRepository;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public
    @ResponseBody
    SchedulePlanResponse addItem(@Valid @RequestBody SchedulePlan schedulePlan, HttpSession session) throws ParseException {
        User user = getUser(session);

        if (Objects.equals(schedulePlan.getName(), "default")) schedulePlan.setId(1L);

        SchedulePlan sp = schedulePlanRepository.findByName(schedulePlan.getName());
        if (sp != null) {
            schedulePlan.setId(sp.getId());
        }

        return new SchedulePlanResponse(schedulePlanRepository.save(schedulePlan));
    }

    @RequestMapping(value = "/get/{name}", method = RequestMethod.GET)
    public
    @ResponseBody
    String getPlan(@Valid @PathVariable(value = "name") String name, HttpSession session) throws ParseException {
        return schedulePlanRepository.findByName(name).getPlan();
    }


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public
    @ResponseBody
    List<String> getPlanList(HttpSession session) throws ParseException {
        return schedulePlanRepository.getAllNames();
    }


    @RequestMapping(value = "/reservedDates", method = RequestMethod.GET)
    public
    @ResponseBody
    List<Date> getReservedDates(HttpSession session) throws ParseException {
        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        return scheduleRepository.getReservedDates(ownerId);
    }


    //@RequestMapping(value = "/save", method = RequestMethod.POST)
    public
    @ResponseBody
    StatusResponse<ScheduleItem> savePlanItem(
            @RequestBody PlanItemRequest request,
            HttpSession session
    ) throws ParseException {
        User user = getUser(session);

        ScheduleItem item = new ScheduleItem();

        Session seasonSession = sessionRepository.findOne(request.getSession());
        item.setSession(seasonSession);
        item.setCoach(coachRepository.findOne(request.getCoach()));
        if (request.getService() != null) {
            item.setService(serviceRepository.findOne(request.getService()));
        }
        item.setEnterpriseId(user.getCompany().getId());

        Calendar c = Calendar.getInstance();
        c.set(9990, Calendar.JANUARY, seasonSession.getDayOfWeek(), 0, 0, 0);
        c.set(Calendar.MILLISECOND, 0);
        item.setDate(c.getTime());

        ScheduleItem result = scheduleRepository.save(item);
        return new StatusResponse<>(result);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public
    @ResponseBody
    List<ScheduleItem> getItems(
            @RequestParam("start") String start,
            @RequestParam("end") String end,
            @RequestParam("view") String view,
            HttpSession session
    ) throws ParseException {

        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        if (view.toLowerCase().equals("month")) {
            throw new RuntimeException("month view does not support for Planning Schedule");
        }

        List<ScheduleItem> schedule = scheduleRepository.findOrderedForWeek(ownerId, FULL_CALENDAR_DATE_FORMAT.parse(start), FULL_CALENDAR_DATE_FORMAT.parse(end));

        if (view.toLowerCase().equals("agendaweek")) {
            return prepareForWeek(schedule);
        }

        // agendaDay
        return schedule;
    }

    /**
     * отображение событий в разбивке по неделям
     *
     * @param schedule
     * @return
     */
    private List<ScheduleItem> prepareForWeek(List<ScheduleItem> schedule) {
        String lastKey = "";
        List<ScheduleItem> result = new ArrayList<>();
        ScheduleItemGroup currentGroup = new ScheduleItemGroup();

        // Порядок сортировки: день, сеанс, тренер, услуга
        for (ScheduleItem it : schedule) {
            String key = it.getDate() + "_" + it.getSession().hashCode() /*+ "_" + it.getCoach().getUuid() + "_" + it.getService().getId()*/;
            if (!key.equals(lastKey)) {
                currentGroup = new ScheduleItemGroup();
                result.add(currentGroup);
                currentGroup.setDate(it.getDate());
                currentGroup.setBegin(it.getStart());
                currentGroup.setEnd(it.getEnd());
                lastKey = key;
            }
//            currentGroup.setTitle(it.getCoach().getName());
//            currentGroup.setSubtitle((it.getService() == null ? "" : (it.getService().getName())) + " [" + (it.getLimit() == null ? 0 : it.getLimit()) + "]");

            //currentGroup.addTitle(it.getCoach().getName() + (it.getService() == null ? "" : (" - " + it.getService().getName())) + " [" + (it.getLimit() == null ? 0 : it.getLimit()) + "]");
            currentGroup.addTitle(it.getCoach().getName() + " [" + (it.getLimit() == null ? 0 : it.getLimit()) + "]");

            //currentGroup.setSubtitle();
        }

        return result;
    }

    /**
     * отображение событий в разбивке по месяцам
     *
     * @param schedule
     * @return
     */
    private List<ScheduleItem> prepareForMonth(List<ScheduleItem> schedule) {
        Map<String, Long> map = new HashMap<>();

        Date lastDay = new Date(0);
        Date day = new Date(0);
        String lastKey = "";
        List<ScheduleItem> result = new ArrayList<>();
        ScheduleItemGroup lastGroup = new ScheduleItemGroup();

        // Порядок сортировки: день, тренер, услуга
        for (ScheduleItem it : schedule) {
            day = it.getDate();
            if (!day.equals(lastDay)) {
                if (!map.isEmpty()) {
                    // выгружаем map в ответ
                    lastGroup = new ScheduleItemGroup();
                    lastGroup.setDate(lastDay);
                    result.add(lastGroup);
                    for (String title : map.keySet()) {
                        lastGroup.addTitle(title + " " + map.get(title));
                    }
                }

                map.clear();
                lastDay = day;
            }

            String key = it.getCoach().getName() + " - " + it.getService().getName();
            if (!key.equals(lastKey)) {

                if (!map.containsKey(key)) {
                    map.put(key, it.getLimit());
                } else {
                    map.put(key, map.get(key) + it.getLimit());
                }
            }
        }

        lastGroup = new ScheduleItemGroup();
        lastGroup.setDate(day);
        result.add(lastGroup);
        for (String title : map.keySet()) {
            lastGroup.addTitle(title + " " + map.get(title));
        }

        return result;
    }

    Iterable<Coach> coaches = null;
    Iterator<Coach> coachIterator = null;
    Iterable<Service> services = null;
    Iterator<Service> serviceIterator = null;

    public Iterable<Coach> getCoaches() {
        if (coaches == null) coaches = coachRepository.findAll();
        return coaches;
    }

    public Iterator<Coach> getCoachIterator() {
        if (coachIterator == null || !coachIterator.hasNext()) coachIterator = getCoaches().iterator();
        return coachIterator;
    }

    public Iterable<Service> getServices() {
        if (services == null) services = serviceRepository.findAll();
        return services;
    }

    public Iterator<Service> getServiceIterator() {
        if (serviceIterator == null || !serviceIterator.hasNext()) serviceIterator = getServices().iterator();
        return serviceIterator;
    }

    static int i = 1;
    static Map<Long, Session> sess = new HashMap();

}
