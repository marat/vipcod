package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.Payment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by marat on 18.06.2015.
 */
@Repository
public interface PaymentRepository extends CrudRepository<Payment, String>, PaymentRepositoryCustom {
    @Query("select p from Payment p where p.ownerId=:ownerId and p.person.deleted = false order by p.created desc")
    List<Payment> findByOwnerId(@Param("ownerId") long ownerId, Pageable p);

    @Query("select count(p) from Payment p where p.ownerId=:ownerId and p.person.deleted = false")
    long findTotalByOwnerId(@Param("ownerId") Long ownerId);

    @Query("select p from Payment p where p.ownerId=:ownerId and p.person.uuid=:personUuid and p.person.deleted = false order by p.created desc")
    List<Payment> findByOwnerIdAndPersonId(@Param("ownerId") Long userId, @Param("personUuid") String personUuid, Pageable p);

    @Query("select p from Payment p where p.sum <> 0 and p.ownerId=:ownerId and p.payTime >= :dateBegin and p.payTime < :dateEnd and p.person.deleted = false order by p.created desc")
    List<Payment> findAllByDate(@Param("ownerId") Long ownerId, @Param("dateBegin") Date dateBegin, @Param("dateEnd") Date dateEnd);

    @Query("select p from Payment p where p.sum <> 0 and p.ownerId=:ownerId and p.payTime >= :dateBegin and p.payTime < :dateEnd and p.person.deleted = false order by p.person.secondName, p.person.id, p.service.name, p.payTime asc")
    List<Payment> findAllByDateForBalance(@Param("ownerId") Long ownerId, @Param("dateBegin") Date dateBegin, @Param("dateEnd") Date dateEnd);

    @Query("select p from Payment p where p.sum <> 0 and p.ownerId=:ownerId and p.nds = :nds and p.payTime > :dateBegin and p.payTime < :dateEnd and p.person.deleted = false order by p.created desc")
    List<Payment> findAllByDateAndNds(@Param("ownerId") Long ownerId, @Param("nds") Boolean nds, @Param("dateBegin") Date dateBegin, @Param("dateEnd") Date dateEnd);

    List<Payment> findAllByEan(@Param("ean") String ean);

    @Query("select p from Payment p where p.ean = :ean order by p.payTime desc, p.created ")
    List<Payment> findOrderedByEan(@Param("ean") String ean);

    @Query("select p from Payment p where p.ownerId=:ownerId and p.person.uuid=:personUuid and p.person.deleted = false and p.period >= :periodFrom and p.period < :periodTo order by p.created desc")
    List<Payment> findByOwnerIdAndPersonIdAndPeriod(@Param("ownerId") Long userId, @Param("personUuid") String personUuid, @Param("periodFrom") Long periodFrom, @Param("periodTo") Long periodTo);
}
