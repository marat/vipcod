package org.marat.vipcod.server.service.excel;

import net.sf.jett.transform.ExcelTransformer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public abstract class JettExportService {
    private static final Logger LOG = LoggerFactory.getLogger(JettExportService.class);

    public void jettReport(Object reportEntity, Map<String, Object> beans, OutputStream outputStream, String templateFile) {
        InputStream fileIn;
        try {
            fileIn = JettExportService.class.getResourceAsStream(templateFile);
            if (fileIn == null) throw new RuntimeException("file not found: " + templateFile);
            //fileIn = new FileInputStream("D:\\WORK\\vipcod\\vipcod-server\\src\\main\\resources\\reports\\dayReportTemplate.xlsx");

            if (!beans.containsKey("report")){
                beans.put("report", reportEntity);
                beans.put("currentDateTime", SimpleDateFormat.getDateTimeInstance().format(new Date()));
            }

            ExcelTransformer transformer = new ExcelTransformer();
            Workbook workbook = transformer.transform(fileIn, beans);
            workbook.write(outputStream);

            outputStream.close();
        } catch (IOException e) {
            LOG.error("IOException reading " + templateFile + ": ", e);
            //System.err.println("IOException reading " + templateFile + ": " + e.getMessage());
            throw new RuntimeException(e);
        } catch (InvalidFormatException e) {
            LOG.error("InvalidFormatException reading " + templateFile + ": ", e);
            //System.err.println("InvalidFormatException reading " + templateFile + ": " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

}
