package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.Tariff;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.marat.vipcod.model.entities.Service;

import java.util.List;

@Repository
public interface ServiceRepository extends CrudRepository<Service, Long> {

    @Query("select s from Service s where s.ownerId=:ownerId and s.deleted <> true and (upper(s.name) like upper(:filter) ) order by s.name")
    List<Service> findByOwnerId(@Param("ownerId") long ownerId, @Param("filter") String filer, Pageable p);

    @Query("select count(s) from Service s where s.ownerId=:ownerId and s.deleted <> true and (upper(s.name) like upper(:filter) )")
    long findTotalByOwnerId(@Param("ownerId") Long ownerId, @Param("filter") String filer);

    @Query("select s from Service s where s.deleted <> true order by s.name")
    Iterable<Service> findAllNotDeleted();

    @Query("select aa from Service aa where aa.ownerId = :ownerId and (aa.created > :prevVersion or aa.updated > :prevVersion) and (aa.created < :untilVersion or aa.updated < :untilVersion)")
    List<Service> findByVersions(@Param("ownerId") Long ownerId, @Param("prevVersion") Long prevVersion, @Param("untilVersion") Long untilVersion);

}
