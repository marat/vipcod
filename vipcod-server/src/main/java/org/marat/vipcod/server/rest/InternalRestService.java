package org.marat.vipcod.server.rest;

import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.Payment;
import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.server.dao.CardRepository;
import org.marat.vipcod.server.dao.PaymentRepository;
import org.marat.vipcod.server.dao.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.function.Consumer;

/**
 * Created by maratislamov on 12.01.16.
 */
@Component
@RequestMapping("/internal")
public class InternalRestService extends AbstractService {

    // eans: Card, Payment, ActionHistory, PosUser, ReportItem

    @Autowired
    CardRepository cardRepository;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    PersonRepository personRepository;

    private static Long currentReEan = 0L;

    public String getNextEan() {
        return String.format("%06d", ++currentReEan);
    }

    @RequestMapping(value = "/updateEans", method = RequestMethod.GET)
    public
    @ResponseBody
    Long updateEans(
            HttpSession session
    ) {
        LOG.info("internal service -> update eans started...");

        try {
            final long[] i = {0};

            Consumer<Card> action = card -> {
                ++i[0];

                String newEan = getNextEan();
                String oldEan = card.getEan();

                LOG.info("{} -> {}", oldEan, newEan);

                Card newCard = new Card(newEan);
                card.copyTo(newCard);
                newCard.setEan(newEan);
                cardRepository.save(newCard);

                // Payment
                Collection<Payment> payments = paymentRepository.findAllByEan(oldEan);
                for (Payment payment : payments) {
                    payment.setEan(newEan);
                }
                paymentRepository.save(payments);

                // Person
                Collection<Person> persons = personRepository.findAllByCurrentCardEan(oldEan);
                for (Person pers : persons) {
                    pers.setCurrentCardEan(newEan);
                }
                personRepository.save(persons);

                cardRepository.delete(card);
            };

            Iterable<Card> cards = cardRepository.findAll();
            cards.forEach(action);

            return i[0];
        } finally {
            LOG.info("internal service -> update eans DONE.");
        }
    }
}