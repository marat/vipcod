package org.marat.vipcod.server.rest.controller;

import org.marat.vipcod.model.entities.server.ClientBase;
import org.marat.vipcod.model.entities.server.Company;
import org.marat.vipcod.server.dao.ClientBaseRepository;
import org.marat.vipcod.server.dao.CompanyRepository;
import org.marat.vipcod.server.dao.UserRepository;
import org.marat.vipcod.server.rest.AbstractService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.marat.vipcod.model.entities.server.User;

import javax.servlet.http.HttpSession;
import javax.validation.*;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Администратор on 28.10.2014.
 */
@Component
@RequestMapping("/api")
public class RegistrationController extends AbstractService {
    Logger LOG = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    UserRepository userRepository;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    ClientBaseRepository clientBaseRepository;

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpSession session) {
        session.removeAttribute("user");
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public void login(
            @RequestBody LoginModel loginModel,
            HttpSession session) {

        Set<ConstraintViolation<LoginModel>> errors = validator.validate(loginModel);
        if (errors.isEmpty()) {

            User user = userRepository.findByEmailAndPassword(loginModel.getEmail(), loginModel.getPassword());
            if (user == null) {
                throw new RuntimeException("Пользователь " + loginModel.getEmail() + " с таким паролем не найден");
            } else {
                session.setAttribute("user", user);
                return;
            }
        }
        throw new RuntimeException("Ошибки: " + Arrays.toString(errors.toArray()));
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public void login(
            @RequestBody RegistrationModel registrationModel,
            HttpSession session) {

        Set<ConstraintViolation<RegistrationModel>> errors = validator.validate(registrationModel);

        if (errors.isEmpty()) {

            User existUser = userRepository.findByNameOrEmail(registrationModel.getName(), registrationModel.getEmail());
            if (existUser == null) {
                Company company = new Company();
                company.setName(registrationModel.getName());
                company = companyRepository.save(company);

                ClientBase cb = new ClientBase();
                cb.setOwnerId(company.getId());
                cb.setName("Клиентская база компании " + company.getName());
                clientBaseRepository.save(cb);

                User user = new User();
                user.setEmail(registrationModel.getEmail());
                user.setName(registrationModel.getName());
                user.setPhone(registrationModel.getPhone());
                user.setPassword(registrationModel.getPassword());
                user.setCompany(company);
                user.setRoles("KMS");
                userRepository.save(user);
                return;
            }

            throw new RuntimeException("Пользователь с таким email уже зарегистрирован");
        }

        throw new RuntimeException("Ошибки: " + Arrays.toString(errors.toArray()));
    }
}
