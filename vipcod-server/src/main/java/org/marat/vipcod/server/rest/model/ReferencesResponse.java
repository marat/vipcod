package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.server.Pos;
import org.marat.vipcod.model.integration.api.PagedResponse;

import java.io.Serializable;
import java.util.List;

/**
 * Created by marat on 23.06.2015.
 */
public class ReferencesResponse<T extends Serializable> extends PagedResponse<T> {

    public ReferencesResponse() {
    }

    public ReferencesResponse(List<T> items) {
        setItems(items);
    }
}
