package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.integration.api.PagedResponse;

import java.util.List;

/**
 * Created by marat on 18.11.2014.
 */
public class PersonPagedResponse extends PagedResponse<Person> {
    public PersonPagedResponse() {
    }

    public PersonPagedResponse(List<Person> persons, long total) {
        setItems(persons);
        setTotal(total);
    }

    public PersonPagedResponse(List<Person> collect) {
        setItems(collect);
        setTotal(collect.size());
    }
}
