package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.server.Pos;
import org.marat.vipcod.model.integration.api.StatusResponse;

/**
 * Created by Администратор on 17.11.2014.
 */
public class PosStatusResponse extends StatusResponse<Pos> {
    public PosStatusResponse() {
    }

    public PosStatusResponse(String error) {
        super(error);
    }

    public PosStatusResponse(Pos result) {
        super(result);
    }

    public PosStatusResponse(Pos result, String error) {
        super(result, error);
    }
}
