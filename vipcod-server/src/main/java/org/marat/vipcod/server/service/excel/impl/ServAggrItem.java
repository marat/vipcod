package org.marat.vipcod.server.service.excel.impl;

import java.io.Serializable;

public class ServAggrItem implements Serializable {

    private String servName;

    private Long count;
    private Long ecount;
    private Long ccount;

    private Double sum;
    private Double eSum;
    private Double cSum;

    public ServAggrItem() {
    }

    public ServAggrItem(String servName, Long count, Long ecount, Long ccount, Double sum, Double esum, Double csum) {
        this.servName = servName;
        this.count = count;
        this.ecount = ecount;
        this.ccount = ccount;
        this.sum = sum;
        this.eSum = esum;
        this.cSum = csum;
    }

    public Long getEcount() {
        return ecount;
    }

    public void setEcount(Long ecount) {
        this.ecount = ecount;
    }

    public Long getCcount() {
        return ccount;
    }

    public void setCcount(Long ccount) {
        this.ccount = ccount;
    }

    public String getServName() {
        return servName;
    }

    public void setServName(String servName) {
        this.servName = servName;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Double geteSum() {
        return eSum;
    }

    public void seteSum(Double eSum) {
        this.eSum = eSum;
    }

    public Double getcSum() {
        return cSum;
    }

    public void setcSum(Double cSum) {
        this.cSum = cSum;
    }

    @Override
    public String toString() {
        return "ServAggrItem{" +
                "servName='" + servName + '\'' +
                ", count=" + count +
                ", ecount=" + ecount +
                ", ccount=" + ccount +
                ", sum=" + sum +
                ", eSum=" + eSum +
                ", cSum=" + cSum +
                '}';
    }
}
