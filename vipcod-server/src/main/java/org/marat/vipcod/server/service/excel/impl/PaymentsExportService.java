package org.marat.vipcod.server.service.excel.impl;

import org.marat.vipcod.DateUtils;
import org.marat.vipcod.server.rest.model.BalanceItem;
import org.marat.vipcod.server.service.excel.JettExportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.ServletOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * генератор excel-отчета о произвольном наборе платежей
 */
@Component
public class PaymentsExportService extends JettExportService {
    private final static String TEMPLATE = "/reports/paymentsTemaplate.xlsx";

    private static final Logger LOG = LoggerFactory.getLogger(PaymentsExportService.class);

    public static PaymentsExportService instance = new PaymentsExportService();

    public void report(final List payments, OutputStream outputStream) {
        HashMap<String, Object> params = new HashMap<>();
        report(payments, outputStream, params);
    }

    public void report(final List payments, OutputStream outputStream, HashMap<String, Object> params) {
        instance.jettReport(payments, params, outputStream, TEMPLATE);
    }
}


