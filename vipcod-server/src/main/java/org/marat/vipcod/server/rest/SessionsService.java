package org.marat.vipcod.server.rest;

import org.marat.vipcod.model.entities.server.User;
import org.marat.vipcod.server.NotAuthException;
import org.marat.vipcod.server.dao.UserRepository;
import org.marat.vipcod.server.rest.model.SessionInfo;
import org.marat.vipcod.server.rest.model.SessionInfoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * Сервис для работы с данными сеанса пользователя
 */
@Component
@RequestMapping("/sessions")
public class SessionsService extends AbstractService {

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public
    @ResponseBody
    SessionInfoResponse getInfo(HttpSession session) {
        User user = getUser(session);
        return new SessionInfoResponse(new SessionInfo(user));
    }
}
