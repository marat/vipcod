package org.marat.vipcod.server.service.excel.impl;

import org.marat.vipcod.DateUtils;
import org.marat.vipcod.server.rest.model.BalanceItem;
import org.marat.vipcod.server.service.excel.JettExportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.ServletOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * генератор excel-отчета о дневных продажах
 */
@Component
public class DayPaymentsExportService extends JettExportService {
    private final static String TEMPLATE_NO_NDS = "/reports/dayReportTemplateNoNds.xlsx";
    private final static String TEMPLATE_NDS = "/reports/dayReportTemplateNds.xlsx";
    private final static String TEMPLATE_BALANCE = "/reports/balanceTemplate.xlsx";

    private static final Logger LOG = LoggerFactory.getLogger(DayPaymentsExportService.class);

    public static DayPaymentsExportService instance = new DayPaymentsExportService();

    public void report(final DayReportData reportData, Date date, OutputStream outputStream, Boolean nds) {
        HashMap<String, Object> params = new HashMap<>();

        params.put("payDate", DateUtils.dd_MM_yyyy.format(date));

        instance.jettReport(reportData, params, outputStream, (nds == null || nds) ? TEMPLATE_NDS : TEMPLATE_NO_NDS);
    }



    public void reportBalance(ArrayList<BalanceItem> balances, Date dateFrom, Date dateTo, ServletOutputStream outputStream) {
        HashMap<String, Object> params = new HashMap<>();

        params.put("dateFrom", DateUtils.dd_MM_yyyy.format(dateFrom));
        params.put("dateTo", DateUtils.dd_MM_yyyy.format(dateTo));
        instance.jettReport(balances, params, outputStream, TEMPLATE_BALANCE);
    }

}


