package org.marat.vipcod.server.dao.impl;

import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.entities.server.ClientBase;
import org.marat.vipcod.model.entities.server.Pos;
import org.marat.vipcod.server.dao.ClientBaseRepositoryCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

/**
 * Created by Marat on 17.02.2015.
 */
@Repository
public class ClientBaseRepositoryImpl implements ClientBaseRepositoryCustom {

    @PersistenceContext
    public EntityManager entityManage;

    @Override
    @Transactional
    public ClientBase savePersonInBaseTx(Person person, Pos pos) {
        ClientBase cb = (ClientBase) entityManage
                .createQuery("select cb from ClientBase cb where :pos member of cb.poses")
                .setParameter("pos", pos)
                .getSingleResult();
        cb.getPersons().add(person);
        return entityManage.merge(cb);
    }

    @Override
    @Transactional
    public ClientBase savePersonInBaseTx(Person person, long ownerId) {
        return savePersonInBase(person, ownerId);

    }

    @Override
    public ClientBase savePersonInBase(Person person, long ownerId) {
        ClientBase cb = (ClientBase) entityManage
                .createQuery("select cb from ClientBase cb where cb.ownerId = :ownerId")
                .setParameter("ownerId", ownerId)
                .getSingleResult();
        cb.getPersons().add(person);
        return entityManage.merge(cb);

    }

    @Override
    @Transactional
    public ClientBase addTerminalInClientBaseTx(Pos pos, long ownerId) {
        Query query = entityManage
                .createQuery("select cb from ClientBase cb where cb.ownerId = :ownerId")
                .setParameter("ownerId", ownerId);
        try {
            ClientBase cb = (ClientBase) query.getSingleResult();
            cb.getPoses().add(pos);
            return entityManage.merge(cb);
        } catch (NoResultException ignore) {
            throw new RuntimeException("ClentBase not found for ownerId = " + ownerId);
        }
    }
}
