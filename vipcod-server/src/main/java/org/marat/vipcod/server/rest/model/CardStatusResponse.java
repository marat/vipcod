package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.integration.api.StatusResponse;

/**
 * Created by marat on 19.11.2014.
 */
public class CardStatusResponse extends StatusResponse<Card>{
    public CardStatusResponse() {
    }

    public CardStatusResponse(String error) {
        super(error);
    }

    public CardStatusResponse(Card result) {
        super(result);
    }

    public CardStatusResponse(Card result, String error) {
        super(result, error);
    }
}
