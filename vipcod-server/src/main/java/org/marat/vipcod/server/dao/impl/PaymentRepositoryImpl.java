package org.marat.vipcod.server.dao.impl;

import org.marat.vipcod.model.entities.Payment;
import org.marat.vipcod.server.dao.PaymentRepositoryCustom;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class PaymentRepositoryImpl implements PaymentRepositoryCustom {

    @PersistenceContext
    public EntityManager entityManage;

    @Override
    public Long findTotalByParams(Long ownerId, String personUuid, Long serviceId, String coachUuid, Long period, String filter) {
        return (Long) selectFromPayment("count(p)", ownerId, personUuid, serviceId, coachUuid, period, "", filter)
                .getSingleResult();
    }

    @Override
    public List<Payment> findByParams(Long ownerId, String personUuid, Long serviceId, String coachUuid, Long period, String filter, PageRequest pageRequest) {
        Query query = selectFromPayment("p", ownerId, personUuid, serviceId, coachUuid, period, " order by p.created desc", filter);
        if (pageRequest != null) {
            query = query.setFirstResult(pageRequest.getOffset()).setMaxResults(pageRequest.getPageSize());
        }
        return query.getResultList();
    }

    private Query selectFromPayment(String selector, Long ownerId, String personUuid, Long serviceId, String coachUuid, Long period, String suffix, String filter) {
        if (ownerId == null) throw new RuntimeException("ownerId expected!");

        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("ownerId", ownerId);

        StringBuilder qlBuider = new StringBuilder("select ").append(selector)
                .append(" from Payment p where p.ownerId=:ownerId and p.person.deleted = false");

        if (personUuid != null) {
            parameterMap.put("personUuid", personUuid);
            qlBuider.append(" and p.person.uuid=:personUuid");
        }

        if (coachUuid != null) {
            parameterMap.put("coachUuid", coachUuid);
            qlBuider.append(" and p.coach.uuid=:coachUuid");
        }

        if (serviceId != null) {
            parameterMap.put("serviceId", serviceId);
            qlBuider.append(" and p.service.id=:serviceId");
        }

        if (period != null) {
            parameterMap.put("period", period);
            qlBuider.append(" and p.period=:period");
        }

        if (filter != null && (filter.startsWith("%") || filter.endsWith("%"))) {
            filter = filter.replaceAll("^\\%", "");
            filter = filter.replaceAll("\\%$", "");
        }

        if (!StringUtils.isEmpty(filter)) {
            parameterMap.put("filter", "%" + filter + "%");
            qlBuider.append(" and (upper(p.coach.name) like upper(:filter) " +
                    "or upper(p.service.name) like upper(:filter) " +
                    "or upper(p.person.secondName) like upper(:filter) " +
                    "or upper(p.person.name) like upper(:filter))");
        }

        qlBuider.append(suffix);

        Query qr = entityManage.createQuery(qlBuider.toString());
        for (String key : parameterMap.keySet()) {
            qr.setParameter(key, parameterMap.get(key));
        }

        return qr;
    }
}
