package org.marat.vipcod.server.service.excel.impl;

import org.marat.vipcod.server.service.excel.JettExportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.OutputStream;
import java.util.HashMap;

/**
 * генератор excel-отчета о произвольном наборе платежей
 */
@Component
public class ScheduleReservesExportService extends JettExportService {
    private final static String TEMPLATE = "/reports/scheduleReserves.xlsx";

    private static final Logger LOG = LoggerFactory.getLogger(ScheduleReservesExportService.class);

    public static ScheduleReservesExportService instance = new ScheduleReservesExportService();

    public void report(final ScheduleReservesExportData schedsData, OutputStream outputStream) {
        HashMap<String, Object> params = new HashMap<>();
        report(schedsData, outputStream, params);
    }

    public void report(final ScheduleReservesExportData scheds, OutputStream outputStream, HashMap<String, Object> params) {
        instance.jettReport(scheds, params, outputStream, TEMPLATE);
    }
}


