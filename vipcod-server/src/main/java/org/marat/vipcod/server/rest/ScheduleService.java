package org.marat.vipcod.server.rest;

import org.hibernate.exception.ConstraintViolationException;
import org.marat.vipcod.DateUtils;
import org.marat.vipcod.model.entities.Coach;
import org.marat.vipcod.model.entities.Payment;
import org.marat.vipcod.model.entities.Service;
import org.marat.vipcod.model.entities.Session;
import org.marat.vipcod.model.entities.schedule.ScheduleItem;
import org.marat.vipcod.model.entities.schedule.ScheduleItemGroup;
import org.marat.vipcod.model.entities.schedule.ScheduleItemRequest;
import org.marat.vipcod.model.entities.schedule.ScheduleReservation;
import org.marat.vipcod.model.entities.server.User;
import org.marat.vipcod.server.dao.*;
import org.marat.vipcod.server.rest.model.PaymentPagedResponse;
import org.marat.vipcod.server.rest.model.ScheduleStatusResponse;
import org.marat.vipcod.server.service.excel.impl.ScheduleReservesExportData;
import org.marat.vipcod.server.service.excel.impl.ScheduleReservesExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.marat.vipcod.DateUtils.getMonthLastDay;
import static org.marat.vipcod.DateUtils.periodToMonth;

/**
 * Created by marat on 22.06.2015.
 */
@Component
@RequestMapping("/schedule")
public class ScheduleService extends AbstractService {

    private static final SimpleDateFormat FULL_CALENDAR_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    CoachRepository coachRepository;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    ServiceRepository serviceRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
    SchedulePlanRepository schedulePlanRepository;

    @Autowired
    ScheduleReservationRepository reservationRepository;

    @Autowired
    ScheduleReservesExportService scheduleReservesExportService;

    @Autowired
    SessionRepository sessionRepository;

    @RequestMapping(value = "/generate", method = RequestMethod.GET)
    public void genItems(HttpSession session) throws ParseException {

        List<ScheduleItem> schedule = new ArrayList<>();
        for (int i = 0; i < 12; ++i) {
            schedule.add(createScheduleItem());
        }
        scheduleRepository.save(schedule);
    }


    /**
     * {"error": "Could not write content: For input string: `` (through reference chain: java.util.ArrayList[1]->org.marat.vipcod.model.entities.schedule.ScheduleItem[`end`]); nested exception is com.fasterxml.jackson.databind.JsonMappingException: For input string: `` (through reference chain: java.util.ArrayList[1]->org.marat.vipcod.model.entities.schedule.ScheduleItem[`end`])"}*
     */
    @RequestMapping(value = "/table", method = RequestMethod.POST)
    public
    @ResponseBody
    List<ScheduleItem> getItems(@RequestBody List<Date> selectedDates, HttpSession session) throws ParseException {
        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        if (selectedDates == null || selectedDates.isEmpty()) return new ArrayList<>();

        return scheduleRepository.findOrderedForDays(ownerId, selectedDates);
    }


    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public
    @ResponseBody
    ScheduleStatusResponse addItem(@Valid @RequestBody ScheduleItemRequest scheduleRequest, HttpSession session) throws ParseException {

        //if (true) return new ScheduleStatusResponse("test error");

        if (scheduleRequest.getDates() == null || scheduleRequest.getDates().isEmpty())
            throw new RuntimeException("даты не выбраны");

        if (StringUtils.isEmpty(scheduleRequest.getSessionBegin()) && (scheduleRequest.getSessions() == null || scheduleRequest.getSessions().isEmpty()))
            throw new RuntimeException("сеанс не выбран");

        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        ArrayList<ScheduleItem> schedule = new ArrayList<>();

        if (scheduleRequest.getSessions() != null)
            throw new RuntimeException("several sessions for schedule save request is NYR!");

        for (Date date : scheduleRequest.getDates()) {

            if (scheduleRequest.getSessionBegin() != null) {
                Session seans = findSessionForDate(date, scheduleRequest.getSessionBegin(), sessionRepository.findForEnterprise(ownerId));
                if (seans == null) {
                    throw new RuntimeException("Unexpected session " + scheduleRequest.getSessionBegin() + " for date " + date);
                }

                ScheduleItem item = new ScheduleItem(
                        date, seans, scheduleRequest.getResource(), scheduleRequest.getCoach(), scheduleRequest.getService(), scheduleRequest.getLimit()
                );
                item.setEnterpriseId(ownerId);

                if (item.getCoach() != null && (StringUtils.isEmpty(item.getCoach().getUuid()) || "0".equals(item.getCoach().getUuid()))) {
                    item.setCoach(null);
                }

                // проверка на наличие записи
                List<ScheduleItem> list = scheduleRepository.existsEqual(item.getEnterpriseId(), item.getDate(), item.getSession().getId(),
                        item.getService().getId(), item.getCoach() == null ? null : item.getCoach().getUuid());
                if (list != null && !list.isEmpty()) {
                    //return new ScheduleStatusResponse("Запись уже есть в расписании: " + item.toString());
                    ScheduleItem existsItem = list.get(0);
                    existsItem.setCoach(item.getCoach());
                    existsItem.setDate(item.getDate());
                    existsItem.setEnterpriseId(item.getEnterpriseId());
                    existsItem.setLimit(item.getLimit());
                    existsItem.setResource(item.getResource());
                    existsItem.setService(item.getService());
                    existsItem.setSession(item.getSession());
                    item = existsItem;
                }

                item = scheduleRepository.save(item);

                schedule.add(item);
            }
        }

        return new ScheduleStatusResponse(schedule);
    }

    private Session findSessionForDate(Date date, String sessionBegin, List<Session> sessions) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
        if (dayOfWeek == 0) dayOfWeek = 7;

        for (Session s : sessions) {
            if (Objects.equals(s.getBegin(), sessionBegin) && s.getDayOfWeek() == dayOfWeek) {
                return s;
            }
        }

        return null;
    }


    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public
    @ResponseBody
    ScheduleStatusResponse deleteItems(@Valid @RequestBody List<ScheduleItem> schedules, HttpSession session) throws ParseException {
        User user = getUser(session);
        //Long ownerId = user.getCompany().getId(); todo: проверка хозяина


        ArrayList<ScheduleItem> errorItems = new ArrayList<>();

        for (ScheduleItem sched : schedules) {
            try {
                scheduleRepository.delete(sched);
            } catch (Throwable e) {
                LOG.error("ошибка при удалении расписания:", e.getMessage());
                if (e instanceof ConstraintViolationException || e.getCause() instanceof ConstraintViolationException) {
                    errorItems.add(sched);
                } else {
                    throw e;
                }

            }
        }
        ScheduleStatusResponse response = errorItems.isEmpty() ? new ScheduleStatusResponse() : new ScheduleStatusResponse(errorItems, "Нельзя удалить сеансы, на которые уже записаны клиенты");
        return response;
    }


    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public
    @ResponseBody
    List<ScheduleItem> getItems(
            @RequestParam("start") String start,
            @RequestParam("end") String end,
            @RequestParam("view") String view,
            HttpSession session
    ) throws ParseException {

        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        if (view.toLowerCase().equals("month")) {
            List<ScheduleItem> schedule = scheduleRepository.findOrderedForMonth(ownerId, FULL_CALENDAR_DATE_FORMAT.parse(start), FULL_CALENDAR_DATE_FORMAT.parse(end));
            return prepareForMonth(schedule);
        }

        List<ScheduleItem> schedule = scheduleRepository.findOrderedForWeek(ownerId, FULL_CALENDAR_DATE_FORMAT.parse(start), FULL_CALENDAR_DATE_FORMAT.parse(end));

        if (view.toLowerCase().equals("agendaweek")) {
            return prepareForWeek(schedule);
        }

        // agendaDay
        return schedule;
    }

    @RequestMapping(value = "/groups/{service}/{coach}/{tariff}/{month}", method = RequestMethod.GET)
    public
    @ResponseBody
    List<ScheduleItem> getItems(
            @PathVariable("service") Long serviceId,
            @PathVariable("coach") String coachUid,
            @PathVariable("tariff") String tariffUid,
            @PathVariable("month") String month,
            HttpSession session
    ) throws ParseException {

        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        if (month.equals("0")) return Collections.emptyList();

        Date start = DateUtils.ddMMyyyy.parse("01" + month);
        Date end = DateUtils.getMonthLastDay(start);

        // захватываем следующий месяц - доп.месяц отключен после того, как решили оставить месячные периоды в платеже
        /*Calendar cal = Calendar.getInstance();
        cal.setTime(end);
        cal.add(Calendar.MONTH, 1);
        end = cal.getTime();*/

        //List<ScheduleItem> schedule = scheduleRepository.findForCalendar(ownerId, start, end);
        //List<ScheduleItem> schedule = scheduleRepository.findOrderedForMonth(ownerId, start, end);

        List<ScheduleItem> schedule = new ArrayList<>();

        if (StringUtils.isEmpty(coachUid) || "0".equals(coachUid)) {
            // без тренера
            schedule = scheduleRepository.findOrderedForMonth(ownerId, serviceId, start, end);
        } else {
            // с тренером
            schedule = scheduleRepository.findOrderedForMonth(ownerId, serviceId, coachUid, start, end);
        }

        for (ScheduleItem sch : schedule) {
            sch.setReserved(reservationRepository.getCountForSchedule(sch));
        }

        return schedule;
    }

    /**
     * отображение событий в разбивке по неделям
     *
     * @param schedule
     * @return
     */
    private List<ScheduleItem> prepareForWeek(List<ScheduleItem> schedule) {
        String lastKey = "";
        List<ScheduleItem> result = new ArrayList<>();
        ScheduleItemGroup currentGroup = new ScheduleItemGroup();

        // Порядок сортировки: день, сеанс, тренер, услуга
        for (ScheduleItem it : schedule) {
            String key = it.getDate() + "_" + it.getSession().hashCode() /*+ "_" + it.getCoach().getUuid() + "_" + it.getService().getId()*/;
            if (!key.equals(lastKey)) {
                currentGroup = new ScheduleItemGroup();
                result.add(currentGroup);
                currentGroup.setDate(it.getDate());
                currentGroup.setBegin(it.getStart());
                currentGroup.setEnd(it.getEnd());
                lastKey = key;
            }
            currentGroup.addTitle(it.getCoach().getName() + " - " + it.getService().getName() + " [" + it.getLimit() + "]");
        }

        return result;
    }

    /**
     * отображение событий в разбивке по месяцам
     *
     * @param schedule
     * @return
     */
    private List<ScheduleItem> prepareForMonth(List<ScheduleItem> schedule) {
        Map<String, Long> map = new HashMap<>();

        Date lastDay = new Date(0);
        Date day = new Date(0);
        String lastKey = "";
        List<ScheduleItem> result = new ArrayList<>();
        ScheduleItemGroup lastGroup = new ScheduleItemGroup();

        // Порядок сортировки: день, тренер, услуга
        for (ScheduleItem it : schedule) {
            day = it.getDate();
            if (!day.equals(lastDay)) {
                if (!map.isEmpty()) {
                    // выгружаем map в ответ
                    lastGroup = new ScheduleItemGroup();
                    lastGroup.setDate(lastDay);
                    result.add(lastGroup);
                    for (String title : map.keySet()) {
                        lastGroup.addTitle(title + " " + map.get(title));
                    }
                }

                map.clear();
                lastDay = day;
            }

            String key = it.getCoach().getName() + " - " + it.getService().getName();
            if (!key.equals(lastKey)) {

                if (!map.containsKey(key)) {
                    map.put(key, it.getLimit());
                } else {
                    map.put(key, map.get(key) + it.getLimit());
                }
            }
        }

        lastGroup = new ScheduleItemGroup();
        lastGroup.setDate(day);
        result.add(lastGroup);
        for (String title : map.keySet()) {
            lastGroup.addTitle(title + " " + map.get(title));
        }

        return result;
    }

    Iterable<Coach> coaches = null;
    Iterator<Coach> coachIterator = null;
    Iterable<Service> services = null;
    Iterator<Service> serviceIterator = null;

    public Iterable<Coach> getCoaches() {
        if (coaches == null) coaches = coachRepository.findAllNotDeleted();
        return coaches;
    }

    public Iterator<Coach> getCoachIterator() {
        if (coachIterator == null || !coachIterator.hasNext()) coachIterator = getCoaches().iterator();
        return coachIterator;
    }

    public Iterable<Service> getServices() {
        if (services == null) services = serviceRepository.findAllNotDeleted();
        return services;
    }

    public Iterator<Service> getServiceIterator() {
        if (serviceIterator == null || !serviceIterator.hasNext()) serviceIterator = getServices().iterator();
        return serviceIterator;
    }

    static int i = 1;
    static Map<Long, Session> sess = new HashMap();

    private ScheduleItem createScheduleItem() {
        coachIterator = getCoachIterator();
        serviceIterator = getServiceIterator();

        Calendar cal = new GregorianCalendar();
        cal.setTime(DateUtils.clearTime(new Date()));
        cal.add(Calendar.DAY_OF_YEAR, ((int) Math.random()) / 3);

        Session session = new Session(Math.floorMod(cal.get(Calendar.DAY_OF_WEEK) + 5, 7) + 1, "1" + (i + 1) + ":00", "1" + (i + 2) + ":00");
        long id = (long) (session.getDayOfWeek() * 1000 + i);
        if (!sess.containsKey(id)) {
            session = sessionRepository.save(session);
            sess.put(id, session);
        }
        session = sess.get(id);

        ScheduleItem scheduleItem = new ScheduleItem(
                cal.getTime(),
                session,
                null,
                coachIterator.next(),
                serviceIterator.next(),
                8L);
        scheduleItem.setEnterpriseId(1);
        i = Math.abs(i - 1);
        return scheduleItem;
    }

    @RequestMapping(value = "/coachSessionsBegin/{coachUuid}/{periodFrom}/{wdays}", method = RequestMethod.GET)
    public
    @ResponseBody
    Collection<String> coachSessions(
            @PathVariable("coachUuid") String coachUuid,
            @PathVariable("periodFrom") Long periodFrom,
            @PathVariable("wdays") Integer[] wdays,
            HttpSession session
    ) throws ParseException {

        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        return scheduleRepository.sessionsForCoach(ownerId, coachUuid, periodFrom, wdays)
                .stream().map(Session::getBegin).sorted().distinct().collect(Collectors.toList());
    }

/*
    @RequestMapping(value = "/getForCoach/{coachUuid}/{sessionBegin}/{period}", method = RequestMethod.GET)
    public
    @ResponseBody
    Collection<ScheduleItem> getForCoach(
            @PathVariable("coachUuid") String coachUuid,
            @PathVariable("sessionBegin") String sessionBegin,
            @PathVariable("period") Long period,
            HttpSession session
    ) throws ParseException {

        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.DAY_OF_MONTH, 1);

        return scheduleRepository.findScheduleReservedByCoach(ownerId, sessionBegin, coachUuid, period);
    }
*/

    @RequestMapping(value = "/personsForSchedule/{coachUuid}/{sessionBegin}/{wdays}/{period}", method = RequestMethod.GET)
    public
    @ResponseBody
    PaymentPagedResponse personsForSchedule(
            @PathVariable("coachUuid") String coachUuid,
            @PathVariable("sessionBegin") String sessionBegin,
            @PathVariable("period") Long period,
            @PathVariable("wdays") Integer[] wdays,
            HttpSession session
    ) throws ParseException {

        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        List<ScheduleReservation> reservationList = scheduleRepository.findReservationsForCoachAndSchedule(ownerId, coachUuid, sessionBegin, wdays, period);
        return new PaymentPagedResponse(reservationList.stream().map(ScheduleReservation::getPayment).distinct().collect(Collectors.toList()));
    }

    @RequestMapping(value = "/personsForSchedule/{coachUuid}/{sessionBegin}/{wdays}/{period}.xlsx", method = RequestMethod.GET)
    void downloadAsXlsx(HttpServletResponse response,
                        HttpSession session,
                        @PathVariable("coachUuid") String coachUuid,
                        @PathVariable("sessionBegin") String sessionBegin,
                        @PathVariable("period") Long period,
                        @PathVariable("wdays") Integer[] wdays) throws IOException {
        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        response.setContentType("application/octet-stream");

        List<ScheduleReservation> reservationList = scheduleRepository.findReservationsForCoachAndSchedule(ownerId, coachUuid, sessionBegin, wdays, period);

        Coach coach = coachRepository.findOne(coachUuid);

        scheduleReservesExportService.report(new ScheduleReservesExportData(reservationList.stream().map(ScheduleReservation::getPayment).distinct().collect(Collectors.toList()), sessionBegin, coach), response.getOutputStream());
    }


    private List<ScheduleItem> schedule(Payment payment){
        List<ScheduleItem> schedule;

        Date start = periodToMonth(payment.getPeriod());
        Date end = getMonthLastDay(start);

        if (payment.getCoach() == null) {
            // без тренера
            schedule = scheduleRepository.findOrderedForMonth(payment.getOwnerId(), payment.getService().getId(), start, end);
        } else {
            // с тренером
            schedule = scheduleRepository.findOrderedForMonth(payment.getOwnerId(), payment.getService().getId(), payment.getCoach().getUuid(), start, end);
        }
        return schedule;
    }

    @RequestMapping(value = "/scheduleForPerson/{cardId}", method = RequestMethod.GET)
    public
    @ResponseBody
    List<ScheduleItem> getSchedule(
            @PathVariable("cardId") String cardId,
            HttpSession session
    ) throws ParseException {

        List<Payment> payments = paymentRepository.findOrderedByEan(cardId);
        if (payments.isEmpty()) {
            return null;
        }
        return schedule(payments.get(0));
    }


    @RequestMapping(value = "/scheduleForPerson/{cardId}.txt", method = RequestMethod.GET)
    void getSchedule(HttpServletResponse response,
                     HttpSession session,
                     @PathVariable("cardId") String cardId) throws IOException {

        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/text; charset=UTF-8");

        PrintWriter writer = response.getWriter();

        try {
            List<Payment> payments = paymentRepository.findOrderedByEan(cardId);
            if (payments.isEmpty()) {
                //writer.format(Locale.getDefault(), "Абонемент не найден");
                writer.format(Locale.getDefault(), "-");
                return;
            }
            Payment lastPayment = payments.get(0);

/*
            writer.println("ДВОРЕЦ СПОРТА");
            writer.println("Муниципальное автономное учреждение «Комплексная спортивная школа»");
            writer.println();

            writer.println(String.format("Абонемент № %s на %d посещений", cardId, lastPayment.getVisitsCount()));
            writer.println(String.format("Период: %s", DateUtils.MMMyyyy.format(DateUtils.periodToMonth(lastPayment.getPeriod()))));
            writer.println();

            writer.println("Услуга: " + lastPayment.getService().getName());
            if (lastPayment.getTariff() != null) {
                writer.println("Тариф: " + lastPayment.getTariff().getName());
            }
            if (lastPayment.getCoach() != null) {
                writer.println("Тренер: " + lastPayment.getCoach().getName());
            }

            writer.println();
            writer.println("Расписание:");
            writer.println();
*/

            writer.println(lastPayment.getService().getName());
            if (lastPayment.getTariff() != null) {
                writer.println(" - " + lastPayment.getTariff().getName());
            }
            if (lastPayment.getCoach() != null) {
                writer.println(" - " + lastPayment.getCoach().getName());
            }

            schedule(lastPayment).stream().forEach(scheduleItem -> {
                writer.println(scheduleItem.toShortString());
            });


        } catch (Throwable t) {
            writer.println(t.getMessage());
        } finally {
            writer.flush();
            writer.close();
        }

    }

}
