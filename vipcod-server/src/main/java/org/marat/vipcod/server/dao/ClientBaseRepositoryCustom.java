package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.entities.server.ClientBase;
import org.marat.vipcod.model.entities.server.Pos;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by Marat on 17.02.2015.
 */
@Repository
public interface ClientBaseRepositoryCustom {
    ClientBase savePersonInBaseTx(Person person, Pos pos);
    ClientBase savePersonInBaseTx(Person person, long ownerId);
    ClientBase savePersonInBase(Person person, long ownerId);

    /**
     *
     * @param pos
     * @param ownerId
     * @return
     */
    ClientBase addTerminalInClientBaseTx(Pos pos, long ownerId);

}
