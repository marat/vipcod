package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.schedule.ScheduleItem;
import org.marat.vipcod.model.entities.schedule.ScheduleReservation;
import org.marat.vipcod.model.integration.api.StatusResponse;

import java.util.ArrayList;

/**
 * Created by marat on 22.06.2015.
 */
public class ScheduleResponse extends StatusResponse<ArrayList<ScheduleItem>>  {
    public ScheduleResponse() {
    }

    public ScheduleResponse(String error) {
        super(error);
    }

    public ScheduleResponse(ArrayList<ScheduleItem> result) {
        super(result);
    }

    public ScheduleResponse(ArrayList<ScheduleItem> result, String error) {
        super(result, error);
    }
}
