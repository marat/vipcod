package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.action.Abonement;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Islamov Marat on 15.12.2014.
 */
@Repository
public interface AbonementRepository extends CrudRepository<Abonement, String> {

    @Query("select aa from Abonement aa, Card card, ClientBase cb " +
            "inner join cb.persons person " +
            "where aa.card.ean = card.ean and card.person.uuid = person.uuid and :pos member of cb.poses and (aa.created > :prevVersion or aa.updated > :prevVersion)")
    //@Query("select aa from Abonement aa where aa.pos = :pos and (aa.created > :prevVersion or aa.updated > :prevVersion)")
    List<Abonement> findByVersions(@Param("pos") String pos, @Param("prevVersion") Long prevVersion);


    //todo: теоретически разные посы могу зарегать карты с одинаковым еаном, затем объединиться в одну клиентскую базу (если будет такой функционал позволен). Тогда ответ тут может быть не один.
    @Query("select aa from Abonement aa, Card card, ClientBase cb " +
            "inner join cb.persons person " +
            "where aa.card.ean = :ean and card.ean = :ean and card.person.uuid = person.uuid and :pos member of cb.poses")
    Abonement findByPosAndEan(@Param("pos") String pos, @Param("ean") String ean);

    @Query("select coalesce(max(p.updated), 0) from Abonement p")
    long currentVersion();

    //@Query("select a from Abonement a where a.card = :card")
    Abonement findOneByCard(/*@Param("card")*/ Card card);

    /*@Query("select aa from Abonement aa where aa.ean = :ean")
    Abonement findByPosAndEan(@Param("ean") String ean);*/
}
