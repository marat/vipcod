package org.marat.vipcod.server.rest.sync;

import org.marat.vipcod.model.EntitiesUtil;
import org.marat.vipcod.model.entities.AbstractEntity;
import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.Service;
import org.marat.vipcod.model.entities.Tariff;
import org.marat.vipcod.model.entities.action.ActionHistory;
import org.marat.vipcod.model.entities.pos.PosUser;
import org.marat.vipcod.model.entities.server.Pos;
import org.marat.vipcod.model.integration.synch.*;
import org.marat.vipcod.server.dao.*;
import org.marat.vipcod.server.sso.SsoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static org.apache.commons.lang3.ObjectUtils.max;
import static org.apache.commons.lang3.ObjectUtils.min;

/**
 * Created by Islamov Marat on 15.12.2014.
 */
@Component
@RequestMapping("/sync/request")
public class RequestRestService {
    private static final Logger LOG = LoggerFactory.getLogger(RequestRestService.class);

    @Autowired
    PosRepository posRepository;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    PosUserRepository posUserRepository;

    @Autowired
    HistoryRepository historyRepository;

    @Autowired
    AbonementRepository abonementRepository;

    @Autowired
    CardRepository cardRepository;

    @Autowired
    TariffRepository tariffRepository;

    @Autowired
    ServiceRepository serviceRepository;


    @RequestMapping(value = "/history", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public VersionedResponse requestHistory(
            @RequestParam("uid") String uid,
            @RequestParam(value = "from", required = false) Long prevVersion,
            @RequestParam(value = "to", required = false) Long untilVersion) {

        Pos pos = getByPosId(uid);
        if (prevVersion == null) prevVersion = 0L;
        if (untilVersion == null || untilVersion == 0L) untilVersion = Long.MAX_VALUE;
        if (prevVersion == 0) prevVersion = -1L; // если 0 то ищем включительно 0

        List<ActionHistory> histories = null;

        // вытаскиваем всех клиентов из клиентских баз данного терминала и загружаем изменения их карт
        // todo: оптимизировтаь. Клиентов будет слишком много, а изменений истории их карт может не быть вообще. все в один запрос
        /*List<Person> persons = posRepository.getPosClients(pos);
        if (persons != null && !persons.isEmpty()) {
            histories = historyRepository.findByVersions(persons, prevVersion, untilVersion);
        }*/

        histories = historyRepository.findByVersions(pos, prevVersion, untilVersion);

        return prepareResponse(new HistoryResponse(), histories);
    }


    @RequestMapping(value = "/activities", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public VersionedResponse requestActivities(
            @RequestParam("uid") String uid,
            @RequestParam(value = "from", required = false) Long prevVersion) {

        Pos pos = getByPosId(uid);
        if (prevVersion == null) prevVersion = 0L;
        if (prevVersion == 0) prevVersion = -1L; // если 0 то ищем включительно 0
        return prepareResponse(new ActivesResponse(), abonementRepository.findByVersions(pos.getUuid(), prevVersion));
    }

    /**
     * Обработка запросов к серверу на получение обновленных данных по клиентам
     *
     * @param uid
     * @param prevVersion
     * @param untilVersion
     * @return
     */
    @RequestMapping(value = "/persons", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public VersionedResponse requestPersons(
            @RequestParam("uid") String uid,
            @RequestParam(value = "from", required = false) Long prevVersion,
            @RequestParam(value = "to", required = false) Long untilVersion) {

        Pos pos = getByPosId(uid);
        if (prevVersion == null) prevVersion = 0L;
        if (untilVersion == null || untilVersion == 0L) untilVersion = Long.MAX_VALUE;
        if (prevVersion == 0) prevVersion = -1L; // если 0 то ищем включительно 0
        return prepareResponse(new PersonsResponse(), personRepository.findByVersions(pos, prevVersion, untilVersion));
    }

    @RequestMapping(value = "/cards", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public VersionedResponse<Card> requestCards(
            @RequestParam("uid") String uid,
            @RequestParam(value = "from", required = false) Long prevVersion,
            @RequestParam(value = "to", required = false) Long untilVersion) {

        Pos pos = getByPosId(uid);
        if (prevVersion == null) prevVersion = 0L;
        if (untilVersion == null || untilVersion == 0L) untilVersion = Long.MAX_VALUE;
        if (prevVersion == 0) prevVersion = -1L; // если 0 то ищем включительно 0
        try {
            VersionedResponse<Card> response = prepareResponse(new CardsResponse(), cardRepository.findByVersions(pos, prevVersion, untilVersion));
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @RequestMapping(value = "/posUsers", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public VersionedResponse requestPosUsers(
            @RequestParam("uid") String uid,
            @RequestParam(value = "from", required = false) Long prevVersion) {

        Pos pos = getByPosId(uid);

        if (prevVersion == null || prevVersion < pos.getUpdated()) {
            List<PosUser> posPosUsers = posUserRepository.findByPosId(pos.getUuid());
            return new PosUserResponse(0L, EntitiesUtil.getNextGlobalVersion(), posPosUsers);
        }

        List<PosUser> posUserChanges = posUserRepository.findByPosIdAfterVersion(pos.getUuid(), prevVersion);
        if (posUserChanges != null && !posUserChanges.isEmpty()) {
            return new PosUserResponse(prevVersion, EntitiesUtil.getNextGlobalVersion(), posUserChanges);
        }

        return new PosUserResponse(0L, EntitiesUtil.getNextGlobalVersion(), null);
    }


    @RequestMapping(value = "/tariffs", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public VersionedResponse requestTariffs(
            @RequestParam("uid") String uid,
            @RequestParam(value = "from", required = false) Long prevVersion,
            @RequestParam(value = "to", required = false) Long untilVersion) {

        Pos pos = getByPosId(uid);
        if (prevVersion == null) prevVersion = 0L;
        if (untilVersion == null || untilVersion == 0L) untilVersion = Long.MAX_VALUE;
        if (prevVersion == 0) prevVersion = -1L; // если 0 то ищем включительно 0

        List<Tariff> models = tariffRepository.findByVersions(pos.getOwner().getId(), prevVersion, untilVersion);
        return prepareResponse(new TariffsResponse(), models);
    }


    @RequestMapping(value = "/services", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public VersionedResponse requestServices(
            @RequestParam("uid") String uid,
            @RequestParam(value = "from", required = false) Long prevVersion,
            @RequestParam(value = "to", required = false) Long untilVersion) {

        Pos pos = getByPosId(uid);
        if (prevVersion == null) prevVersion = 0L;
        if (untilVersion == null || untilVersion == 0L) untilVersion = Long.MAX_VALUE;
        if (prevVersion == 0) prevVersion = -1L; // если 0 то ищем включительно 0

        List<Service> services = serviceRepository.findByVersions(pos.getOwner().getId(), prevVersion, untilVersion);
        return prepareResponse(new ServicesResponse(), services);
    }


    protected Pos getByPosId(String uid) {
        String posId = SsoService.instance.getPosBySessionId(uid);
        Pos pos = posRepository.findByPosId(posId);
        if (pos == null) throw new SynchUploadException("bad uid for sync");
        return pos;
    }

    private <T extends AbstractEntity> VersionedResponse<T>
    prepareResponse(VersionedResponse<T> response, List<T> items) {
        if (items == null || items.isEmpty()) return response;

        Long from = Long.MAX_VALUE;
        Long to = 0L;
        for (T item : items) {
            from = min(from, item.getUpdated());
            to = max(from, item.getCreated(), item.getUpdated());
        }
        return new VersionedResponse<>(from, to, items);
    }



}
