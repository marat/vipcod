package org.marat.vipcod.server.rest;

import org.marat.vipcod.model.entities.ModelUtils;
import org.marat.vipcod.model.entities.Payment;
import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.entities.schedule.ScheduleItem;
import org.marat.vipcod.model.entities.schedule.ScheduleReservation;
import org.marat.vipcod.model.entities.server.Pos;
import org.marat.vipcod.model.entities.server.User;
import org.marat.vipcod.model.integration.api.impl.PaymentPagedRequest;
import org.marat.vipcod.model.integration.synch.SyncUploadResult;
import org.marat.vipcod.model.integration.synch.SynchUploadException;
import org.marat.vipcod.server.dao.PaymentRepository;
import org.marat.vipcod.server.dao.PersonRepository;
import org.marat.vipcod.server.dao.PosRepository;
import org.marat.vipcod.server.dao.ScheduleReservationRepository;
import org.marat.vipcod.server.rest.model.PaymentPagedResponse;
import org.marat.vipcod.server.rest.model.PaymentSaveRequest;
import org.marat.vipcod.server.rest.model.PaymentStatusResponse;
import org.marat.vipcod.server.sso.SsoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

//import javax.ws.rs.QueryParam;

/**
 * Created by Администратор on 28.10.2014.
 */
@Component
@RequestMapping("/payment")
public class PaymentRestService extends AbstractService {

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    ScheduleReservationRepository scheduleReservationRepository;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    PosRepository posRepository;


    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public
    @ResponseBody
    PaymentPagedResponse getPayments(
            @RequestBody PaymentPagedRequest request,
            HttpSession session
    ) {
        logger.info("getPayments: " + request);

        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        if (request.getPersonUuid() != null && request.getCoachUuid() != null)
            throw new RuntimeException("Only resonUid OR coachUid expected. Not both!");

        PaymentPagedResponse response;

        response = new PaymentPagedResponse(
                paymentRepository.findByParams(
                        ownerId,
                        request.getPersonUuid(),
                        request.getServiceId(),
                        request.getCoachUuid(),
                        request.getPeriod(),
                        request.getFilter(),
                        new PageRequest(request.getPageNum(), request.getPageSize())),

                paymentRepository.findTotalByParams(
                        ownerId,
                        request.getPersonUuid(),
                        request.getServiceId(),
                        request.getCoachUuid(),
                        request.getPeriod(),
                        request.getFilter()
                )
        );

        if (!response.getItems().isEmpty()) {
            response.setPerson(response.getItems().get(0).getPerson());
        } else {
            response.setPerson(request.getPersonUuid() == null ? null : personRepository.findOne(request.getPersonUuid()));
        }

        return response;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public
    @ResponseBody
    @Transactional
    PaymentStatusResponse savePayments(@Valid @RequestBody PaymentSaveRequest paymentRequest, HttpSession session, BindingResult bindingResult) {

        logger.info("save: {}", paymentRequest);

        Payment payment = paymentRequest.getPayment();
        if (payment.getCoach() != null && (ModelUtils.isJavascriptNullValue(payment.getCoach().getUuid()) || "0".equals(payment.getCoach().getUuid()))) {
            payment.setCoach(null);
        }

        if (bindingResult.hasErrors()) {
            return new PaymentStatusResponse(bindingResult.toString());
        }

        Payment originPayment = null;

        if (payment.getUuid() != null) {
            originPayment = paymentRepository.findOne(payment.getUuid());
        }
        if (originPayment == null) {
            originPayment = new Payment(payment.getUuid());
            User user = getUser(session);
            originPayment.setOwnerId(user.getCompany().getId());
        }
        payment.setOwnerId(originPayment.getOwnerId());
        payment.setCreated(originPayment.getCreated());

        // todo: удаление некорректно! Поскольку эти резервирования могли быть компенсированы
        // и будут пересозданы уже как не компенсированные


        List<ScheduleReservation> oldReservations = payment.getReservations();
        payment.setReservations(new ArrayList<>());
        //payment = paymentRepository.save(payment);

        Person person = payment.getPerson();
        for (ScheduleItem shi : paymentRequest.getSelectedTimes()) {
            ScheduleReservation reservation = extractReservationFromCollectionByScheduleId(oldReservations, shi.getId());
            if (reservation == null) {
                reservation = new ScheduleReservation();
                reservation.setPerson(person);
                reservation.setPayment(payment);
                reservation.setSchedule(shi);
                reservation = scheduleReservationRepository.save(reservation);
            }
            payment.getReservations().add(reservation);
        }

        if (oldReservations.size() > 0) {
            scheduleReservationRepository.delete(oldReservations);
        }

        // обработка компенсаций
        for (ScheduleReservation reservation : payment.getCompensatedReservations()) {
            //todo: проверять, что допустимо
            Long reservationId = reservation.getId();
            reservation = scheduleReservationRepository.findOne(reservationId);
            if (reservation == null)
                throw new RuntimeException("не удалось найти резервацию с id = " + reservationId + " для компансации");
            reservation.setPaymentCompensator(payment);
        }

        payment = paymentRepository.save(payment);

        return new PaymentStatusResponse(payment);
    }

    private ScheduleReservation extractReservationFromCollectionByScheduleId(List<ScheduleReservation> oldReservations, Long schId) {
        for (int idx = 0; idx < oldReservations.size(); ++idx) {
            ScheduleReservation reservation = oldReservations.get(idx);
            if (Objects.equals(reservation.getSchedule().getId(), schId)) {
                oldReservations.remove(idx);
                return reservation;
            }
        }
        return null;
    }

    @RequestMapping(value = "/upload/{suid}", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    SyncUploadResult uploadPayments(
            @PathVariable("suid") String uid,
            @Valid @RequestBody List<Payment> payment,
            HttpSession session, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new SynchUploadException(bindingResult.toString());
        }

        String posId = SsoService.instance.getPosBySessionId(uid);
        Pos pos = posRepository.findByPosId(posId);

        if (pos == null) throw new SynchUploadException("sync session not found by id = " + uid);

        for (Payment pay : payment) {
            if (pay.getUuid() != null) {
                Payment entity = paymentRepository.findOne(pay.getUuid());
                if (entity == null) throw new SynchUploadException("Payment not found by uuid = " + pay.getUuid());
                if (!Objects.equals(entity.getOwnerId(), pos.getOwner().getId()))
                    throw new SynchUploadException("Payment not found for owner by persn.uuid = " + pay.getUuid());
            }
        }
        paymentRepository.save(payment);

        return new SyncUploadResult();
    }


    @RequestMapping(value = "/last/{uuid}/{period}", method = RequestMethod.GET)
    public
    @ResponseBody
    List<Payment> lastPaymentUntilPeriod(@PathVariable("uuid") String uuid, @PathVariable("period") Long period, HttpSession session) {
        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        if (period == 0) {
            return paymentRepository.findByOwnerIdAndPersonId(ownerId, uuid, new PageRequest(0, 10));
        }
        return paymentRepository.findByOwnerIdAndPersonIdAndPeriod(ownerId, uuid, period - 3, period);
    }

    @RequestMapping(value = "/last/{uuid}", method = RequestMethod.GET)
    public
    @ResponseBody
    Payment lastPayment(@PathVariable("uuid") String uuid, HttpSession session) {
        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        List<Payment> payments = paymentRepository.findByOwnerIdAndPersonId(ownerId, uuid, new PageRequest(0, 1));
        Payment payment = payments.isEmpty() ? null : payments.get(0);
        if (payment != null && payment.getTariff() != null && payment.getTariff().isDeleted()) {
            Payment newPayment = new Payment(payment);
            newPayment.setTariff(null);
            payment = newPayment;
        }
        return payment;
    }

    /**
     * служебный сервис для обновления статуса ндс у платежей
     *
     * @return
     */
    @RequestMapping(value = "/updateNds", method = RequestMethod.GET)
    public
    @ResponseBody
    Long updateNds() {
        long i = 0;
        long j = 0;
        Iterator<Payment> iter = paymentRepository.findAll().iterator();
        while (iter.hasNext()) {
            Payment p = iter.next();
            Boolean nds = p.getNds();
            p.updateNds();
            if (nds != p.getNds()) {
                paymentRepository.save(p);
                j++;
            }
            ++i;
        }
        logger.info("updated {} / {}", j, i);
        return i;
    }
}
