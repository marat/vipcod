package org.marat.vipcod.server.rest;

import org.marat.vipcod.model.entities.Service;
import org.marat.vipcod.server.dao.ServiceRepository;
import org.marat.vipcod.server.rest.model.ServiceStatusResponse;
import org.marat.vipcod.server.rest.model.TariffPagedResponse;
import org.marat.vipcod.server.rest.model.TariffStatusResponse;
import org.marat.vipcod.model.entities.Tariff;
import org.marat.vipcod.server.NotAuthException;
import org.marat.vipcod.server.dao.TariffRepository;
import org.marat.vipcod.server.dao.PersonRepository;
import org.marat.vipcod.server.dao.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.marat.vipcod.model.entities.server.User;
import org.marat.vipcod.model.integration.api.PagedRequest;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Objects;

//import javax.ws.rs.QueryParam;

/**
 * Created by Администратор on 28.10.2014.
 */
@Component
@RequestMapping("/tariff")
public class TariffRestService extends AbstractService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    TariffRepository tariffRepository;

    @Autowired
    ServiceRepository serviceRepository;

    @Autowired
    PersonRepository personRepository;

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public
    @ResponseBody
    TariffPagedResponse getTariffs(
            @RequestBody PagedRequest request,
            HttpSession session
    ) {
        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        List<Tariff> tariffList = null;
        long totalByOwnerId = 0;
        if (StringUtils.isEmpty(request.getFilter())) {
            tariffList = tariffRepository.findByOwnerId(ownerId, new PageRequest(request.getPageNum(), request.getPageSize()));
            totalByOwnerId = tariffRepository.findTotalByOwnerId(ownerId);
        } else {
            String filter = "%" + request.getFilter() + "%";
            tariffList = tariffRepository.findByOwnerId(ownerId, filter, new PageRequest(request.getPageNum(), request.getPageSize()));
            totalByOwnerId = tariffRepository.findTotalByOwnerId(ownerId, filter);
        }

        return new TariffPagedResponse(
                tariffList,
                totalByOwnerId
        );
    }

    @RequestMapping(value = "/{serviceId}/save", method = RequestMethod.POST)
    public
    @ResponseBody
    TariffStatusResponse saveTariff(@RequestBody Tariff tariff, @PathVariable("serviceId") Long serviceId, HttpSession session) {
        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        Tariff originTariff = null;

        if (tariff.getUuid() != null) {
            originTariff = tariffRepository.findOne(tariff.getUuid());
        }

        if (tariff.getOwnerId() == 0) {
            tariff.setOwnerId(ownerId);
        }

        if (originTariff == null) {
            originTariff = new Tariff();
            originTariff.setUuid(tariff.getUuid());
            originTariff.setOwnerId(ownerId);
        }

        // проверить хозяина tariff
        if (originTariff.getOwnerId() != tariff.getOwnerId()) {
            return new TariffStatusResponse("У вас нет прав на изменение этого тарифа");
        }

        /*if (tariff.getService() == null) {
            tariff.setService(originTariff.getService());
        } else {
            if (originTariff.getService() != null && !Objects.equals(tariff.getService().getId(), originTariff.getService().getId()))
                return new TariffStatusResponse("перемещение тарифа из одного сервиса в другой не реализовано");
        }*/

        // todo: присвоить параметры, которые не сериализуемы. Либо скопировать значения
        // сериализуемых параметров в оригинал из пришедшего объекта
        tariff.setOwnerId(originTariff.getOwnerId());

        tariff = tariffRepository.save(tariff);

        Service service = (serviceId == null || serviceId == 0) ? null : serviceRepository.findOne(serviceId);
        if (service == null) {
            // return new TariffStatusResponse("service " + serviceId + " not found");
        } else {
            service.getTariffs().add(tariff);
            serviceRepository.save(service);
        }
        return new TariffStatusResponse(tariff);
    }

    @RequestMapping(value = "/del", method = RequestMethod.GET)
    public void delItem(@RequestParam String uuid, HttpSession session) {
        // todo: присвоить параметры, которые не сериализуемы. Либо скопировать значения
        // сериализуемых параметров в оригинал из пришедшего объекта
        User user = getUser(session);
        Long ownerId = user.getCompany().getId();

        Tariff tariffForDel = null;

        if (uuid != null) {
            tariffForDel = tariffRepository.findOne(uuid);
        }
        if (tariffForDel == null) {
            throw new RuntimeException("Тариф с идентификатором " + uuid + " не найден");
        }

        if (tariffForDel.getOwnerId() != ownerId) {
            throw new RuntimeException("Вы не являетесь создателем данного тарифа");
        }

        // валидация неудаляемых тарифов

        tariffForDel.setDeleted(true);
        tariffRepository.save(tariffForDel);
    }
}
