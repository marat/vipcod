package org.marat.vipcod.server.service.excel.impl;

import java.io.Serializable;
import java.util.List;

/**
 * Created by marat on 14.03.2016.
 */
public class DayReport implements Serializable {
    private List payments;
    private List paymentCorrects;

    public DayReport() {
    }

    public DayReport(List payments, List paymentCorrects) {
        this.payments = payments;
        this.paymentCorrects = paymentCorrects;
    }

    public List getPayments() {
        return payments;
    }

    public void setPayments(List payments) {
        this.payments = payments;
    }

    public List getPaymentCorrects() {
        return paymentCorrects;
    }

    public void setPaymentCorrects(List paymentCorrects) {
        this.paymentCorrects = paymentCorrects;
    }
}
