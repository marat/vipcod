package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.Coach;
import org.marat.vipcod.model.integration.api.PagedResponse;

import java.util.List;

/**
 * Created by marat on 18.06.2015.
 */
public class CoachPagedResponse extends PagedResponse<Coach> {
    public CoachPagedResponse() {
    }


    public CoachPagedResponse(List<Coach> coachList, long total) {
        setItems(coachList);
        setTotal(total);
    }
}
