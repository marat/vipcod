package org.marat.vipcod.server.rest.controller;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created by Marat on 19.03.2015.
 */
public class RegPosModel {
    @NotNull
    @NotBlank
    String userName;

    @NotNull
    @NotBlank
    String password;

    @NotNull
    @NotBlank
    String description;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "RegPosModel{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
