package org.marat.vipcod.server.config;

import com.github.valdr.ValidationRulesServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.filter.CharacterEncodingFilter;

import java.util.TimeZone;


@Configuration
@EntityScan(basePackages = "org.marat.vipcod.model.entities")
@EnableJpaRepositories(basePackages = {"org.marat.vipcod.server"})
@EnableTransactionManagement
@ComponentScan(basePackages = {"org.marat.vipcod.server", "org.marat.vipcod.server.rest"})
@EnableAutoConfiguration
public class Application extends SpringBootServletInitializer {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    // Used when deploying to a standalone servlet container
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        logger.info("SET TIMEZONE TO UTC");
        TimeZone.setDefault(TimeZone.getTimeZone("Etc/UTC"));

        return application.sources(Application.class);
    }

    @Bean
    public FilterRegistrationBean filterEncodingBean() {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);

        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(characterEncodingFilter);
        registrationBean.setOrder(Integer.MIN_VALUE);
        registrationBean.addUrlPatterns("/*");
        return registrationBean;
    }

    @Bean
    public ServletRegistrationBean servletRegistrationBean(){
        return new ServletRegistrationBean(new ValidationRulesServlet(),"/validationRules");
    }


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
