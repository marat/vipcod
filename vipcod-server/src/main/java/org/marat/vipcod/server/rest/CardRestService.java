package org.marat.vipcod.server.rest;

import org.marat.vipcod.server.dao.CardRepository;
import org.marat.vipcod.server.dao.ClientBaseRepository;
import org.marat.vipcod.server.dao.PersonRepository;
import org.marat.vipcod.server.dao.UserRepository;
import org.marat.vipcod.server.rest.model.CardPagedResponse;
import org.marat.vipcod.server.rest.model.CardStatusResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.entities.server.User;
import org.marat.vipcod.model.integration.api.PagedRequest;

import javax.servlet.http.HttpSession;
import java.util.List;

//import javax.ws.rs.QueryParam;

/**
 * Created by Администратор on 28.10.2014.
 */
@Component
@RequestMapping("/card")
public class CardRestService extends AbstractService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CardRepository cardRepository;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    ClientBaseRepository clientBaseRepository;

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public
    @ResponseBody
    CardPagedResponse getCards(
            @RequestBody PagedRequest request,
            HttpSession session
    ) {

        // todo: использовать id сессии
        User user = getUser(session);
        Long companyId = user.getCompany().getId();

        List<Card> cardList = null;
        long totalByOwnerId = 0;
        if (StringUtils.isEmpty(request.getFilter())) {
            cardList = cardRepository.findByOwnerId(companyId, new PageRequest(request.getPageNum(), request.getPageSize()));
            totalByOwnerId = cardRepository.findTotalByOwnerId(companyId);
        } else {
            String filter = "%" + request.getFilter() + "%";
            cardList = cardRepository.findByOwnerId(companyId, filter, new PageRequest(request.getPageNum(), request.getPageSize()));
            totalByOwnerId = cardRepository.findTotalByOwnerId(companyId, filter);
        }

        return new CardPagedResponse(
                cardList,
                totalByOwnerId
        );
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public
    @ResponseBody
    CardStatusResponse saveCard(@RequestBody Card card, HttpSession session) {
        // todo: присвоить параметры, которые не сериализуемы. Либо скопировать значения
        // сериализуемых параметров в оригинал из пришедшего объекта

        // todo: проверить хозяина card

        Card originCard = null;

        originCard = cardRepository.findOne(card.getEan());

        if (originCard == null) {
            originCard = new Card(card.getEan());
            User user = getUser(session);

            originCard.setOwnerId(user.getCompany().getId());
        }
        card.setOwnerId(originCard.getOwnerId());
        card.setPerson(originCard.getPerson());

        if (!StringUtils.isEmpty(card.getTransientPersonFio())) {
            Person own = new Person();
            own.setOwnerId(card.getOwnerId());
            own.setName(card.getTransientPersonFio());
            own = personRepository.save(own);
            clientBaseRepository.savePersonInBaseTx(own, card.getOwnerId()); // сохраняем нового клиента в к.базе владельца
            card.setPerson(own);
        }


        //todo: ПРОДУМАТЬ ЛОГИКУ КЛИЕНТИСКИХ БАЗ, СВЯЗЕЙ С ПЕРСОНАМИ КАРТАМИ И ПОСТЕРМИНАЛАМИ

        card = cardRepository.save(card);
        return new CardStatusResponse(card);
    }
}
