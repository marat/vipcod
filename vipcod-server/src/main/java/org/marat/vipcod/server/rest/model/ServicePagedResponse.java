package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.Service;
import org.marat.vipcod.model.integration.api.PagedResponse;

import java.util.List;

/**
 * Created by marat on 19.11.2014.
 */
public class ServicePagedResponse extends PagedResponse<Service>{

    public ServicePagedResponse() {
    }

    public ServicePagedResponse(List<Service> items, long total) {
        setItems(items);
        setTotal(total);
    }

}
