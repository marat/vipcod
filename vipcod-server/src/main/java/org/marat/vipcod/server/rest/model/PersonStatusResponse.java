package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.integration.api.StatusResponse;

/**
 * Created by marat on 18.11.2014.
 */
public class PersonStatusResponse extends StatusResponse<Person> {
    public PersonStatusResponse(Person person) {
        setResult(person);
    }

    public PersonStatusResponse(Person result, String error) {
        super(result, error);
    }

    public PersonStatusResponse() {
    }

    public PersonStatusResponse(String error) {
        super(error);
    }
}
