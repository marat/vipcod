package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.Session;
import org.marat.vipcod.model.entities.server.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface SessionRepository extends CrudRepository<Session, Long> {

    @Query("select s from Session s where :orgId=:orgId order by s.dayOfWeek, s.begin")
        // todo: ?????? ???????? ? orgId
    List<Session> findForEnterprise(@Param("orgId") Long orgId);

//    @Query("select s from Session s where :orgId=:orgId AND s.name=:name AND s.dayOfWeek=:dayOfWeek")
//        // todo: ?????? ???????? ? orgId
//    Session findByNameAndWeekAndOrgId(@Param("name") String name, @Param("dayOfWeek") int dayOfWeek, @Param("orgId") Long orgId);
}
