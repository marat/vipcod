package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.Tariff;
import org.marat.vipcod.model.integration.api.StatusResponse;

/**
 * Created by marat on 19.11.2014.
 */
public class TariffStatusResponse extends StatusResponse<Tariff>{
    public TariffStatusResponse() {
    }

    public TariffStatusResponse(String error) {
        super(error);
    }

    public TariffStatusResponse(Tariff result) {
        super(result);
    }

    public TariffStatusResponse(Tariff result, String error) {
        super(result, error);
    }
}
