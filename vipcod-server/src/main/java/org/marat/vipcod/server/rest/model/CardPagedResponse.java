package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.integration.api.PagedResponse;

import java.util.List;

/**
 * Created by marat on 19.11.2014.
 */
public class CardPagedResponse extends PagedResponse<Card>{

    public CardPagedResponse() {
    }

    public CardPagedResponse(List<Card> cards, long total) {
        setItems(cards);
        setTotal(total);
    }
}
