package org.marat.vipcod.server.rest;

import org.marat.vipcod.MessageUtils;
import org.marat.vipcod.model.entities.server.User;
import org.marat.vipcod.server.NotAuthException;
import org.marat.vipcod.server.dao.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpSession;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.UnsupportedEncodingException;

/**
 * Created by marat on 05.03.2015.
 */
public abstract class AbstractService {
    Logger LOG = LoggerFactory.getLogger(AbstractService.class);

    @Autowired
    UserRepository userRepository;

    public static boolean TEST_MODE = true;

    Logger logger = LoggerFactory.getLogger(AbstractService.class);

    private static ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
    public static Validator validator = vf.getValidator();


    @ExceptionHandler(NotAuthException.class)
    public ResponseEntity handleIOException(NotAuthException ex) {
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    protected User getUser(HttpSession session){
        User user = (User) session.getAttribute("user");
        if (user == null) throw new NotAuthException();
        user = userRepository.findOne(user.getId());
        if (user == null) throw new RuntimeException("User not found");
        return user;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(Exception ex) {
        MessageUtils.printExceptionTrace(ex);
        String exceptionMessage = MessageUtils.getExceptionMessage(ex).replaceAll("\"", "`").replaceAll("\n", " ");
        try {
            String encodedExceptionMessage = new String(exceptionMessage.getBytes("ISO-8859-1"), "UTF-8");
            if (!encodedExceptionMessage.contains("??")){
                exceptionMessage = encodedExceptionMessage;
            }
        } catch (UnsupportedEncodingException e) {
            logger.error("error", e);
        }

        logger.error("", ex);

        logger.error("\n######################\nERROR MESSAGE FOR CLIENT: {}\n######################", exceptionMessage);

        return new ResponseEntity<>(
                String.format("{\"error\": \"%s\"}", exceptionMessage),
                HttpStatus.FORBIDDEN
        );
    }
}
