package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.Tariff;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TariffRepository extends CrudRepository<Tariff, String> {

    @Query("select p from Tariff p where p.ownerId=:ownerId")
    List<Tariff> findByOwnerId(@Param("ownerId") long ownerId, Pageable p);

    @Query("select p from Tariff p where p.ownerId=:ownerId and upper(p.name) like upper(:filter)")
    List<Tariff> findByOwnerId(@Param("ownerId") long ownerId, @Param("filter") String filer, Pageable p);

    @Query("select count(p) from Tariff p where p.ownerId=:ownerId and (upper(p.name) like upper(:filter) )")
    long findTotalByOwnerId(@Param("ownerId") Long ownerId, @Param("filter") String filer);

    @Query("select count(p) from Tariff p where p.ownerId=:ownerId")
    long findTotalByOwnerId(@Param("ownerId") Long ownerId);

    @Query("select aa from Tariff aa where aa.ownerId = :ownerId and (aa.created > :prevVersion or aa.updated > :prevVersion) and (aa.created < :untilVersion or aa.updated < :untilVersion)")
    List<Tariff> findByVersions(@Param("ownerId") Long ownerId, @Param("prevVersion") Long prevVersion, @Param("untilVersion") Long untilVersion);

    @Query("select coalesce(max(p.updated), 0) from Tariff p")
    Long getCurrentVersion();


}
