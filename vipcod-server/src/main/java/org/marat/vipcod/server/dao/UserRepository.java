package org.marat.vipcod.server.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.marat.vipcod.model.entities.server.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User findByEmail(String email);

    User findByNameOrEmail(String name, String email);

    User findByEmailAndPassword(String email, String password);
}
