package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.pos.PosUser;
import org.marat.vipcod.model.integration.api.StatusResponse;

/**
 * Created by marat on 19.11.2014.
 */
public class PosUserStatusResponse extends StatusResponse<PosUser>{
    public PosUserStatusResponse() {
    }

    public PosUserStatusResponse(String error) {
        super(error);
    }

    public PosUserStatusResponse(PosUser result) {
        super(result);
    }

    public PosUserStatusResponse(PosUser result, String error) {
        super(result, error);
    }
}
