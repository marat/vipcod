package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.Service;
import org.marat.vipcod.model.integration.api.StatusResponse;

/**
 * Created by marat on 19.11.2014.
 */
public class ServiceStatusResponse extends StatusResponse<Service> {
    public ServiceStatusResponse() {
    }

    public ServiceStatusResponse(String error) {
        super(error);
    }

    public ServiceStatusResponse(Service result) {
        super(result);
    }

    public ServiceStatusResponse(Service result, String error) {
        super(result, error);
    }
}
