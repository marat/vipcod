package org.marat.vipcod.server.rest.sync;

import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.Tariff;
import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.entities.action.Abonement;
import org.marat.vipcod.model.entities.action.ActionHistory;
import org.marat.vipcod.model.entities.server.ClientBase;
import org.marat.vipcod.model.entities.server.Pos;
import org.marat.vipcod.model.integration.synch.SyncUploadResult;
import org.marat.vipcod.model.integration.synch.SynchUploadException;
import org.marat.vipcod.server.dao.*;
import org.marat.vipcod.server.service.ActionActiveCalculatorService;
import org.marat.vipcod.server.sso.SsoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

import static org.apache.commons.lang3.ObjectUtils.max;

/**
 * Created by Islamov Marat on 15.12.2014.
 */
@Component
@RequestMapping("/sync/upload")
public class UploadRestService {
    private static final Logger LOG = LoggerFactory.getLogger(UploadRestService.class);

    @Autowired
    PosRepository posRepository;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    CardRepository cardRepository;

    @Autowired
    ClientBaseRepository clientBaseRepository;

    @Autowired
    AbonementRepository abonementRepository;

    @Autowired
    TariffRepository tariffRepository;

    @Autowired
    HistoryRepository historyRepository;

    @Autowired
    ActionActiveCalculatorService actionActiveCalculatorService;

    @PersistenceContext
    EntityManager entityManager;

    @RequestMapping(value = "/persons/{suid}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public SyncUploadResult uploadPerson(
            @PathVariable("suid") String suid,
            @Valid @RequestBody List<Person> items,
            HttpSession session, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return new SyncUploadResult(bindingResult.toString());
        }

        Pos pos = getPosBySessionId(suid);

        long version = personRepository.getCurrentVersion();
        for (Person item : items) {
            if (item.getUuid() != null) {
                Person entity = personRepository.findOne(item.getUuid());
                if (entity != null && entity.getOwnerId() != pos.getOwner().getId())
                    return new SyncUploadResult("item not found for owner. uuid = " + item.getUuid());
            }
            //todo: прочая валидация допустимых изменений
            item.setOwnerId(pos.getOwner().getId());
            item = personRepository.save(item);
            version = max(version, item.getUpdated()); //TODO: версии у всех записей тут надо сделать так, чтоб совпадали. То есть присваивать принудительно
        }

        return new SyncUploadResult(version);

    }


    /**
     * выгружаем новые записи: Card
     *
     * @param uid
     * @param items
     * @param session
     * @param bindingResult
     * @return
     */
    @RequestMapping(value = "/newCards/{suid}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    @Transactional
    public SyncUploadResult uploadNewCards(
            @PathVariable("suid") String uid,
            @Valid @RequestBody List<Card> items,
            HttpSession session, BindingResult bindingResult) {

        LOG.info("uploadNewCards...");
        if (bindingResult.hasErrors()) {
            LOG.error("bindingResult has Errors !");
            return new SyncUploadResult(bindingResult.toString());
        }

        Pos pos = getPosBySessionId(uid);

        long version = cardRepository.currentVersion();
        for (Card item : items) {
            /*if (cardRepository.exists(item.getEan())) {
                LOG.error("entity " + item.getClass().getName() + " exists in DB: id = " + item.getEan());
                return new SyncUploadResult("entity " + item.getClass().getName() + " exists in DB: id = " + item.getEan());
            }*/
            Card existsCard = cardRepository.findByEan(item.getEan());
            if (existsCard != null){
                // карта есть - привязываем ее к базе терминала (игнорируем разницу между старыми данными и новыми - берем за основу старые)
                // todo: для данного терминала/сети терминаов сохранять свою версию данных пользователя/карты
                ClientBase cb = clientBaseRepository.savePersonInBaseTx(existsCard.getPerson(), pos);
                version = max(version, cb.getCreated());
                continue;
            }

            item.setOwnerId(pos.getOwner().getId());

            //todo: когда посюзеры будут создаваться только на серевре - раскомментировать
            item.setManager(null /*entityManager.getReference(PosUser.class, item.getManager().getId())*/);
            if (item.getPerson() != null) {
                // перед выгрузкой новых карт, должны были быть выгружены все новые персоны. С учетом этого, считаем, что suid в базе есть
                String personUid = item.getPerson().getUuid();
                // todo: проверить, что suid не подставлен вручную злоумышленником
                if (personUid == null) throw new RuntimeException("suid of person is null!");
                //item.setPerson(entityManager.getReference(Person.class, personUid));
                item.setPerson(personRepository.findOne(personUid));
            } else {
                // todo: некорректно?
            }

            try {
                item = cardRepository.save(item);
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }

            // при создании карты (начале обслуживания персоны) обавляем персона в клиентскую базу терминала
            pos.getClientBase().getPersons().add(item.getPerson());
            //todo: когда будут партнерские программы - добавлять персона и  дргуие клиентские базы, в зависимости от пренадлежности постерминала этим базам

            version = max(version, item.getCreated());
        }
        return new SyncUploadResult(version);
    }


    /**
     * выгружаем новые записи: Limits
     *
     * @param uid
     * @param items
     * @param session
     * @param bindingResult
     * @return
     */
    @RequestMapping(value = "/newLimits/{suid}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    @Transactional
    public SyncUploadResult uploadNewLimits(
            @PathVariable("suid") String uid,
            @Valid @RequestBody List<Abonement> items,
            HttpSession session, BindingResult bindingResult) {

        LOG.info("uploadNewLimits...");
        if (bindingResult.hasErrors()) {
            LOG.error("bindingResult has Errors !");
            return new SyncUploadResult(bindingResult.toString());
        }

        long version = abonementRepository.currentVersion();
        for (Abonement item : items) {
            if (abonementRepository.exists(item.getUuid())) {
                LOG.error("entity " + item.getClass().getName() + " exists in DB: id = " + item.getUuid());
                return new SyncUploadResult("entity " + item.getClass().getName() + " exists in DB: id = " + item.getUuid());
            }

            item.setCount(0L); // мы не сохраняем текущее состояние посещений при выгрузках.
            // Мы его вычисляем по истории. В Abonement у нас хранятся как ограничения, так и
            // состояние абонемента. Тут мы должны проигнорировать состояние из терминала, так как
            // расчитаем его позже - при выгрузке истории


            //Игнорим Abonement у новых карт. Так как он вычисляется на сервере, а не берется из терминала

            //todo: когда посюзеры будут создаваться только на серевре - раскомментировать
            item.setManager(null /*entityManager.getReference(PosUser.class, item.getManager().getId())*/);

            //todo:когда модели будут только на сервере - раскомментировать item.setTariff(entityManager.getReference(CardTariff.class, item.getTariff().getId()));
            item.setTariff(null); //???

            //item.setCard(cardRepository.findByEan(item.getCard().getEan()));

            try {
                entityManager.persist(item);
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }


            version = max(version, item.getCreated());
        }
        return new SyncUploadResult(version);
    }


    @RequestMapping(value = "/updCards/{suid}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public SyncUploadResult uploadUpdCards(
            @PathVariable("suid") String uid,
            @Valid @RequestBody List<Card> items,
            HttpSession session, BindingResult bindingResult) {

        LOG.info("uploadUpdCards: " + items.size());

        if (bindingResult.hasErrors()) {
            return new SyncUploadResult(bindingResult.toString());
        }

        try {
            Pos pos = getPosBySessionId(uid);

            long version = cardRepository.currentVersion();
            for (Card item : items) {
                Card persistedCard = cardRepository.findByEan(item.getEan());
                if (persistedCard == null) {
                    return new SyncUploadResult("entity " + item.getClass().getName() + " NOT exists in DB: id = " + item.getEan());
                }
                item.setOwnerId(pos.getOwner().getId());

                //todo: обновлять или нет запись, если она уже есть в бд и была изменена после предыдущей синхронизации - сделать настраиваемой политику
                item = cardRepository.save(item);
                version = max(version, item.getUpdated());
            }
            return new SyncUploadResult(version);
        } catch (Throwable t) {
            t.printStackTrace();
            return new SyncUploadResult(t.getMessage());
        }
    }


    @RequestMapping(value = "/updLimits/{suid}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public SyncUploadResult uploadUpdLimits(
            @PathVariable("suid") String uid,
            @Valid @RequestBody List<Abonement> items,
            HttpSession session, BindingResult bindingResult) {

        LOG.info("uploadUpdLimits: " + items.size());

        if (bindingResult.hasErrors()) {
            return new SyncUploadResult(bindingResult.toString());
        }

        try {
            Pos pos = getPosBySessionId(uid);

            long version = cardRepository.currentVersion();
            for (Abonement item : items) {
                Abonement persistedAbonement = abonementRepository.findOne(item.getUuid());
                if (persistedAbonement == null) {
                    return new SyncUploadResult("entity " + item.getClass().getName() + " NOT exists in DB: id = " + item.getUuid());
                }

                item.setCount(persistedAbonement.getCount()); // мы игнорируем информацию о состоянии
                // карты на терминале. Позже вычислим его в облаке по выгруженной со всех терминалов истории

                //todo: обновлять или нет запись, если она уже есть в бд и была изменена после предыдущей синхронизации - сделать настраиваемой политику
                item = abonementRepository.save(item);
                version = max(version, item.getUpdated());
            }
            return new SyncUploadResult(version);
        } catch (Throwable t) {
            t.printStackTrace();
            return new SyncUploadResult(t.getMessage());
        }
    }


    @RequestMapping(value = "/history/{suid}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public SyncUploadResult uploadHistory(
            @PathVariable("suid") String uid,
            @Valid @RequestBody List<ActionHistory> items,
            HttpSession session, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return new SyncUploadResult(bindingResult.toString());
        }

        try {
            Pos pos = getPosBySessionId(uid);

            long version = 0;
            for (ActionHistory item : items) {
                assert item.getUuid() != null;

                ActionHistory persisted = historyRepository.findByUuid(item.getUuid());
                if (persisted != null) {
                    LOG.warn("update exists history! suid = " + item.getUuid()); //todo: политика обновления
                }
                //todo: проверить разрешение на добавление истории

                // вычисление Abonement при появлении нового действия
                Abonement aa = actionActiveCalculatorService.calculate(pos, item);

                if (version != 0) { // для всей порции данных используем одну и ту же версию
                    item.setCreated(version);
                    item.setUpdated(version);
                }
                item = historyRepository.save(item);
                version = max(version, item.getUpdated());

                aa = abonementRepository.save(aa); // сохраняем состояние карты (active)
                System.out.println(aa);
            }
            return new SyncUploadResult(version);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
            return new SyncUploadResult(e.getMessage());
        }
    }

    @RequestMapping(value = "/tariffs/{suid}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public SyncUploadResult uploadTariffs(
            @PathVariable("suid") String uid,
            @Valid @RequestBody List<Tariff> items,
            HttpSession session, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return new SyncUploadResult(bindingResult.toString());
        }

        Pos pos = getPosBySessionId(uid);

        long version = tariffRepository.getCurrentVersion();
        for (Tariff item : items) {
            if (item.getUuid() != null) {
                Tariff persisted = tariffRepository.findOne(item.getUuid());
                if (persisted != null && persisted.getOwnerId() != pos.getOwner().getId())
                    return new SyncUploadResult("item not found for owner. id = " + item.getUuid());
            }

            item.setOwnerId(pos.getOwner().getId());

            item = tariffRepository.save(item);
            version = max(version, item.getUpdated());

        }
        return new SyncUploadResult(version);
    }


    private Pos getPosBySessionId(String sessionUid) {
        String posId = SsoService.instance.getPosBySessionId(sessionUid);
        Pos pos = posRepository.findByPosId(posId);
        if (pos == null) throw new SynchUploadException("bad sessionUid for sync");
        return pos;
    }

}
