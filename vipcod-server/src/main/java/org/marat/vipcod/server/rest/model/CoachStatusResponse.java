package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.Coach;
import org.marat.vipcod.model.integration.api.StatusResponse;

/**
 * Created by marat on 18.06.2015.
 */
public class CoachStatusResponse extends StatusResponse<Coach> {
    public CoachStatusResponse() {
    }

    public CoachStatusResponse(String error) {
        super(error);
    }

    public CoachStatusResponse(Coach result) {
        super(result);
    }

    public CoachStatusResponse(Coach result, String error) {
        super(result, error);
    }
}
