package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.server.ClientBase;
import org.marat.vipcod.model.entities.server.Pos;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by marat on 25.12.2014.
 */
@Repository
public interface ClientBaseRepository extends CrudRepository<ClientBase, Long>, ClientBaseRepositoryCustom {
    @Query("select cb from ClientBase cb where :pos member of cb.poses")
    ClientBase findByPos(@Param("pos") Pos pos);
}
