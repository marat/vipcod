package org.marat.vipcod.server.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.marat.vipcod.model.entities.pos.PosUser;

import java.util.List;

@Repository
public interface PosUserRepository extends CrudRepository<PosUser, String> {
    @Query("select u.users from Pos u where u.uuid = :posId")
    List<PosUser> findByPosId(@Param("posId") String posId);


    //todo: упростить запрос

    /**
     * ищет какие-либо изменения с пользователями терминала.
     * @param posId
     * @param timeFrom
     * @return если результат пустой - нет изменений. Иначе ест, но могу быть не все
     */
    @Query("select u from PosUser u where " +
            "EXISTS (select p from Pos p where p.uuid = :posId and u MEMBER OF p.users) " +
            " AND (u.updated >= :timeFrom)")
    List<PosUser> findByPosIdAfterVersion(@Param("posId") String posId, @Param("timeFrom") Long timeFrom);

    @Query("select pu from PosUser pu where pu.ownerId = :ownerId and pu.deleted <> true")
    List<PosUser> findByOwnerId(@Param("ownerId") long ownerId, Pageable p);

    @Query("select count(pu) from PosUser pu where pu.ownerId = :ownerId and pu.deleted <> true")
    long findTotalByOwnerId(@Param("ownerId") Long ownerId);

    @Query("select pu from PosUser pu where pu.ownerId = :ownerId and (coalesce(upper(pu.name), '') like upper(:filter)) and pu.deleted <> true")
    List<PosUser> findByOwnerId(@Param("ownerId") long ownerId, @Param("filter") String filer, Pageable p);

    @Query("select count(pu) from PosUser pu where pu.ownerId = :ownerId and (coalesce(upper(pu.name),'') like upper(:filter)) and pu.deleted <> true")
    long findTotalByOwnerId(@Param("ownerId") Long ownerId, @Param("filter") String filer);

}
