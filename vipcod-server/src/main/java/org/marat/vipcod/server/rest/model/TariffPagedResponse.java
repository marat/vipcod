package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.Tariff;
import org.marat.vipcod.model.integration.api.PagedResponse;

import java.util.List;

/**
 * Created by marat on 19.11.2014.
 */
public class TariffPagedResponse extends PagedResponse<Tariff>{

    public TariffPagedResponse() {
    }

    public TariffPagedResponse(List<Tariff> items, long total) {
        setItems(items);
        setTotal(total);
    }
}
