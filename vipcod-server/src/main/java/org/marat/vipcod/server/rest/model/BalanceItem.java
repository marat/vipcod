package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.entities.Service;

import java.io.Serializable;

/**
 * Created by marat on 21.12.2015.
 */
public class BalanceItem implements Serializable {

    private Person person;

    private Service service;

    //private Double sumBegin = 0.0;
    //private Double sumEnd = 0.0;

    private Double sumPeriod = 0.0;

    public BalanceItem() {
    }

    public BalanceItem(Person person, Service service) {
        this.person = person;
        this.service = service;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Double getSumPeriod() {
        return sumPeriod;
    }

    public void setSumPeriod(Double sumPeriod) {
        this.sumPeriod = sumPeriod;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    /*public Double getSumBegin() {
        return sumBegin;
    }

    public void setSumBegin(Double sumBegin) {
        this.sumBegin = sumBegin;
    }

    public Double getSumEnd() {
        return sumEnd;
    }

    public void setSumEnd(Double sumEnd) {
        this.sumEnd = sumEnd;
    }*/
}
