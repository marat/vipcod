package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.pos.PosUser;
import org.marat.vipcod.model.integration.api.PagedResponse;

import java.util.List;

/**
 * Created by marat on 19.11.2014.
 */
public class PosUsersPagedResponse extends PagedResponse<PosUser> {
    public PosUsersPagedResponse() {
    }

    public PosUsersPagedResponse(List<PosUser> items, long total) {
        setTotal(total);
        setItems(items);
    }
}
