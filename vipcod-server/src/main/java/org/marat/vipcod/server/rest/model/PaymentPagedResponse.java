package org.marat.vipcod.server.rest.model;

import org.marat.vipcod.model.entities.Payment;
import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.integration.api.PagedResponse;

import java.util.List;

/**
 * Created by marat on 26.06.2015.
 */
public class PaymentPagedResponse extends PagedResponse<Payment> {
    private Person person;

    public PaymentPagedResponse() {
    }

    public PaymentPagedResponse(List<Payment> payments, long total) {
        setItems(payments);
        setTotal(total);
    }

    public PaymentPagedResponse(List<Payment> payments) {
        setItems(payments);
        setTotal(payments.size());
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}