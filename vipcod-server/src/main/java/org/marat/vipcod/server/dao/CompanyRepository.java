package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.server.Company;
import org.marat.vipcod.model.entities.server.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {
}
