package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.Person;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.marat.vipcod.model.entities.server.Pos;

import java.util.List;

@Repository
public interface PosRepository extends CrudRepository<Pos, Long> {
    @Query("select p from Pos p where p.uuid = ?1")
    Pos findByPosId(String posId);

    @Query("select p from Pos p where p.owner.id = :ownerId and (upper(p.name) like upper(:filter) or upper(p.description) like upper(:filter))")
    List<Pos> findByOwnerId(@Param("ownerId") long ownerId, @Param("filter") String filer, Pageable p);

    @Query("select count(p) from Pos p where p.owner.id = :ownerId and (upper(p.name) like upper(:filter) or upper(p.description) like upper(:filter))")
    long findTotalByOwnerId(@Param("ownerId") Long ownerId, @Param("filter") String filer);

    @Query("select cb.persons from ClientBase cb where :pos member of cb.poses")
    List<Person> getPosClients(@Param("pos") Pos pos);

}
