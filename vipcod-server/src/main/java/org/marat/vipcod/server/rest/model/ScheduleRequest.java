package org.marat.vipcod.server.rest.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by marat on 22.06.2015.
 */
public class ScheduleRequest implements Serializable {
    private Date start;
    private Date end;
    private String view;

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }
}
