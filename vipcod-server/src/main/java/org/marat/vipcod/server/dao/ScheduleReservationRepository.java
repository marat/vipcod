package org.marat.vipcod.server.dao;

import org.marat.vipcod.model.entities.Payment;
import org.marat.vipcod.model.entities.schedule.ScheduleReservation;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by marat on 18.06.2015.
 */
@Repository
public interface ScheduleReservationRepository extends CrudRepository<ScheduleReservation, Long>, ScheduleReservationRepositoryCustom {


}
