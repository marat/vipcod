package org.marat.vipcod.server.service;

import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.action.ActionHistory;
import org.marat.vipcod.server.dao.CardRepository;
import org.marat.vipcod.server.dao.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CardService {
    @Autowired
    CardRepository cardRepository;

    @Autowired
    HistoryRepository historyRepository;

    public void saveHistory(ActionHistory hist) {
        Card card = cardRepository.findByEan(hist.getEan());
        hist.setCard(card);
        historyRepository.save(hist);
    }
}
