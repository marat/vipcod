INSERT INTO company_entity values (2, 0, 0, 'Дворец спорта', 'Дворец спорта');
INSERT INTO user_entity (id, company_id, email, name, password, phone, roles) VALUES (2, 2, 'kassa@ds.ru', 'кассир', '123', '00-00-00', 'K');
INSERT INTO user_entity (id, company_id, email, name, password, phone, roles) VALUES (3, 2, 'manager@ds.ru', 'менеджер', '123', '00-00-00', 'M');
INSERT INTO user_entity (id, company_id, email, name, password, phone, roles) VALUES (4, 2, 'admin@ds.ru', 'системный администратор', '123', '00-00-00', 'S');
INSERT INTO user_entity (id, company_id, email, name, password, phone, roles) VALUES (5, 2, 'dev@ds.ru', 'разработчик', '123', '00-00-00', 'KMS');
INSERT INTO client_base (id, created, name, owner_id) VALUES (1, 0, 'клиенты Дворца Спорта', 2);
