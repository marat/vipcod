--
-- PostgreSQL database dump
--

CREATE TABLE abonemets (
  uuid        CHARACTER VARYING(255) NOT NULL,
  created     BIGINT                 NOT NULL,
  updated     BIGINT,
  count       BIGINT,
  end_time    TIMESTAMP WITHOUT TIME ZONE,
  max_count   BIGINT,
  record_time TIMESTAMP WITHOUT TIME ZONE,
  start_time  TIMESTAMP WITHOUT TIME ZONE,
  ean         CHARACTER VARYING(255),
  user_uuid   CHARACTER VARYING(255),
  tariff_uuid CHARACTER VARYING(255)
);

--
-- TOC entry 172 (class 1259 OID 158926)
-- Name: cards; Type: TABLE; Schema: public; Owner: vipcod; Tablespace:
--

CREATE TABLE cards (
  ean         CHARACTER VARYING(255) NOT NULL,
  created     BIGINT                 NOT NULL,
  updated     BIGINT,
  bonus       DOUBLE PRECISION,
  comment     CHARACTER VARYING(255),
  owner_id    BIGINT,
  user_uuid   CHARACTER VARYING(255),
  client_uuid CHARACTER VARYING(255) NOT NULL
);


--
-- TOC entry 173 (class 1259 OID 158934)
-- Name: client_base; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE client_base (
  id       BIGINT                 NOT NULL,
  created  BIGINT                 NOT NULL,
  updated  BIGINT,
  name     CHARACTER VARYING(255) NOT NULL,
  owner_id BIGINT                 NOT NULL
);


--
-- TOC entry 174 (class 1259 OID 158939)
-- Name: client_base_persons; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE client_base_persons (
  client_base_id BIGINT                 NOT NULL,
  persons_uuid   CHARACTER VARYING(255) NOT NULL
);


--
-- TOC entry 175 (class 1259 OID 158944)
-- Name: client_base_poses; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE client_base_poses (
  client_base_id BIGINT                 NOT NULL,
  poses_uuid     CHARACTER VARYING(255) NOT NULL
);


--
-- TOC entry 176 (class 1259 OID 158947)
-- Name: coach; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE coach (
  uuid     CHARACTER VARYING(255) NOT NULL,
  created  BIGINT                 NOT NULL,
  updated  BIGINT,
  name     CHARACTER VARYING(255),
  owner_id BIGINT
);


--
-- TOC entry 177 (class 1259 OID 158955)
-- Name: coach_services; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE coach_services (
  coach_uuid  CHARACTER VARYING(255) NOT NULL,
  services_id BIGINT                 NOT NULL
);


--
-- TOC entry 178 (class 1259 OID 158958)
-- Name: company_entity; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE company_entity (
  id          BIGINT NOT NULL,
  created     BIGINT NOT NULL,
  updated     BIGINT,
  description CHARACTER VARYING(255),
  name        CHARACTER VARYING(255)
);


--
-- TOC entry 179 (class 1259 OID 158966)
-- Name: company_entity_client_bases; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE company_entity_client_bases (
  company_entity_id BIGINT NOT NULL,
  client_bases_id   BIGINT NOT NULL
);


--
-- TOC entry 180 (class 1259 OID 158969)
-- Name: company_entity_pos; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE company_entity_pos (
  company_entity_id BIGINT                 NOT NULL,
  pos_uuid          CHARACTER VARYING(255) NOT NULL
);


--
-- TOC entry 181 (class 1259 OID 158972)
-- Name: history; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE history (
  uuid         CHARACTER VARYING(255) NOT NULL,
  created      BIGINT                 NOT NULL,
  updated      BIGINT,
  bonus        DOUBLE PRECISION,
  count        BIGINT,
  countmax     BIGINT,
  datebegin    TIMESTAMP WITHOUT TIME ZONE,
  dateend      TIMESTAMP WITHOUT TIME ZONE,
  ean          CHARACTER VARYING(255) NOT NULL,
  newstatus    CHARACTER VARYING(255),
  payment      DOUBLE PRECISION,
  pos_user_ean CHARACTER VARYING(255),
  tariff       CHARACTER VARYING(255),
  "time"       TIMESTAMP WITHOUT TIME ZONE
);


--
-- TOC entry 182 (class 1259 OID 158980)
-- Name: payment; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE payment (
  uuid           CHARACTER VARYING(255)      NOT NULL,
  created        BIGINT                      NOT NULL,
  updated        BIGINT,
  ean            CHARACTER VARYING(255),
  nds            BOOLEAN,
  owner_id       BIGINT,
  pay_time       TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  sum            DOUBLE PRECISION            NOT NULL,
  abonement_uuid CHARACTER VARYING(255),
  coach_uuid     CHARACTER VARYING(255),
  person_uuid    CHARACTER VARYING(255)      NOT NULL,
  service_id     BIGINT
);


--
-- TOC entry 183 (class 1259 OID 158988)
-- Name: person_entity; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE person_entity (
  uuid        CHARACTER VARYING(255) NOT NULL,
  created     BIGINT                 NOT NULL,
  updated     BIGINT,
  birth       TIMESTAMP WITHOUT TIME ZONE,
  middle_name CHARACTER VARYING(255),
  name        CHARACTER VARYING(255) NOT NULL,
  owner_id    BIGINT,
  phone       CHARACTER VARYING(255),
  photo       OID,
  second_name CHARACTER VARYING(255) NOT NULL,
  current_ean CHARACTER VARYING(255)
);


--
-- TOC entry 184 (class 1259 OID 158996)
-- Name: pos_config_entity; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE pos_config_entity (
  pos              CHARACTER VARYING(255) NOT NULL,
  export_path      CHARACTER VARYING(255),
  fast_time_border BIGINT                 NOT NULL,
  web_cam_delay    BIGINT                 NOT NULL,
  web_cam_selected INTEGER                NOT NULL
);


--
-- TOC entry 185 (class 1259 OID 159004)
-- Name: pos_entity; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE pos_entity (
  uuid           CHARACTER VARYING(255) NOT NULL,
  created        BIGINT                 NOT NULL,
  updated        BIGINT,
  description    CHARACTER VARYING(255) NOT NULL,
  name           CHARACTER VARYING(255) NOT NULL,
  client_base_id BIGINT,
  owner_id       BIGINT
);


--
-- TOC entry 186 (class 1259 OID 159012)
-- Name: pos_entity_users; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE pos_entity_users (
  pos_entity_uuid CHARACTER VARYING(255) NOT NULL,
  users_uuid      CHARACTER VARYING(255) NOT NULL
);


--
-- TOC entry 187 (class 1259 OID 159018)
-- Name: pos_user_entity; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE pos_user_entity (
  uuid       CHARACTER VARYING(255) NOT NULL,
  created    BIGINT                 NOT NULL,
  updated    BIGINT,
  deleted    BOOLEAN,
  ean        CHARACTER VARYING(255),
  is_manager BOOLEAN,
  name       CHARACTER VARYING(255) NOT NULL,
  owner_id   BIGINT                 NOT NULL
);


--
-- TOC entry 188 (class 1259 OID 159026)
-- Name: reservations; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE reservations (
  id          BIGINT NOT NULL,
  created     BIGINT NOT NULL,
  updated     BIGINT,
  person_uuid CHARACTER VARYING(255),
  schedule_id BIGINT
);


--
-- TOC entry 189 (class 1259 OID 159031)
-- Name: resources; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE resources (
  id            BIGINT NOT NULL,
  created       BIGINT NOT NULL,
  updated       BIGINT,
  name          CHARACTER VARYING(255),
  persons_limit BIGINT
);


--
-- TOC entry 190 (class 1259 OID 159036)
-- Name: schedule; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE schedule (
  id               BIGINT NOT NULL,
  created          BIGINT NOT NULL,
  updated          BIGINT,
  date             TIMESTAMP WITHOUT TIME ZONE,
  enterprise_id    BIGINT,
  limit_attendance BIGINT,
  coach            CHARACTER VARYING(255),
  resource         BIGINT,
  service          BIGINT,
  session          BIGINT
);


--
-- TOC entry 191 (class 1259 OID 159041)
-- Name: service; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE service (
  id       BIGINT                 NOT NULL,
  created  BIGINT                 NOT NULL,
  updated  BIGINT,
  deleted  BOOLEAN,
  name     CHARACTER VARYING(255) NOT NULL,
  owner_id BIGINT                 NOT NULL
);


--
-- TOC entry 192 (class 1259 OID 159046)
-- Name: sessions; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE sessions (
  id          BIGINT NOT NULL,
  created     BIGINT NOT NULL,
  updated     BIGINT,
  start_time  CHARACTER VARYING(255),
  day_of_week INTEGER,
  end_time    CHARACTER VARYING(255)
);


--
-- TOC entry 193 (class 1259 OID 159054)
-- Name: tariff; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE tariff (
  uuid          CHARACTER VARYING(255) NOT NULL,
  created       BIGINT                 NOT NULL,
  updated       BIGINT,
  count_portion BIGINT                 NOT NULL,
  days_portion  BIGINT                 NOT NULL,
  deleted       BOOLEAN,
  name          CHARACTER VARYING(255) NOT NULL,
  owner_id      BIGINT,
  price         DOUBLE PRECISION       NOT NULL,
  CONSTRAINT tariff_count_portion_check CHECK ((count_portion >= 1)),
  CONSTRAINT tariff_days_portion_check CHECK ((days_portion >= 1)),
  CONSTRAINT tariff_price_check CHECK ((price >= (0) :: DOUBLE PRECISION))
);


--
-- TOC entry 194 (class 1259 OID 159065)
-- Name: user_entity; Type: TABLE; Schema: public; Owner: vipcod; Tablespace: 
--

CREATE TABLE user_entity (
  id         BIGINT NOT NULL,
  email      CHARACTER VARYING(255),
  name       CHARACTER VARYING(255),
  password   CHARACTER VARYING(255),
  phone      CHARACTER VARYING(255),
  roles      CHARACTER VARYING(255),
  company_id BIGINT NOT NULL
);

CREATE TABLE SERVICE_TARIFF (
  service_id  BIGINT NOT NULL,
  tariff_uuid CHARACTER VARYING(255)
);

--
-- TOC entry 1913 (class 2606 OID 158925)
-- Name: abonemets_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY abonemets
ADD CONSTRAINT abonemets_pkey PRIMARY KEY (uuid);


--
-- TOC entry 1915 (class 2606 OID 158933)
-- Name: cards_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY cards
ADD CONSTRAINT cards_pkey PRIMARY KEY (ean);


--
-- TOC entry 1919 (class 2606 OID 158943)
-- Name: client_base_persons_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY client_base_persons
ADD CONSTRAINT client_base_persons_pkey PRIMARY KEY (client_base_id, persons_uuid);


--
-- TOC entry 1917 (class 2606 OID 158938)
-- Name: client_base_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY client_base
ADD CONSTRAINT client_base_pkey PRIMARY KEY (id);


--
-- TOC entry 1923 (class 2606 OID 158954)
-- Name: coach_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY coach
ADD CONSTRAINT coach_pkey PRIMARY KEY (uuid);


--
-- TOC entry 1925 (class 2606 OID 158965)
-- Name: company_entity_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY company_entity
ADD CONSTRAINT company_entity_pkey PRIMARY KEY (id);


--
-- TOC entry 1927 (class 2606 OID 158979)
-- Name: history_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY history
ADD CONSTRAINT history_pkey PRIMARY KEY (uuid);


--
-- TOC entry 1929 (class 2606 OID 158987)
-- Name: payment_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY payment
ADD CONSTRAINT payment_pkey PRIMARY KEY (uuid);


--
-- TOC entry 1931 (class 2606 OID 158995)
-- Name: person_entity_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY person_entity
ADD CONSTRAINT person_entity_pkey PRIMARY KEY (uuid);


--
-- TOC entry 1933 (class 2606 OID 159003)
-- Name: pos_config_entity_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY pos_config_entity
ADD CONSTRAINT pos_config_entity_pkey PRIMARY KEY (pos);


--
-- TOC entry 1935 (class 2606 OID 159011)
-- Name: pos_entity_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY pos_entity
ADD CONSTRAINT pos_entity_pkey PRIMARY KEY (uuid);


--
-- TOC entry 1937 (class 2606 OID 159025)
-- Name: pos_user_entity_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY pos_user_entity
ADD CONSTRAINT pos_user_entity_pkey PRIMARY KEY (uuid);


--
-- TOC entry 1939 (class 2606 OID 159030)
-- Name: reservations_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY reservations
ADD CONSTRAINT reservations_pkey PRIMARY KEY (id);


--
-- TOC entry 1941 (class 2606 OID 159035)
-- Name: resources_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY resources
ADD CONSTRAINT resources_pkey PRIMARY KEY (id);


--
-- TOC entry 1943 (class 2606 OID 159040)
-- Name: schedule_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY schedule
ADD CONSTRAINT schedule_pkey PRIMARY KEY (id);


--
-- TOC entry 1945 (class 2606 OID 159045)
-- Name: service_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY service
ADD CONSTRAINT service_pkey PRIMARY KEY (id);


--
-- TOC entry 1947 (class 2606 OID 159053)
-- Name: sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY sessions
ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);


--
-- TOC entry 1949 (class 2606 OID 159064)
-- Name: tariff_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY tariff
ADD CONSTRAINT tariff_pkey PRIMARY KEY (uuid);


--
-- TOC entry 1921 (class 2606 OID 159074)
-- Name: uk_lndb0cl0pp2bc2qd2sf31wyvv; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY client_base_poses
ADD CONSTRAINT uk_lndb0cl0pp2bc2qd2sf31wyvv UNIQUE (poses_uuid);


--
-- TOC entry 1951 (class 2606 OID 159072)
-- Name: user_entity_pkey; Type: CONSTRAINT; Schema: public; Owner: vipcod; Tablespace: 
--

ALTER TABLE ONLY user_entity
ADD CONSTRAINT user_entity_pkey PRIMARY KEY (id);


--
-- TOC entry 1955 (class 2606 OID 159090)
-- Name: fk_1auyn4vs2ywo7j26vv3x8dj48; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY cards
ADD CONSTRAINT fk_1auyn4vs2ywo7j26vv3x8dj48 FOREIGN KEY (user_uuid) REFERENCES pos_user_entity (uuid);


--
-- TOC entry 1976 (class 2606 OID 159195)
-- Name: fk_1itw3qu7qqdm65nnp0dcetj1f; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY pos_entity_users
ADD CONSTRAINT fk_1itw3qu7qqdm65nnp0dcetj1f FOREIGN KEY (pos_entity_uuid) REFERENCES pos_entity (uuid);


--
-- TOC entry 1978 (class 2606 OID 159205)
-- Name: fk_2c1au44m0nqakegstukx1lw5k; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY reservations
ADD CONSTRAINT fk_2c1au44m0nqakegstukx1lw5k FOREIGN KEY (schedule_id) REFERENCES schedule (id);


--
-- TOC entry 1962 (class 2606 OID 159125)
-- Name: fk_432kfc5pd1f8wwmigibuhmu08; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY coach_services
ADD CONSTRAINT fk_432kfc5pd1f8wwmigibuhmu08 FOREIGN KEY (coach_uuid) REFERENCES coach (uuid);


--
-- TOC entry 1970 (class 2606 OID 159165)
-- Name: fk_4dhlcynshgcibkltpx4yu67ao; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY payment
ADD CONSTRAINT fk_4dhlcynshgcibkltpx4yu67ao FOREIGN KEY (person_uuid) REFERENCES person_entity (uuid);


--
-- TOC entry 1965 (class 2606 OID 159140)
-- Name: fk_4ow1433tp2o3otx80q5wyp1wr; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY company_entity_pos
ADD CONSTRAINT fk_4ow1433tp2o3otx80q5wyp1wr FOREIGN KEY (pos_uuid) REFERENCES pos_entity (uuid);


--
-- TOC entry 1977 (class 2606 OID 159200)
-- Name: fk_4yg7oi5i04thip4h4112wms7p; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY reservations
ADD CONSTRAINT fk_4yg7oi5i04thip4h4112wms7p FOREIGN KEY (person_uuid) REFERENCES person_entity (uuid);


--
-- TOC entry 1960 (class 2606 OID 159115)
-- Name: fk_60uw0g80vsdc45av8focnur5e; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY client_base_poses
ADD CONSTRAINT fk_60uw0g80vsdc45av8focnur5e FOREIGN KEY (client_base_id) REFERENCES client_base (id);


--
-- TOC entry 1961 (class 2606 OID 159120)
-- Name: fk_6jfle1t08ri2f34gy9p5kurxj; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY coach_services
ADD CONSTRAINT fk_6jfle1t08ri2f34gy9p5kurxj FOREIGN KEY (services_id) REFERENCES service (id);


--
-- TOC entry 1979 (class 2606 OID 159210)
-- Name: fk_6jlli5qurwojcaes56mocch5m; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY schedule
ADD CONSTRAINT fk_6jlli5qurwojcaes56mocch5m FOREIGN KEY (coach) REFERENCES coach (uuid);


--
-- TOC entry 1967 (class 2606 OID 159150)
-- Name: fk_838ioq1sgdv1vffm5q28t0sj0; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY history
ADD CONSTRAINT fk_838ioq1sgdv1vffm5q28t0sj0 FOREIGN KEY (ean) REFERENCES cards (ean);


--
-- TOC entry 1954 (class 2606 OID 159085)
-- Name: fk_8ei1aqwkr6usove45kkwyvjug; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY abonemets
ADD CONSTRAINT fk_8ei1aqwkr6usove45kkwyvjug FOREIGN KEY (tariff_uuid) REFERENCES tariff (uuid);


--
-- TOC entry 1974 (class 2606 OID 159185)
-- Name: fk_bottvko4h8k4tnvk8k4vipslc; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY pos_entity
ADD CONSTRAINT fk_bottvko4h8k4tnvk8k4vipslc FOREIGN KEY (owner_id) REFERENCES company_entity (id);


--
-- TOC entry 1982 (class 2606 OID 159225)
-- Name: fk_cb4ngrty16ph97bvc014koowv; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY schedule
ADD CONSTRAINT fk_cb4ngrty16ph97bvc014koowv FOREIGN KEY (session) REFERENCES sessions (id);


--
-- TOC entry 1958 (class 2606 OID 159105)
-- Name: fk_cu87k9vu2nkr5x70ptdbw7cx9; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY client_base_persons
ADD CONSTRAINT fk_cu87k9vu2nkr5x70ptdbw7cx9 FOREIGN KEY (client_base_id) REFERENCES client_base (id);


--
-- TOC entry 1972 (class 2606 OID 159175)
-- Name: fk_dfwr1862yqkwpf61grwplf8w6; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY person_entity
ADD CONSTRAINT fk_dfwr1862yqkwpf61grwplf8w6 FOREIGN KEY (current_ean) REFERENCES cards (ean);


--
-- TOC entry 1968 (class 2606 OID 159155)
-- Name: fk_eecqc1tood4cy1sx3yv0oxfls; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY payment
ADD CONSTRAINT fk_eecqc1tood4cy1sx3yv0oxfls FOREIGN KEY (abonement_uuid) REFERENCES abonemets (uuid);


--
-- TOC entry 1980 (class 2606 OID 159215)
-- Name: fk_ff69ianm367o7qlptiy4bbq9s; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY schedule
ADD CONSTRAINT fk_ff69ianm367o7qlptiy4bbq9s FOREIGN KEY (resource) REFERENCES resources (id);


--
-- TOC entry 1957 (class 2606 OID 159100)
-- Name: fk_i9njgog8b1022mrbp79mawl7j; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY client_base_persons
ADD CONSTRAINT fk_i9njgog8b1022mrbp79mawl7j FOREIGN KEY (persons_uuid) REFERENCES person_entity (uuid);


--
-- TOC entry 1952 (class 2606 OID 159075)
-- Name: fk_iaf8925189qm2exb0ehrxclb1; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY abonemets
ADD CONSTRAINT fk_iaf8925189qm2exb0ehrxclb1 FOREIGN KEY (ean) REFERENCES cards (ean);


--
-- TOC entry 1966 (class 2606 OID 159145)
-- Name: fk_jpppnh7wcq4tn9eakubcj6jhn; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY company_entity_pos
ADD CONSTRAINT fk_jpppnh7wcq4tn9eakubcj6jhn FOREIGN KEY (company_entity_id) REFERENCES company_entity (id);


--
-- TOC entry 1963 (class 2606 OID 159130)
-- Name: fk_kxoyrpsuhge9tqyyxu8n440y0; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY company_entity_client_bases
ADD CONSTRAINT fk_kxoyrpsuhge9tqyyxu8n440y0 FOREIGN KEY (client_bases_id) REFERENCES client_base (id);


--
-- TOC entry 1959 (class 2606 OID 159110)
-- Name: fk_lndb0cl0pp2bc2qd2sf31wyvv; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY client_base_poses
ADD CONSTRAINT fk_lndb0cl0pp2bc2qd2sf31wyvv FOREIGN KEY (poses_uuid) REFERENCES pos_entity (uuid);


--
-- TOC entry 1973 (class 2606 OID 159180)
-- Name: fk_lwmv3g8mkchg048lpn80n8wmy; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY pos_entity
ADD CONSTRAINT fk_lwmv3g8mkchg048lpn80n8wmy FOREIGN KEY (client_base_id) REFERENCES client_base (id);


--
-- TOC entry 1969 (class 2606 OID 159160)
-- Name: fk_n2vi8js633u11ltkgqkg21x0f; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY payment
ADD CONSTRAINT fk_n2vi8js633u11ltkgqkg21x0f FOREIGN KEY (coach_uuid) REFERENCES coach (uuid);


--
-- TOC entry 1975 (class 2606 OID 159190)
-- Name: fk_n429cxd3bf1ftg36b3fuagbe5; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY pos_entity_users
ADD CONSTRAINT fk_n429cxd3bf1ftg36b3fuagbe5 FOREIGN KEY (users_uuid) REFERENCES pos_user_entity (uuid);


--
-- TOC entry 1964 (class 2606 OID 159135)
-- Name: fk_nw1p8ftfoj04p3xn45bcbjyiy; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY company_entity_client_bases
ADD CONSTRAINT fk_nw1p8ftfoj04p3xn45bcbjyiy FOREIGN KEY (company_entity_id) REFERENCES company_entity (id);

--
-- TOC entry 1971 (class 2606 OID 159170)
-- Name: fk_owe9xpqh6m7aoqqbqij3m5ksp; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY payment
ADD CONSTRAINT fk_owe9xpqh6m7aoqqbqij3m5ksp FOREIGN KEY (service_id) REFERENCES service (id);


--
-- TOC entry 1953 (class 2606 OID 159080)
-- Name: fk_py1ofskqyyr4bogv4saeqdmpn; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY abonemets
ADD CONSTRAINT fk_py1ofskqyyr4bogv4saeqdmpn FOREIGN KEY (user_uuid) REFERENCES pos_user_entity (uuid);


--
-- TOC entry 1981 (class 2606 OID 159220)
-- Name: fk_qn3jr6dxqt6qn1crd9yr92nwh; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY schedule
ADD CONSTRAINT fk_qn3jr6dxqt6qn1crd9yr92nwh FOREIGN KEY (service) REFERENCES service (id);


--
-- TOC entry 1985 (class 2606 OID 159240)
-- Name: fk_su0iy7673ygqesbqonxuwc8g5; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY user_entity
ADD CONSTRAINT fk_su0iy7673ygqesbqonxuwc8g5 FOREIGN KEY (company_id) REFERENCES company_entity (id);


--
-- TOC entry 1956 (class 2606 OID 159095)
-- Name: fk_t8cev8k7dphuyxve9522pm18e; Type: FK CONSTRAINT; Schema: public; Owner: vipcod
--

ALTER TABLE ONLY cards
ADD CONSTRAINT fk_t8cev8k7dphuyxve9522pm18e FOREIGN KEY (client_uuid) REFERENCES person_entity (uuid);


-- Completed on 2015-09-02 19:12:39

--
-- PostgreSQL database dump complete
--

