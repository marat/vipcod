ALTER TABLE service ADD COLUMN no_nds boolean NOT NULL DEFAULT false;

----- только для дворца спорта:
update payment set sum = 0 where created <= 1447684779501;
update service set no_nds = true where id in (10005, 10007, 10008);
update payment set ean = (select current_ean from person_entity where person_entity.uuid = payment.person_uuid) where payment.ean is null;
-- преобразование ean в число
ALTER TABLE person_entity DROP CONSTRAINT fk_dfwr1862yqkwpf61grwplf8w6;
update cards set ean = substr( replace(replace(replace(replace(replace(replace(replace(ean,'-',''),'a',''),'b',''),'c',''),'d',''),'e',''),'f','') ,0,12 );
update person_entity set current_ean = substr( replace(replace(replace(replace(replace(replace(replace(current_ean,'-',''),'a',''),'b',''),'c',''),'d',''),'e',''),'f','') ,0,12 );
ALTER TABLE ONLY person_entity ADD CONSTRAINT fk_dfwr1862yqkwpf61grwplf8w6 FOREIGN KEY (current_ean) REFERENCES cards (ean);
update payment set ean = substr( replace(replace(replace(replace(replace(replace(replace(ean,'-',''),'a',''),'b',''),'c',''),'d',''),'e',''),'f','') ,0,12 );
----- http://127.0.0.1:8080/vipcod-server-1.0/payment/updateNds


