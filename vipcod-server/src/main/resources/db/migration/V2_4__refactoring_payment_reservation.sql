ALTER TABLE reservations ADD COLUMN payment_uuid CHARACTER VARYING(255);
ALTER TABLE reservations ADD COLUMN payment_compensator_uuid CHARACTER VARYING(255);

ALTER TABLE ONLY reservations
ADD CONSTRAINT fk_payment_uuid FOREIGN KEY (payment_uuid) REFERENCES payment (uuid);

DROP TABLE PAYMENT_RESERVATIONS;

ALTER TABLE reservations DROP CONSTRAINT "reservations_person_uuid_schedule_id_key";

ALTER TABLE ONLY schedule_plan
ADD CONSTRAINT uk_schedule_plan_name UNIQUE (name);
