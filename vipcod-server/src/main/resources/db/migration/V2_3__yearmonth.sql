CREATE OR REPLACE VIEW dim_monthyear AS
  SELECT dq.idx + 1 AS id,
         (to_char(dq.datum::timestamp with time zone, 'TMMonth'::text) || ' '::text) || date_part('year'::text, dq.datum) AS yearmonthname,
         date_part('year'::text, dq.datum) AS year,
         date_part('month'::text, dq.datum) AS month
  FROM ( SELECT sequence.idx,
           '2015-01-01'::date + sequence.idx::double precision * '1 mon'::interval AS datum
         FROM generate_series(0, 239) sequence(idx)
         GROUP BY sequence.idx) dq
  ORDER BY dq.datum;


DROP VIEW dim_date;
CREATE OR REPLACE VIEW dim_date AS
  SELECT dq.datum AS date,
         date_part('year'::text, dq.datum) AS year,
         date_part('month'::text, dq.datum) AS month,
         to_char(dq.datum::timestamp with time zone, 'TMMonth'::text) AS monthname,
         (to_char(dq.datum::timestamp with time zone, 'TMMonth'::text) || ' '::text) || date_part('year'::text, dq.datum) AS yearmonthname,
         my.id AS ym_id,
         date_part('day'::text, dq.datum) AS day,
         date_part('doy'::text, dq.datum) AS dayofyear,
         to_char(dq.datum::timestamp with time zone, 'TMDay'::text) AS weekdayname,
         date_part('week'::text, dq.datum) AS calendarweek,
         to_char(dq.datum::timestamp with time zone, 'dd. mm. yyyy'::text) AS formatteddate,
         'Q'::text || to_char(dq.datum::timestamp with time zone, 'Q'::text) AS quartal,
         to_char(dq.datum::timestamp with time zone, 'yyyy/"Q"Q'::text) AS yearquartal,
         to_char(dq.datum::timestamp with time zone, 'yyyy/mm'::text) AS yearmonth,
         to_char(dq.datum::timestamp with time zone, 'iyyy/IW'::text) AS yearcalendarweek,
         CASE
         WHEN date_part('isodow'::text, dq.datum) = ANY (ARRAY[6::double precision, 7::double precision]) THEN 'Weekend'::text
         ELSE 'Weekday'::text
         END AS weekend,
         CASE
         WHEN to_char(dq.datum::timestamp with time zone, 'MMDD'::text) >= '0701'::text AND to_char(dq.datum::timestamp with time zone, 'MMDD'::text) <= '0831'::text THEN 'Summer break'::text
         WHEN to_char(dq.datum::timestamp with time zone, 'MMDD'::text) >= '1115'::text AND to_char(dq.datum::timestamp with time zone, 'MMDD'::text) <= '1225'::text THEN 'Christmas season'::text
         WHEN to_char(dq.datum::timestamp with time zone, 'MMDD'::text) > '1225'::text OR to_char(dq.datum::timestamp with time zone, 'MMDD'::text) <= '0106'::text THEN 'Winter break'::text
         ELSE 'Normal'::text
         END AS period,
         dq.datum + (1::double precision - date_part('isodow'::text, dq.datum))::integer AS cwstart,
         dq.datum + (7::double precision - date_part('isodow'::text, dq.datum))::integer AS cwend,
         dq.datum + (1::double precision - date_part('day'::text, dq.datum))::integer AS monthstart,
         (dq.datum + (1::double precision - date_part('day'::text, dq.datum))::integer + '1 mon'::interval)::date - '1 day'::interval AS monthend
  FROM ( SELECT '2015-01-01'::date + sequence.day AS datum
         FROM generate_series(0, 7304) sequence(day)
         GROUP BY sequence.day) dq
    LEFT JOIN dim_monthyear my ON my.year = date_part('year'::text, dq.datum) AND my.month = date_part('month'::text, dq.datum)
  ORDER BY dq.datum;

update payment set period = period + 12 where period < 10;
