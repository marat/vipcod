ALTER TABLE TARIFF ADD COLUMN price_one DOUBLE PRECISION NOT NULL DEFAULT 0;

ALTER TABLE PAYMENT ADD COLUMN tariff_uuid CHARACTER VARYING(255);
ALTER TABLE ONLY PAYMENT ADD CONSTRAINT fk_tariff_uuid FOREIGN KEY (tariff_uuid) REFERENCES TARIFF (uuid);

/*дворец спорта only*/
insert into TARIFF (uuid, created, updated, count_portion, days_portion, deleted, name, owner_id, price, price_one) VALUES ('001', 0, 0, 999, 999, true, 'не указан', 2, 0, 0);
insert into TARIFF (uuid, created, updated, count_portion, days_portion, deleted, name, owner_id, price, price_one) VALUES ('002', 0, 0, 999, 999, true, 'не указан', 2, 0, 0);
insert into TARIFF (uuid, created, updated, count_portion, days_portion, deleted, name, owner_id, price, price_one) VALUES ('003', 0, 0, 999, 999, true, 'не указан', 2, 0, 0);
insert into TARIFF (uuid, created, updated, count_portion, days_portion, deleted, name, owner_id, price, price_one) VALUES ('004', 0, 0, 999, 999, true, 'не указан', 2, 0, 0);
insert into TARIFF (uuid, created, updated, count_portion, days_portion, deleted, name, owner_id, price, price_one) VALUES ('005', 0, 0, 999, 999, true, 'не указан', 2, 0, 0);
insert into TARIFF (uuid, created, updated, count_portion, days_portion, deleted, name, owner_id, price, price_one) VALUES ('006', 0, 0, 999, 999, true, 'не указан', 2, 0, 0);
insert into TARIFF (uuid, created, updated, count_portion, days_portion, deleted, name, owner_id, price, price_one) VALUES ('007', 0, 0, 999, 999, true, 'не указан', 2, 0, 0);

insert into SERVICE_TARIFF values (10005, '001');
insert into SERVICE_TARIFF values (10006, '002');
insert into SERVICE_TARIFF values (10007, '003');
insert into SERVICE_TARIFF values (10008, '004');
insert into SERVICE_TARIFF values (10009, '005');
insert into SERVICE_TARIFF values (10010, '006');
insert into SERVICE_TARIFF values (10011, '007');
UPDATE PAYMENT SET tariff_uuid = (select tariff_uuid from SERVICE_TARIFF ST where ST.service_id=PAYMENT.service_id) where PAYMENT.tariff_uuid is null;

ALTER TABLE PAYMENT ADD COLUMN visits BIGINT;
ALTER TABLE PAYMENT ADD COLUMN back_sum BIGINT DEFAULT 0;