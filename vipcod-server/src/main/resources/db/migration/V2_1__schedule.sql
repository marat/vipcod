CREATE TABLE PAYMENT_RESERVATIONS (
  payment_uuid CHARACTER VARYING(255)      NOT NULL,
  reservation_id          BIGINT NOT NULL
);

alter table reservations add unique (person_uuid, schedule_id);

CREATE TABLE SCHEDULE_PLAN (
  id          BIGINT NOT NULL,
  created     BIGINT NOT NULL,
  updated     BIGINT,
  name CHARACTER VARYING(255)      NOT NULL,
  plan        TEXT   NOT NULL
);

ALTER TABLE ONLY SCHEDULE_PLAN
ADD CONSTRAINT SCHEDULE_PLAN_pkey PRIMARY KEY (id);

insert into SCHEDULE_PLAN values (1, 0, 0, 'default', '');

