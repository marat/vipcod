CREATE OR REPLACE FUNCTION back_sum_after_date(varchar, timestamp without time zone, bigint) RETURNS varchar AS $$
DECLARE
  v_client ALIAS FOR $1;
  v_time ALIAS FOR $2;
  v_owner ALIAS FOR $3;
  t2_row payment%ROWTYPE;
  v_sum real;
  v_last_date date;
  pay RECORD;
BEGIN
  v_last_date := NULL;
  v_sum := 0;
  FOR pay IN SELECT * FROM PAYMENT WHERE owner_id = v_owner AND person_uuid = v_client AND pay_time::date > v_time::date ORDER BY pay_time ASC LIMIT 10 LOOP

    IF v_last_date is NULL THEN
      v_last_date = pay.pay_time::date;
    END IF;

    IF pay.pay_time::date <> v_last_date THEN
      RETURN  v_sum;
    END IF;

    v_sum := pay.back_sum + v_sum;
  END LOOP;

  RETURN v_sum;
END;
$$ LANGUAGE plpgsql;

--select back_sum_after_date('b42db106-a4a5-4e8c-82a7-6a310b0bfbeb', '2015-12-19', 2);