package org.marat.vipcod.server.rest.sync;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.marat.vipcod.model.entities.server.Pos;
import org.marat.vipcod.model.integration.synch.VersionedResponse;
import org.marat.vipcod.server.config.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;

import static org.mockito.Matchers.any;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.spy;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
//@IntegrationTest({"flyway.enabled=true", "spring.datasource.url=jdbc:h2:mem:test", "spring.datasource.username=sa", "spring.datasource.driverClassName=org.h2.Driver"})
@IntegrationTest({"flyway.enabled=false", "spring.datasource.continue-on-error=true"})
public class RequestRestServiceTest {

    @Autowired
    EntityManager em;

    @Autowired
    RequestRestService requestRestServiceOriginal;

    RequestRestService requestRestServiceMock;

    public void initMocks() {
        requestRestServiceMock = spy(requestRestServiceOriginal);
        doReturn(em.getReference(Pos.class, "123")).when(requestRestServiceMock).getByPosId(any(String.class));
    }

    @Test
    public void testRequestHistory() throws Exception {
        initMocks();

        VersionedResponse resp = requestRestServiceMock.requestHistory("uid", 0L, Long.MAX_VALUE);
        System.out.println(resp);
    }

    @Test
    public void testRequestActivities() throws Exception {

    }

    @Test
    public void testRequestPersons() throws Exception {

    }

    @Test
    public void testRequestCards() throws Exception {

    }

    @Test
    public void testRequestPosUsers() throws Exception {

    }
}