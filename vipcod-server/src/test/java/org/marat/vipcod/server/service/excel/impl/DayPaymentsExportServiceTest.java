package org.marat.vipcod.server.service.excel.impl;

import org.junit.Test;
import org.marat.vipcod.model.entities.Payment;
import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.entities.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class DayPaymentsExportServiceTest {

    DayPaymentsExportService service = new DayPaymentsExportService();

    @Test
    public void testReport() throws Exception {
        List<Payment> payments = getItems();
        File tempFile = File.createTempFile("pref", ".xlsx");
        OutputStream out = new FileOutputStream(tempFile);
        service.report(new DayReportData(payments, new ArrayList()), new Date(), out, null);
        //Runtime.getRuntime().exec("explorer " + tempFile.getAbsolutePath());
    }

    public List<Payment> getItems() {
        List<Payment> items = new ArrayList<>();
        items.add(newPayment());
        items.add(newPayment());
        items.add(newPayment());
        return items;
    }

    private Payment newPayment() {
        Payment pay = new Payment(UUID.randomUUID().toString());
        pay.setNds(true);
        pay.setSum(1000.0);
        pay.setPayTime(new Date());
        pay.setPerson(newPerson());
        pay.setService(new Service("dummy service"));
        pay.setVisitsCount(1L);
        return pay;
    }

    private Person newPerson() {
        Person person = new Person(UUID.randomUUID().toString());
        person.setBirth(new Date());
        person.setName("name");
        person.setSecondName("sname");
        person.setMiddleName("mname");
        return person;
    }
}