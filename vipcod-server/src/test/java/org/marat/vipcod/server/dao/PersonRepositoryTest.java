package org.marat.vipcod.server.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.marat.vipcod.model.entities.server.Pos;
import org.marat.vipcod.server.config.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@IntegrationTest({
        "flyway.enabled=false",
        "spring.datasource.url=jdbc:h2:mem:test",
        "spring.datasource.username=sa",
        "spring.datasource.driverClassName=org.h2.Driver"})
public class PersonRepositoryTest {

    @Autowired
    EntityManager em;

    @Autowired
    PersonRepository personRepository;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testFindByName() throws Exception {

    }

    @Test
    public void testFindByOwnerId() throws Exception {

    }

    @Test
    public void testFindTotalByOwnerId() throws Exception {

    }

    @Test
    public void testFindByVersions() throws Exception {
        personRepository.findByVersions(em.getReference(Pos.class, "123"), 0L, Long.MAX_VALUE);
    }
}