package org.marat.vipcod.server.service.excel.impl;

import org.junit.Test;
import org.marat.vipcod.model.entities.Payment;
import org.marat.vipcod.model.entities.Service;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class DayReportDataTest {

    DayReportData dayReportData = new DayReportData();

    @Test
    public void setPayments() {

        dayReportData.setPayments(new ArrayList<Payment>(){{
            Payment payment = new Payment("111");
            payment.setService(new Service("serv1"));
            payment.setVisitsCount(0L);
            payment.setNds(true);
            payment.setCash(false);
            payment.setSum(1000.0);
            add(payment);
        }});

        dayReportData.setPayments(new ArrayList<Payment>(){{
            Payment payment = new Payment("222");
            payment.setService(new Service("serv1"));
            payment.setVisitsCount(0L);
            payment.setNds(true);
            payment.setCash(true);
            payment.setSum(2000.0);
            add(payment);
        }});

        assert dayReportData.getAgregateNds().size() == 1;
        assert dayReportData.getAgregateNoNds().size() == 0;
        assert dayReportData.getAgregateNds().get(0).getSum() == 3000;
        assert dayReportData.getAgregateNds().get(0).getcSum() == 2000;
        assert dayReportData.getAgregateNds().get(0).geteSum() == 1000;
    }
}