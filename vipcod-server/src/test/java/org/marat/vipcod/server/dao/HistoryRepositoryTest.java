package org.marat.vipcod.server.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.server.config.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;

import java.util.Arrays;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@IntegrationTest({
        "flyway.enabled=false",
        "spring.datasource.url=jdbc:h2:mem:test;DB_CLOSE_ON_EXIT=FALSE",
        "spring.datasource.username=sa",
        "spring.datasource.driverClassName=org.h2.Driver"})
public class HistoryRepositoryTest {

    @Autowired
    EntityManager em;

    @Autowired
    HistoryRepository historyRepository;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testFindByVersions() throws Exception {
        historyRepository.findByVersions(Arrays.asList(new Person("111"), new Person("222")), 0L, Long.MAX_VALUE);
    }

    @Test
    public void testFindByUuid() throws Exception {
        historyRepository.findByUuid("123");
    }
}