package org.marat.vipcod.model.integration.synch;

import java.io.Serializable;

/**
 * Created by Администратор on 20.10.2014.
 */
public class RequestPosSettings implements Serializable {
    private String systemUid;

    public String getSystemUid() {
        return systemUid;
    }

    public void setSystemUid(String systemUid) {
        this.systemUid = systemUid;
    }
}
