package org.marat.vipcod.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.marat.vipcod.model.entities.action.Abonement;
import org.marat.vipcod.model.entities.action.ActionHistory;
import org.marat.vipcod.model.entities.pos.PosUser;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 */
@NamedQueries({
        @NamedQuery(
                name = "Card.findByFilter",
                query = "select o from Card o where upper(o.ean) like upper(:filter) OR upper(o.person.name) like upper(:filter)")
})
@Entity
@Table(name = "CARDS")
@Cacheable(false)
public class Card extends AbstractEntity {
    @Id
    @NotNull
    //@Digits(integer = 13, fraction = 0) - закомментировано, поскольку допускаю uuid или значение QR
    @Size(max = 255)
    @Column(name = "ean", length = 255)
    private String ean;

    @Valid
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Person.class, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "client_uuid")
    private Person person;

    @Valid
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = PosUser.class)
    @JoinColumn(name = "user_uuid")
    private PosUser manager;

    @Column(name = "owner_id", nullable = true) // в терминале owner не важен
    private long ownerId;

    /*@Valid
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "card")
    @OrderBy(value = "time desc")
    @XmlTransient
    @JsonIgnore
    private List<ActionHistory> history = new ArrayList<>();*/

    @Column(name = "bonus")
    private Double bonus = 0.0;

    private String comment;

    @Transient
    //@XmlTransient
    //@JsonIgnore - отменено чтобы получить фио от angular
    private String transientPersonFio;

    @Transient
    //@XmlTransient
    //@JsonIgnore - отменено чтобы получить фио от angular
    private Long transientPersonId;


    public Card() {
    }


    public Card(String ean, Person person) {
        this.ean = ean;
        this.setPerson(person);
        //this.limit.setCard(this);
    }

    public Card(String ean) {
        this.setEan(ean);
        //this.limit.setCard(this);
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public PosUser getManager() {
        return manager;
    }

    public void setManager(PosUser manager) {
        this.manager = manager;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    /*public List<ActionHistory> getHistory() {
        return history;
    }

    public void setHistory(List<ActionHistory> history) {
        this.history = history;
    }*/

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public String getTransientPersonFio() {
        return transientPersonFio == null && person != null ? person.getName() : transientPersonFio;
    }

    public Double getBonus() {
        return bonus;
    }

    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }

    @Transient
    public void copyTo(Card targetCard) {
        targetCard.setOwnerId(this.getOwnerId());
        targetCard.setEan(this.getEan());
        if (this.getCreated() != null) {
            targetCard.setCreated(this.getCreated());
        }
        targetCard.setBonus(this.getBonus());
        targetCard.setComment(this.getComment());
        //targetCard.setHistory(this.getHistory());

        if (targetCard.getPerson() == null) targetCard.setPerson(new Person());
        this.getPerson().copyTo(targetCard.getPerson());

        if (this.getManager() != null) {
            targetCard.setManager(this.getManager());
        }
    }

    @Override
    public String toString() {
        return "Card{" + ean + ", upd=" + getUpdated() + ", crt=" + getCreated() + "}";
    }

}
