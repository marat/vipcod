package org.marat.vipcod.model;

/**
 * Created by Администратор on 25.11.2014.
 */
public class VipcodMessageException extends Exception  {
    public VipcodMessageException() {
    }

    public VipcodMessageException(String message) {
        super(message);
    }

    public VipcodMessageException(String message, Throwable cause) {
        super(message, cause);
    }

    public VipcodMessageException(Throwable cause) {
        super(cause);
    }

    public VipcodMessageException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
