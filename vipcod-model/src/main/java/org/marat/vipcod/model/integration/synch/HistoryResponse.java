package org.marat.vipcod.model.integration.synch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.marat.vipcod.model.entities.action.ActionHistory;

import java.util.List;

/**
 * Created by Islamov Marat on 07.12.2014.
 */
public class HistoryResponse extends VersionedResponse<ActionHistory>{
    private static final Logger LOG = LoggerFactory.getLogger(HistoryResponse.class);

    public HistoryResponse() {
    }

    public HistoryResponse(Long versionFrom, Long versionTo, List<ActionHistory> data) {
        super(versionFrom, versionTo, data);
    }
}
