package org.marat.vipcod.model.entities.schedule;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.JsonNode;
import org.marat.vipcod.model.entities.AbstractEntityWithId;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "SCHEDULE_PLAN")
@Cacheable(false)
public class SchedulePlan extends AbstractEntityWithId {

    @JsonProperty(value = "name")
    private String name;

    private String plan;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonRawValue
    public String getPlan() {
        return plan;
    }

    public void setPlan(JsonNode plan) {
        this.plan = plan.toString();
    }

    @Override
    public String toString() {
        return "SchedulePlan{" +
                "name='" + name + '\'' +
                ", plan=" + plan +
                '}';
    }
}
