package org.marat.vipcod.model.entities;

import javax.persistence.*;

/**
 * Сеанс
 */
@Entity
@Table(name = "SESSIONS")
@Cacheable(false)
public class Session extends AbstractEntityWithId implements Comparable<Session> {

    /**
     * порядковый номер дня недели
     */
    @Column(name = "day_of_week")
    private int dayOfWeek;

    /**
     */
    @Column(name = "startTime")
    private String begin;

    /**
     */
    @Column(name = "endTime")
    private String end;

    @Transient
    public String getName() {
        return begin + "-" + end;
    }

    public Session() {
    }

    public Session(int dayOfWeek, String begin, String end) {
        this.dayOfWeek = dayOfWeek;
        this.begin = begin;
        this.end = end;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Session)) return false;
        if (!super.equals(o)) return false;

        Session session = (Session) o;

        if (dayOfWeek != session.dayOfWeek) return false;
        if (begin != null ? !begin.equals(session.begin) : session.begin != null) return false;
        if (end != null ? !end.equals(session.end) : session.end != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + dayOfWeek;
        result = 31 * result + (begin != null ? begin.hashCode() : 0);
        result = 31 * result + (end != null ? end.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public int compareTo(Session o) {
        return getDayOfWeek() == o.getDayOfWeek() ? getBegin().compareTo(o.getBegin()) : getDayOfWeek() - o.getDayOfWeek();
    }
}
