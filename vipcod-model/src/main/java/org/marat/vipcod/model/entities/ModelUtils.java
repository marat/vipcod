package org.marat.vipcod.model.entities;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by marat on 10.06.2014.
 */
public class ModelUtils {

    public static DateFormat df = new SimpleDateFormat("dd.MM.yyyy") {

        @Override
        public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition pos) {
            if (date == null) return new StringBuffer();
            return super.format(date, toAppendTo, pos);
        }
    };

    public static DateFormat dtf = new SimpleDateFormat("dd.MM.yyyy HH:mm") {
        @Override
        public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition pos) {
            if (date == null) return new StringBuffer();
            return super.format(date, toAppendTo, pos);
        }
    };

    public static Date beginOfDay(Date date) {
        if (date == null) return null;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(year, month, day, 0, 0, 0);
        return calendar.getTime();
    }

    public static Date endOfDay(Date date) {
        if (date == null) return null;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(Calendar.MILLISECOND, 999);
        calendar.set(year, month, day, 23, 59, 59);
        return calendar.getTime();
    }

    public static Date middleOfDay(Date date) {
        if (date == null) return null;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(Calendar.MILLISECOND, 555);
        calendar.set(year, month, day, 12, 30, 30);
        return calendar.getTime();
    }

    public static Date addDays(Date date, int days) {

        /*if (date == null) {
            date = new Date();
        }
        LocalDateTime ldt = LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneOffset.UTC);
        LocalDateTime plusDays = ldt.plusDays(days);
        return Date.from(plusDays.toInstant(ZoneOffset.UTC));*/

        if (date == null) return new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, days);
        return calendar.getTime();
    }

    public static boolean isJavascriptNullValue(String value) {
        return value == null || value.equals("") || value.toLowerCase().equals("null") || value.toLowerCase().equals("undefined");
    }
}
