package org.marat.vipcod.model.entities;

import com.fasterxml.jackson.annotation.JsonFilter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * Оказываемые услуги
 */
@NamedQueries({
        @NamedQuery(
                name = "Service.findByName",
                query = "select o from Service o where o.name = :name")
})

@Entity
@Table(name = "SERVICE")
@Cacheable(false)
public class Service extends AbstractEntityWithId {

    @NotNull
    @Size(min = 2, max = 255)
    private String name;

    @NotNull
    @Column(name = "owner_id")
    private long ownerId;

    @NotNull
    @Column(name = "no_nds")
    private boolean noNds;


    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name="SERVICE_TARIFF",
            joinColumns = @JoinColumn( name="service_id"),
            inverseJoinColumns = @JoinColumn( name="tariff_uuid")
    )
    @OrderBy("name")
    private List<Tariff> tariffs = new ArrayList<>();

    @Column(name = "deleted")
    private boolean deleted;

    public Service() {
    }

    public Service(String name) {
        this.name = name;
    }


    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Tariff> getTariffs() {
        return tariffs;
    }

    public void setTariffs(List<Tariff> tariffs) {
        this.tariffs = tariffs;
    }

    public Service(long id) {
        this.id = id;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public boolean isNoNds() {
        return noNds;
    }

    public void setNoNds(boolean noNds) {
        this.noNds = noNds;
    }
}
