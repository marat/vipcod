package org.marat.vipcod.model.integration.api.impl;

import org.marat.vipcod.model.integration.api.PagedRequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Created by Marat on 27.06.2015.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentPagedRequest extends PagedRequest {

    private String personUuid;
    private String coachUuid;
    private Long serviceId;
    private Long period;
    private String filter;

    public String getPersonUuid() {
        return personUuid;
    }

    public void setPersonUuid(String personUuid) {
        this.personUuid = personUuid;
    }

    public String getCoachUuid() {
        return coachUuid;
    }

    public void setCoachUuid(String coachUuid) {
        this.coachUuid = coachUuid;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public Long getPeriod() {
        return period;
    }

    public void setPeriod(Long period) {
        this.period = period;
    }

    @Override
    public String getFilter() {
        return filter;
    }

    @Override
    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public String toString() {
        return "PaymentPagedRequest{" +
                "personUuid='" + personUuid + '\'' +
                ", coachUuid='" + coachUuid + '\'' +
                ", serviceId=" + serviceId +
                ", period=" + period +
                ", filter='" + filter + '\'' +
                "} " + super.toString();
    }
}
