package org.marat.vipcod.model.entities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * клиент заведения
 */
@Entity
@Table(name = "PERSON_ENTITY")
public class Person extends AbstractEntityWithUuid {

    @NotNull
    @Size(min = 2, max = 255, message = "некорректное имя")
    @Column(name = "name")
    private String name;

    @NotNull
    @Size(min = 2, max = 255, message = "некорректная фамилия")
    @Column(name = "second_name")
    private String secondName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "birth")
    @Past
    @Temporal(TemporalType.TIMESTAMP)
    private Date birth;

    @Size(min = 2, max = 255)
    @Column(name = "phone")
    private String phone;

    @Lob
    @Column(name = "photo")
    private byte[] photo;

    // в терминале owner не важен
    @Column(name = "owner_id")
    private long ownerId;

    /*@ManyToOne(targetEntity = Card.class)
    @JoinColumn(name = "current_ean", referencedColumnName = "ean")
    @JsonIgnoreProperties({"person"})
    private Card currentCard;*/

    @Column(name = "current_ean")
    private String currentCardEan;

    @Transient
    public String getFullName() {
        String fio = secondName + " " + name + " " + middleName;
        return fio.replaceAll("null", "").replaceAll("  ", " ").trim();
    }

    @Column(name = "deleted")
    private boolean deleted = false;

    public Person() {
    }

    public Person(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

/*
    public Card getCurrentCard() {
        return currentCard;
    }

    public void setCurrentCard(Card currentCard) {
        this.currentCard = currentCard;
    }
*/

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCurrentCardEan() {
        return currentCardEan;
    }

    public void setCurrentCardEan(String currentCardEan) {
        this.currentCardEan = currentCardEan;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted == null ? false : deleted;
    }



    public void copyTo(Person person) {
        person.setCurrentCardEan(this.getCurrentCardEan());
        person.setName(this.getName());
        person.setMiddleName(this.getMiddleName());
        person.setSecondName(this.getSecondName());
        person.setPhone(this.getPhone());
        person.setBirth(this.getBirth());
        person.setPhoto(this.getPhoto());
        person.setOwnerId(this.getOwnerId());
        person.setDeleted(this.getDeleted());
        if (this.getUuid() != null) {
            person.setUuid(this.getUuid());
        }
    }

    @Override
    public String toString() {
        return "Person{" +
                "fio='" + getFullName() + '\'' +
                ", upd=" + getUpdated() +
                " ,crt=" + getCreated() +
                '}';
    }


}
