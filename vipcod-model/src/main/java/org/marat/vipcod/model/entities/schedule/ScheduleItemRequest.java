package org.marat.vipcod.model.entities.schedule;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.marat.vipcod.model.entities.Coach;
import org.marat.vipcod.model.entities.Resource;
import org.marat.vipcod.model.entities.Service;
import org.marat.vipcod.model.entities.Session;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by marat on 18.04.2016.
 */
public class ScheduleItemRequest implements Serializable {

    @JsonProperty("dates")
    private List<Date> dates;

    @JsonProperty("sessions")
    private List<Session> sessions;

    @JsonProperty("sessionBegin")
    private String sessionBegin;

    /**
     *
     */
    private Resource resource;

    /**
     *
     */
    private Coach coach;

    /**
     *
     */
    @NotNull
    private Service service;

    /**
     *
     */
    private Long limit;

    public List<Date> getDates() {
        return dates;
    }

    public void setDates(List<Date> dates) {
        this.dates = dates;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Long getLimit() {
        return limit;
    }

    public void setLimit(Long limit) {
        this.limit = limit;
    }

    public String getSessionBegin() {
        return sessionBegin;
    }

    public void setSessionBegin(String sessionBegin) {
        this.sessionBegin = sessionBegin;
    }
}
