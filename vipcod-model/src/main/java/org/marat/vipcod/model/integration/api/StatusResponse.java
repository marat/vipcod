package org.marat.vipcod.model.integration.api;

import java.io.Serializable;

public class StatusResponse<T extends Serializable> implements Serializable {
    String error;

    T result;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public StatusResponse() {
    }

    public StatusResponse(String error) {
        this.error = error;
    }

    public StatusResponse(T result) {
        this.result = result;
    }

    public StatusResponse(T result, String error) {
        this.error = error;
        this.result = result;
    }
}
