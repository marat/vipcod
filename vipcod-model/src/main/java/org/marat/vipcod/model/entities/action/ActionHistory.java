package org.marat.vipcod.model.entities.action;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.marat.vipcod.model.entities.AbstractEntityWithUuid;
import org.marat.vipcod.model.entities.Card;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;

/**
 *
 */
@NamedQueries({
        @NamedQuery(name = "Report.getPayedHistoryForTime",
                query = "select h from ActionHistory h " +
                        "where h.time >= :timeFrom and h.time <= :timeTo and not h.payment is null"),
        @NamedQuery(name = "ActionHistory.getPayedHistory",
                query = "select h from ActionHistory h " +
                        "where not h.payment is null order by h.time, h.ean")
})
@Entity
@Table(name = "HISTORY")
public class ActionHistory extends AbstractEntityWithUuid {

    // EAN
    @NotNull
    @Column(name = "ean")
    private String ean;

    @XmlTransient
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = {})
    @JoinColumn(name = "ean", insertable = false, updatable = false)
    private Card card;

    @Column(name = "tariff")
    private String tariffUuid;

    // дата время
    @Column(name = "`time`")
    private Date time;

    // новый статус
    @Column(name = "newstatus")
    private String newStatus;

    // price
    @Column(name = "payment")
    private Double payment;

    @Column(name = "count")
    private Long count;

    @Column(name = "countmax")
    private Long countMax;

    @Column(name = "datebegin")
    private Date dateBegin;

    @Column(name = "dateend")
    private Date dateEnd;

    @Column(name = "pos_user_ean")
    private String posUserEan;

    @Column(name = "bonus")
    private Double bonus;

    public ActionHistory() {
    }

    public ActionHistory(Abonement abonement, String status, Double payment, Double bonusPayment, String posUserEan) {
        setCard(abonement.getCard());
        setTime(new Date());
        setEan(card.getEan());
        setNewStatus(status);
        setPayment(payment);
        setCount(abonement.getCount());
        setCountMax(abonement.getMaxCount());
        setDateBegin(abonement.getStartTime());
        setDateEnd(abonement.getEndTime());
        setPosUserEan(posUserEan);
        setBonus(bonusPayment);
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(String newStatus) {
        this.newStatus = newStatus;
    }

    public Double getPayment() {
        return payment;
    }

    public void setPayment(Double price) {
        this.payment = price;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getCountMax() {
        return countMax;
    }

    public void setCountMax(Long countMax) {
        this.countMax = countMax;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public String getPosUserEan() {
        return posUserEan;
    }

    public void setPosUserEan(String posUserEan) {
        this.posUserEan = posUserEan;
    }

    public Double getBonus() {
        return bonus;
    }

    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }

    public String getTariffUuid() {
        return tariffUuid;
    }

    public void setTariffUuid(String tariffUuid) {
        if (tariffUuid != null) {
            this.tariffUuid = tariffUuid;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ActionHistory)) return false;

        ActionHistory history = (ActionHistory) o;

        if (uuid != null ? !uuid.equals(history.uuid) : history.uuid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return uuid != null ? uuid.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "ActionHistory{" + ean + '\\' + count + "  " + posUserEan + '}';
    }
}
