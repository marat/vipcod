package org.marat.vipcod.model.integration.synch;

import org.marat.vipcod.model.entities.action.Abonement;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Администратор on 21.10.2014.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
//@XmlSeeAlso(PosUser.class)
public class ActivesResponse extends VersionedResponse<Abonement> {
    public ActivesResponse() {
    }

    public ActivesResponse(Long versionFrom, Long versionTo, List<Abonement> data) {
        super(versionFrom, versionTo, data);
    }
}
