package org.marat.vipcod.model.integration.synch;

import org.marat.vipcod.model.entities.Service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

/**
 * Created by marat on 09.09.2015.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@XmlSeeAlso(Service.class)
public class ServicesResponse extends VersionedResponse<Service> {
    public ServicesResponse() {
    }

    public ServicesResponse(Long versionFrom, Long versionTo, List<Service> data) {
        super(versionFrom, versionTo, data);
    }
}
