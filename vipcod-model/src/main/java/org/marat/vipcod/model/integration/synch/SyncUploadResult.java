package org.marat.vipcod.model.integration.synch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * Created by Islamov Marat on 07.12.2014.
 */
public class SyncUploadResult implements Serializable {
    private static final Logger LOG = LoggerFactory.getLogger(SyncUploadResult.class);

    private Long version;
    private String errorMessage;

    public Long version() {
        if (getErrorMessage() == null) return version;
        LOG.error("version() for failed SyncUploadResult");
        throw new SynchUploadException(getErrorMessage());
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public SyncUploadResult() {
    }

    public SyncUploadResult(Long version) {
        this.version = version;
    }

    public SyncUploadResult(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
