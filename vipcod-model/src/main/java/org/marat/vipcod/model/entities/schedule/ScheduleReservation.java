package org.marat.vipcod.model.entities.schedule;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.marat.vipcod.model.entities.AbstractEntityWithId;
import org.marat.vipcod.model.entities.Payment;
import org.marat.vipcod.model.entities.Person;

import javax.persistence.*;

/**
 * Запись клиента на сеанс
 */
@Entity
@Table(name = "RESERVATIONS")
@Cacheable(false)
public class ScheduleReservation extends AbstractEntityWithId {

    /**
     * записанный клиент
     */
    @ManyToOne
    private Person person;

    /**
     * точка записи в расписании
     */
    @ManyToOne
    private ScheduleItem schedule;

    /**
     * платеж, по которому осуществлена данная запись клиента
     */
    @ManyToOne
    @JoinColumn(name = "payment_uuid")
    @JsonIgnoreProperties({"reservations", "compensatedReservations"})
    private Payment payment;

    /**
     * платеж, в рамках которого данная запись была компенсирована
     */
    @ManyToOne
    @JoinColumn(name = "payment_compensator_uuid")
    @JsonIgnoreProperties({"reservations", "compensatedReservations"})
    private Payment paymentCompensator;

    @Transient
    public String getCompensated() {
        return paymentCompensator == null ? null : "YES";
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public ScheduleItem getSchedule() {
        return schedule;
    }

    public void setSchedule(ScheduleItem schedule) {
        this.schedule = schedule;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Payment getPaymentCompensator() {
        return paymentCompensator;
    }

    public void setPaymentCompensator(Payment paymentCompensator) {
        this.paymentCompensator = paymentCompensator;
    }
}
