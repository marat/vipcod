package org.marat.vipcod.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.marat.vipcod.model.EntitiesUtil;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.xml.bind.annotation.*;
import java.beans.Transient;
import java.io.Serializable;

/**
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

    @XmlElement
    @Column(name = "created", nullable = false)
    @JsonIgnore // у клиента и сервера свои значения
    private long created;

    @XmlElement
    @Column(name = "updated" /*, nullable = false*/)
    @JsonIgnore // у клиента и сервера свои значения
    private Long updated;

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        if (created != null && created != 0) this.created = created;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    @PrePersist
    protected void onCreate() {
        if (created == 0) {
            updated = created = EntitiesUtil.getNextGlobalVersion();
        }
        /*if ((created > 1462046400000L) && (this instanceof Card)){
            Card card = (Card) this;
            card.setEan("#" + card.getEan().substring(1));
        }*/
    }

    @PreUpdate
    protected void onUpdate() {
        updated = EntitiesUtil.getNextGlobalVersion();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "created=" + created +
                ", updated=" + updated +
                '}';
    }
}
