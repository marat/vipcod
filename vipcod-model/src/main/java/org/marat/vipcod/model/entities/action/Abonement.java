package org.marat.vipcod.model.entities.action;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.marat.vipcod.model.entities.AbstractEntityWithUuid;
import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.Tariff;
import org.marat.vipcod.model.entities.CardLogicalState;
import org.marat.vipcod.model.entities.pos.PosUser;

import javax.persistence.*;
import javax.validation.Valid;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;

/**
 *
 */
@Entity
@Table(name = "ABONEMETS")
@Cacheable(false)
public class Abonement extends AbstractEntityWithUuid {

    // терминал, которому принадлежит это состояние карты. теоретически, на одну карту могут быть разные состояния
    // тут нужно продумать как будут списываться посещения по партнерке, ведь некоторые заведения захотят по карте
    // вести свой отдельный учет, а некоторые совместный. Для отдельного учета будет создаваться отдельное состояние
    // с соответствующим posUid, а если группа партнеров - то тут надо тщательно продумать. Возможно, вместо pos надо
    // будет хранить clientBase, так как он есть как индивидуальный для терминала, так и на группу партнеров.
    //@Column(name = "pos")
    //@NotNull
    //private String pos;

    @ManyToOne(cascade = {})
    @JoinColumn(name = "ean", referencedColumnName = "ean")
    //@JsonIgnore - нужно передавать еан
    private Card card;

    // кол-во
    @Column(name = "count")
    private long count = 0L;

    @Column(name = "record_time")
    private Date recordTime;

    //// LIMIT
    // max кол-во
    @Column(name = "max_count")
    private Long maxCount;

    // start data
    @Column(name = "start_time")
    private Date startTime;

    // end data
    @Column(name = "end_time")
    private Date endTime;

    // null для произвольной модели
    @Valid
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Tariff.class, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "tariff_uuid")
    private Tariff tariff;

    @Transient
    @XmlTransient
    @JsonIgnore
    private CardLogicalState state = CardLogicalState.IN_PROGRESS;

    @Valid
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = PosUser.class, optional = true)
    @JoinColumn(name = "user_uuid")
    private PosUser manager;


    public void copyTo(Abonement abonement) {
        abonement.setCount(this.getCount());
        //abonement.setPos(this.getPos());
        abonement.setRecordTime(this.getRecordTime());
        abonement.setMaxCount(this.getMaxCount());
        abonement.setStartTime(this.getStartTime());
        abonement.setEndTime(this.getEndTime());

        abonement.setLogicalState(this.getLogicalState());
        abonement.setManager(this.getManager());
        abonement.setTariff(this.getTariff());
        if (this.getUuid() != null) abonement.setUuid(this.getUuid());

        if (abonement.getCard() == null) {
            throw new IllegalStateException("card expected in abonement");
        }

        if (this.getCard() != null) {
            this.getCard().copyTo(abonement.getCard());
        }

    }


    public Abonement() {
    }

    public Abonement(Card card, PosUser manager) {
        this.card = card;
        this.manager = manager;
    }

    /*public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }*/

    @Transient
    @XmlTransient
    @JsonIgnore
    public CardLogicalState getLogicalState() {
        if ((getCreated() == null || getCreated() == 0) && (getUpdated() == null || getUpdated() == 0))
            return CardLogicalState.NEW_CARD;
        return state;
    }

    public void setLogicalState(CardLogicalState state) {
        assert state != CardLogicalState.NEW_CARD;
        this.state = state;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    public Long getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(Long maxCount) {
        this.maxCount = maxCount;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public PosUser getManager() {
        return manager;
    }

    public void setManager(PosUser manager) {
        this.manager = manager;
    }

    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        if (tariff != null && tariff.getUuid() == null) {
            //todo: зачем?
            tariff = null;
        }
        this.tariff = tariff;
    }

/*
    @Override
    public String toString() {
        return "Abonement{" +
                ", card.ean='" + (card == null ? null : card.getEan()) +
                ", count=" + count +
                ", recordTime=" + recordTime +
                ", maxCount=" + maxCount +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
*/

    @Override
    public String toString() {
        return "Abonement{" +
                "card.ean='" + (card == null ? null : card.getEan()) +
                ", upd=" + getUpdated() +
                ", crt=" + getCreated() +
                '}';
    }
}
