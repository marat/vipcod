package org.marat.vipcod.model.integration.synch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Islamov Marat on 10.12.2014.
 */
public class SynchUploadException extends RuntimeException {
    private static final Logger LOG = LoggerFactory.getLogger(SynchUploadException.class);

    public SynchUploadException() {
    }

    public SynchUploadException(String message) {
        super(message);
    }
}
