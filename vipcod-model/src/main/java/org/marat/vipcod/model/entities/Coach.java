package org.marat.vipcod.model.entities;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * Тренер
 */
@Entity
@Table(name = "COACH")
@Cacheable(false)
public class Coach extends AbstractEntityWithUuid {

    @Column(name = "name")
    @Size(min = 2, max = 255)
    private String name;

    /**
     * услуги, которые способен предоставить тренер
     */
    @ManyToMany
    private List<Service> services = new ArrayList<>();

    @Column(name = "owner_id")
    private Long ownerId;

    public Coach() {
    }

    public Coach(String name, List<Service> services) {
        this.name = name;
        this.services = services;
    }

    public Coach(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    @Transient
    public String getShortName() {
        if (name != null && name.contains(" ")) {
            String[] strings = name.split(" ");
            return strings.length > 1 ? strings[0] + " " + strings[1].charAt(0) + "." : strings[0];
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    @Override
    public String toString() {
        return "Coach{" +
                "name='" + name + '\'' +
                ", ownerId=" + ownerId +
                "} " + super.toString();
    }
}
