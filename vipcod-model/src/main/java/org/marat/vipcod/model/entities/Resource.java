package org.marat.vipcod.model.entities;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * помещения и дорожки
 */
@Entity
@Table(name = "RESOURCES")
@Cacheable(false)
public class Resource extends AbstractEntityWithId {

    @Column(name = "name")
    @Size(min = 2, max = 255)
    private String name;

    /**
     * вместимость человек
     */
    @Column(name = "persons_limit")
    private Long personsLimit;

    public Resource() {
    }

    public Resource(String name, Long personsLimit) {
        this.name = name;
        this.personsLimit = personsLimit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPersonsLimit() {
        return personsLimit;
    }

    public void setPersonsLimit(Long personsLimit) {
        this.personsLimit = personsLimit;
    }
}
