package org.marat.vipcod.model.entities;

/**
 * Логическое состояние карты - используется в интерфейсе редактирования карты
 */
public enum CardLogicalState {
    // карта новая
    NEW_CARD,

    // карту нужно продлить
    TO_PAY,

    // карта в процессе обслуживания
    IN_PROGRESS;
}
