package org.marat.vipcod.model.entities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Islamov Marat on 15.12.2014.
 */
@MappedSuperclass
public abstract class AbstractEntityWithId extends AbstractEntity {
    private static final Logger LOG = LoggerFactory.getLogger(AbstractEntityWithId.class);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @XmlElement(nillable = true)
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractEntityWithId)) return false;
        if (!super.equals(o)) return false;

        AbstractEntityWithId that = (AbstractEntityWithId) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }
}
