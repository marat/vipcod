package org.marat.vipcod.model.entities.schedule;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Created by Marat on 22.06.2015.
 */
public class ScheduleItemGroup extends ScheduleItem {
    @JsonProperty("date")
    private Date date;

    @JsonProperty("start")
    private String begin;

    @JsonProperty("end")
    private String end;

    @JsonProperty("title")
    private String title = "";

    @JsonProperty("subtitle")
    private String subtitle;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void addTitle(String tit) {
        if (!Objects.equals(title, "")) title += "\n";
        title += tit.replaceAll("\n", " ");
    }
}
