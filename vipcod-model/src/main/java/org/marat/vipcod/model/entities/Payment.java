package org.marat.vipcod.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;
import org.marat.vipcod.model.entities.action.Abonement;
import org.marat.vipcod.model.entities.schedule.ScheduleReservation;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Платеж
 */
@Entity
@Table(name = "PAYMENT")
@Cacheable(false)
public class Payment extends AbstractEntityWithUuid {

    @NotNull
    @Column(name = "pay_time")
    private Date payTime;

    /**
     * сумма платежа
     */
    @NotNull
    @Column(name = "sum")
    private Double sum;

    /**
     * карта
     */
    @Column(name = "ean")
    private String ean;

    /**
     * Период, за который осуществляется платеж (пока это месяц)
     */
    @Column(name = "period")
    private Long period;

    /**
     * автор платежа
     */
    @NotNull
    @ManyToOne
    private Person person;

    /**
     * абонемент, по которому осуществлен платеж, если по абонементу
     */
    @ManyToOne
    private Abonement abonement;

    /**
     * услуга, по которой осуществлен платеж
     */
    @ManyToOne
    private Service service;

    @ManyToOne
    private Tariff tariff;

    /**
     * тренер, услуги которого оплачивает клиент
     */
    @ManyToOne
    private Coach coach;

    /**
     * адресат платежа
     */
    @Column(name = "owner_id")
    private Long ownerId;

    @Column(name = "nds")
    private Boolean nds;

    @Column(name = "cash")
    private Boolean cash = true;

    /**
     * платеж, стронирующий этот
     */
    @ManyToOne
    @JoinColumn(name = "stron_uuid")
    private Payment stronedBy;

    /**
     * Для стронирующего платежа - причина стронирования
     */
    @Column(name = "stron_reason")
    private String stroneReason;

    @Column(name = "visits")
    private Long visitsCount;

    @Column(name = "back_sum")
    private long backSum = 0L;

    //todo: менеджер/пользователь, создавший платеж

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "payment")
    List<ScheduleReservation> reservations = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "paymentCompensator")
    List<ScheduleReservation> compensatedReservations = new ArrayList<>();

    public Payment() {
    }

    public Payment(Payment payment) {
        this.payTime = payment.payTime;
        this.sum = payment.sum;
        this.ean = payment.ean;
        this.period = payment.period;
        this.person = payment.person;
        this.abonement = payment.abonement;
        this.service = payment.service;
        this.tariff = payment.tariff;
        this.coach = payment.coach;
        this.ownerId = payment.ownerId;
        this.nds = payment.nds;
        this.cash = payment.cash;
        this.stronedBy = payment.stronedBy;
        this.stroneReason = payment.stroneReason;
        this.visitsCount = payment.visitsCount;
        this.backSum = payment.backSum;
    }

    public Payment getStronedBy() {
        return stronedBy;
    }

    public void setStronedBy(Payment stronedBy) {
        this.stronedBy = stronedBy;
    }

    public String getStroneReason() {
        return stroneReason;
    }

    public void setStroneReason(String stroneReason) {
        this.stroneReason = stroneReason;
    }

    public Payment(String uuid) {
        setUuid(uuid);
    }

    public Boolean getNds() {
        return nds;
    }

    public void setNds(Boolean nds) {
        this.nds = nds;
    }

    public Boolean getCash() {
        return cash;
    }

    public void setCash(Boolean cash) {
        this.cash = cash;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;

        if (person != null) {
            if (person.getCurrentCardEan() != null && StringUtils.isEmpty(getEan())) {
                setEan(person.getCurrentCardEan());
            }
            updateNds();
        }
    }

    public Abonement getAbonement() {
        return abonement;
    }

    public void setAbonement(Abonement abonement) {
        this.abonement = abonement;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
        updateNds();
    }

    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
        assert service.getTariffs().contains(tariff);
    }

    @Transient
    @JsonIgnore
    public void updateNds() {
        if (person != null && person.getBirth() != null && service != null) {

            Calendar dob = Calendar.getInstance();
            dob.setTime(person.getBirth());

            Calendar today = Calendar.getInstance();
            int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
            if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
                age--;
            } else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
                    && today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
                age--;
            }

            setNds(age > 18 || !service.isNoNds());
        } else {
            setNds(true);
        }

    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public Long getPeriod() {
        return period;
    }

    public void setPeriod(Long period) {
        this.period = period;
    }

    public Long getVisitsCount() {
        return visitsCount;
    }

    public Long getVisitsCountDvSp() {
        return visitsCount == null ? null : (long) (sum * visitsCount / (sum + backSum));
    }

    public void setVisitsCount(Long visitsCount) {
        this.visitsCount = visitsCount;
    }

    public long getBackSum() {
        return backSum;
    }

    public void setBackSum(long backSum) {
        this.backSum = backSum;
    }

    public List<ScheduleReservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<ScheduleReservation> reservations) {
        this.reservations = reservations;
    }

    public List<ScheduleReservation> getCompensatedReservations() {
        return compensatedReservations;
    }

    public void setCompensatedReservations(List<ScheduleReservation> compensatedReservations) {
        this.compensatedReservations = compensatedReservations;
    }



    @PreUpdate
    public void preUpdate() {
        updateNds();
    }

    @Override
    public String toString() {
        return "Payment{" +
                "payTime=" + payTime +
                ", sum=" + sum +
                ", ean='" + ean + '\'' +
                ", person=" + person +
                ", abonement=" + abonement +
                ", service=" + service +
                ", tariff=" + tariff +
                ", coach=" + coach +
                ", ownerId=" + ownerId +
                ", nds=" + nds +
                ", cash=" + cash +
                ", visitsCount=" + visitsCount +
                ", backSum=" + backSum +
                '}';
    }
}
