package org.marat.vipcod.model.integration.synch;

import org.marat.vipcod.model.entities.Person;
import org.marat.vipcod.model.entities.pos.PosUser;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

/**
 * Created by Администратор on 21.10.2014.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@XmlSeeAlso(PosUser.class)
public class PersonsResponse extends VersionedResponse<Person> {
    public PersonsResponse() {
    }

    public PersonsResponse(Long versionFrom, Long versionTo, List<Person> data) {
        super(versionFrom, versionTo, data);
    }
}
