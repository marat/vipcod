package org.marat.vipcod.model.integration.synch;

import org.marat.vipcod.model.entities.Tariff;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

/**
 * Created by Администратор on 21.10.2014.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@XmlSeeAlso(Tariff.class)
public class TariffsResponse extends VersionedResponse<Tariff> {
    public TariffsResponse() {
    }

    public TariffsResponse(Long versionFrom, Long versionTo, List<Tariff> data) {
        super(versionFrom, versionTo, data);
    }
}
