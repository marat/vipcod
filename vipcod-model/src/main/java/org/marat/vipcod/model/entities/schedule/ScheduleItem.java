package org.marat.vipcod.model.entities.schedule;

import org.apache.commons.lang3.StringUtils;
import org.marat.vipcod.DateUtils;
import org.marat.vipcod.model.entities.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

/**
 * Элемент расписания
 */
@Entity
@Table(name = "SCHEDULE")
@Cacheable(false)
public class ScheduleItem extends AbstractEntityWithId implements Comparable<ScheduleItem> {

    /**
     * день
     */
    @Column(name = "date")
    private Date date;

    /**
     * сеанс
     */
    @ManyToOne
    @JoinColumn(name = "session")
    @NotNull
    private Session session;

    /**
     * помещение/дорожка
     */
    @ManyToOne
    @JoinColumn(name = "resource")
    private Resource resource;

    /**
     * кто распоряжается
     */
    @ManyToOne
    @JoinColumn(name = "coach")
    private Coach coach;

    /**
     * за какой услугой закреплено
     */
    @ManyToOne
    @JoinColumn(name = "service")
    @NotNull
    private Service service;

    /**
     * ограничение на количество записей клиентов
     */
    @Column(name = "limit_attendance")
    private Long limit;

    @Transient // брони. Используем для передачи во фронт, но в базе не храним
    private Long reserved;

    /**
     * id предприятия/организации, для которого утверждено расписание
     */
    @Column(name = "enterprise_id")
    @NotNull
    private long enterpriseId;

    @Transient
    public String getStart() {
        if (StringUtils.isEmpty(session.getBegin())) return "";
        return DateUtils.yyyy_MM_dd_HH_mm.format(DateUtils.setTimePart(date, session.getBegin()));
    }

    @Transient
    public String getEnd() {
        if (StringUtils.isEmpty(session.getEnd())) return "";
        return DateUtils.yyyy_MM_dd_HH_mm.format(DateUtils.setTimePart(date, session.getEnd()));
    }

    public ScheduleItem() {
    }

    public ScheduleItem(Date date, Session session, Resource resource, Coach coach, Service service, Long limit) {
        this.date = date;
        this.session = session;
        this.resource = resource;
        this.coach = coach;
        this.service = service;
        this.limit = limit;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Long getLimit() {
        return limit;
    }

    public void setLimit(Long limit) {
        this.limit = limit;
    }

    public long getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(long enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    public Long getReserved() {
        return reserved;
    }

    public void setReserved(Long reserved) {
        this.reserved = reserved;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScheduleItem)) return false;
        if (!super.equals(o)) return false;
        ScheduleItem item = (ScheduleItem) o;
        return enterpriseId == item.enterpriseId &&
                Objects.equals(date, item.date) &&
                Objects.equals(session, item.session) &&
                Objects.equals(coach, item.coach) &&
                Objects.equals(service, item.service);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), date, session, coach, service, enterpriseId);
    }

    @Override
    public String toString() {
        return (date == null ? "null" : DateUtils.dd_MM_yyyy.format(date)) +
                (session == null ? "" : (", " + session)) +
                (resource == null ? "" : (", " + resource.getName())) +
                (coach == null ? "" : (", " + coach.getName())) +
                (service == null ? "" : (", " + service.getName()));
    }

    public String toShortString() {
        return (date == null ? "null" : DateUtils.dd_MM_yyyy_E.format(date)) +
                (session == null ? "" : (", " + session)) +
                (resource == null ? "" : (", " + resource.getName()));
    }

    @Override
    public int compareTo(ScheduleItem o) {
        int i = getDate().compareTo(o.getDate());
        return (i == 0) ? o.getSession().getBegin().compareTo(getSession().getBegin()) : i;
    }
}
