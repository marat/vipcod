package org.marat.vipcod.model.integration.synch;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Администратор on 21.10.2014.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class VersionedResponse<T extends Serializable> implements Serializable {

    @XmlElement
    protected Long versionFrom;

    @XmlElement
    protected Long versionTo;

    @XmlAnyElement
    protected List<T> data;

    public VersionedResponse() {
    }

    public VersionedResponse(Long versionFrom, Long versionTo, List<T> data) {
        this.versionFrom = versionFrom;
        this.versionTo = versionTo;
        this.data = data;
    }


    public Long getVersionFrom() {
        return versionFrom;
    }

    public void setVersionFrom(Long versionFrom) {
        this.versionFrom = versionFrom;
    }

    public Long getVersionTo() {
        return versionTo;
    }

    public void setVersionTo(Long versionTo) {
        this.versionTo = versionTo;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("VersionedResponse{");
        sb.append("versionFrom=").append(versionFrom);
        sb.append(", versionTo=").append(versionTo);
        sb.append(", data=").append(data == null ? 0 : data.size());
        sb.append('}');
        return sb.toString();
    }
}
