package org.marat.vipcod.model.entities.report;

import org.marat.vipcod.model.entities.ModelUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Администратор on 19.06.14.
 */
public class ReportItem implements Serializable {
    private String ean;
    private Date time;
    private String name;
    private String phone;
    private Date birth;
    private Double sum;

    public ReportItem() {
    }

    public ReportItem(String ean, Date time, String name, String phone, Date birth, Double sum) {
        this.ean = ean;
        this.time = time;
        this.name = name;
        this.phone = phone;
        this.birth = birth;
        this.sum = sum;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String toCsv() {
        final StringBuilder sb = new StringBuilder();
        sb.append(ean).append(';');
        sb.append(ModelUtils.dtf.format(time)).append(';');
        sb.append(name).append(';');
        sb.append(phone).append(';');
        sb.append(ModelUtils.df.format(birth)).append(';');
        sb.append(sum);
        return sb.toString();
    }

}
