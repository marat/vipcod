package org.marat.vipcod.model.entities;

import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * Created by Администратор on 19.06.14.
 */
@NamedQueries({
        @NamedQuery(
                //todo: верно для sa. Но для серверной надо еще фильтровать по owner
                name = "Tariff.getActualAndById",
                query = "select o from Tariff o where o.name not like '-%' or o.uuid = :tariffUid order by o.created"),
        @NamedQuery(
                name = "Tariff.findByName",
                query = "select o from Tariff o where o.name = :name")
})
@Entity
@Table(name = "TARIFF")
@Cacheable(false)
public class Tariff extends AbstractEntityWithUuid {

    @NotNull
    @Size(min = 2, max = 255)
    private String name;

    @NotNull
    @Min(value = 1)
    @DecimalMin("1")
    private Long countPortion = 9999L;

    @NotNull
    @Min(value = 1)
    @DecimalMin("1")
    private Long daysPortion = 9999L;

    @NotNull
    @Min(value = 0)
    @Digits(integer = Integer.MAX_VALUE, fraction = 2)
    private Double price = 0.0;

    @Column(name = "owner_id", nullable = true) // в терминале owner не важен
    private long ownerId;

    @Column(name = "deleted")
    private boolean deleted = false;

    @Column(name = "price_one")
    private Double priceOne = 0.0;

    public Tariff() {
    }

    public Tariff(String name) {
        this.name = name;
    }

    public Tariff(String name, Double priceOne) {
        this.name = name;
        this.priceOne = priceOne;
    }

    public Tariff(String name, Long countPortion, Long daysPortion, Number price) {
        this.name = name;
        this.countPortion = countPortion;
        this.daysPortion = daysPortion;
        this.price = price.doubleValue();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCountPortion() {
        return countPortion;
    }

    public void setCountPortion(Long countPortion) {
        this.countPortion = countPortion;
    }

    public Long getDaysPortion() {
        return daysPortion;
    }

    public void setDaysPortion(Long daysPortion) {
        this.daysPortion = daysPortion;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public Double getPriceOne() {
        return priceOne;
    }

    public void setPriceOne(Double priceOne) {
        this.priceOne = priceOne;
    }

    /*public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Tariff that = (Tariff) o;

        if (ownerId != that.ownerId) return false;
        if (countPortion != null ? !countPortion.equals(that.countPortion) : that.countPortion != null) return false;
        if (daysPortion != null ? !daysPortion.equals(that.daysPortion) : that.daysPortion != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (priceOne != null ? !priceOne.equals(that.priceOne) : that.priceOne != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (countPortion != null ? countPortion.hashCode() : 0);
        result = 31 * result + (daysPortion != null ? daysPortion.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (priceOne != null ? priceOne.hashCode() : 0);
        result = 31 * result + (int) (ownerId ^ (ownerId >>> 32));
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Tariff{");
        sb.append("name='").append(name).append('\'');
        sb.append(", countPortion=").append(countPortion);
        sb.append(", daysPortion=").append(daysPortion);
        sb.append(", price=").append(price);
        sb.append(", priceOne=").append(priceOne);
        sb.append('}');
        return sb.toString();
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
