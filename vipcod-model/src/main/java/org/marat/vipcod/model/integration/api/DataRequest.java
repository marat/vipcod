package org.marat.vipcod.model.integration.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;

/**
 * Created by Администратор on 18.11.2014.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataRequest<T> implements Serializable {
    T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
