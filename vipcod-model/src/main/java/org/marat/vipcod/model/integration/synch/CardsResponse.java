package org.marat.vipcod.model.integration.synch;

import org.marat.vipcod.model.entities.Card;
import org.marat.vipcod.model.entities.pos.PosUser;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

/**
 * Created by Администратор on 21.10.2014.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@XmlSeeAlso(PosUser.class)
public class CardsResponse extends VersionedResponse<Card> {
    public CardsResponse() {
    }

    public CardsResponse(Long versionFrom, Long versionTo, List<Card> data) {
        super(versionFrom, versionTo, data);
    }
}
