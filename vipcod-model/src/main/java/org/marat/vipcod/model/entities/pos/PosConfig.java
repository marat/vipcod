package org.marat.vipcod.model.entities.pos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;

/**
 * Created by Администратор on 29.10.2014.
 */
@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Table(name = "POS_CONFIG_ENTITY")
public class PosConfig {
    @Id
    String pos;

    // интервал "случайного нажатия клавиши"
    long fastTimeBorder = 300;

    // таймаут для поиска камер
    long webCamDelay = 5000L;

    // порядковый номер используемой камеры
    int webCamSelected = 0;

    // каталог для автовыгрузки ежедневных отчетов
    @Column(name = "export_path")
    private String exportPath = "\\vipcod\\export\\";

    public long getFastTimeBorder() {
        return fastTimeBorder;
    }

    public void setFastTimeBorder(long fastTimeBorder) {
        this.fastTimeBorder = fastTimeBorder;
    }

    public long getWebCamDelay() {
        return webCamDelay;
    }

    public void setWebCamDelay(long webCamDelay) {
        this.webCamDelay = webCamDelay;
    }

    public int getWebCamSelected() {
        return webCamSelected;
    }

    public void setWebCamSelected(int webCamSelected) {
        this.webCamSelected = webCamSelected;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getExportPath() {
        if (exportPath == null) return null;
        return exportPath.endsWith("\\") || exportPath.endsWith("/") ? exportPath : exportPath + File.separator;
    }

    public void setExportPath(String exportPath) {
        if (exportPath != null) {
            this.exportPath = exportPath;
        }
    }
}
