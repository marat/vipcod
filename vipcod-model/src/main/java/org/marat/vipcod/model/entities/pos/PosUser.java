package org.marat.vipcod.model.entities.pos;

import org.marat.vipcod.model.entities.AbstractEntityWithUuid;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Пользователь системы (кассир, менеджер итп)
 */
@NamedQueries({
        @NamedQuery(name = "PosUser.findByEan", query = "select u from PosUser u where u.ean = :ean and u.deleted != true")
})
@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Table(name = "POS_USER_ENTITY")
public class PosUser extends AbstractEntityWithUuid {

    @Digits(integer = 13, fraction = 0, message = "EAN должен состоять из цифр (не более 13)")
    @XmlElement
    @Column(name = "ean")
    // у персонала может и не быть еана
    private String ean; // личная карта

    @NotNull
    @XmlElement
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "owner_id", nullable = false)
    protected long ownerId;

    @Column(name = "is_manager", nullable = true)
    protected boolean isManager = false;

    @Column(name = "deleted", nullable = true)
    protected boolean deleted = false;

    public PosUser() {
    }

    public PosUser(String ean, String name) {
        this.ean = ean;
        this.name = name;
    }

    public PosUser(String ean, String name, boolean deleted) {
        this.ean = ean;
        this.name = name;
        this.deleted = deleted;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isManager() {
        return isManager;
    }

    public void setManager(boolean isManager) {
        this.isManager = isManager;
    }

    @Override
    public String toString() {
        return "PosUser{" +
                "uid=" + getUuid() +
                ", name='" + name + '\'' +
                ", is_manager='" + isManager + '\'' +
                ", ean='" + ean + '\'' +
                '}';
    }
}
